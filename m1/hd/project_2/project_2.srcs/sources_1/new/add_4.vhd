----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/02/2023 04:32:54 PM
-- Design Name: 
-- Module Name: add_4 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity add_4 is
    Port ( word_1 : in STD_LOGIC_VECTOR (3 downto 0) := "0000";
           word_2 : in STD_LOGIC_VECTOR (3 downto 0) := "0000";
           SUM : out STD_LOGIC_VECTOR (4 downto 0)  := "00000");
end add_4;

architecture Behavioral of add_4 is

begin
    SUM <= ('0' & word_1) + ('0' & word_2);
end Behavioral;










