-- Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
-- Copyright 2022-2023 Advanced Micro Devices, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2023.1 (lin64) Build 3865809 Sun May  7 15:04:56 MDT 2023
-- Date        : Sun Oct 15 17:36:03 2023
-- Host        : enji running 64-bit EndeavourOS Linux
-- Command     : write_vhdl -force -mode funcsim
--               /home/hf/courses/m2/pesd/random_proc_exam/random_proc_exam.gen/sources_1/ip/dist_mem_gen_0/dist_mem_gen_0_sim_netlist.vhdl
-- Design      : dist_mem_gen_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35tcpg236-1
-- --------------------------------------------------------------------------------
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2023.1"
`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
bEktTo8XfP53J4LC9J1bzNOsr+DeYSQtsSeSeRwv1ROtu7MJT7BubpFM5B3JNITvmmXMIQ7cHCcM
BFy5Vu0fdwcQmgznzr1F4XAF5OH/PlBVKmCiA5IZpd+UQUMuy8l823afh4u8+Fg3bwZX7B36A3bn
Zez9yHjSKD7JGdQ9zA8=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
vAZQ8ZTe/MermX+omywGuwEzd7SLijiaDbuX0B9K4vjWUXvRoI6Em0qizreOX/qdo4JlybEpt70i
jJhVvWv69a9yKb8TMuvLagWbQydSwTJKTY6VSR/CtA2Uive8NvQyiQKFXLjR8k8OBlgOYmyzZEEM
vYgZLdnM3d2xSMMmeGF+dNh8tCJpM10LRaCrnj5w8L73RtOImlhI/zlR8cC5oo1TbyRV+JuHvvMZ
sYS3+4qn/f80Ugvao3cYMW0LtoTftK9oYpzhiyqg6hnJnbGsAENom2wqBpcRJf1vsI98WiJqDCuh
LIdMFI+M5KuqToM8D+FTQUOT2NniYpTmj5qTFg==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
VpwnevLJi/mNDesLbbdRntRX/1KkSUuxvcBO6/opCSkxKA2w7s8Eyh+CvZJvHhBMtWZquJPlWZsE
d3toYaeyczcrzAzfKryx5nnTvscAyYnKl8QyY0fWsE1UqWjg6tazMCtzxlfF3HfKx/GSm3D/0NEz
xzyxLBgRosbKCX4YRV0=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
MhGbYf5xy0E517prDNoCHbf/sVQ5JHlfzlh1Fz+rfDm8S3/Zt1g/AR2QuQPNwJUQO22hvTTB491a
xRG5ct3upD6ZdXgMesPA9KgwjRjoBp/uriYuT6Sb/yE2jugYl2qBGpqxN9n2OgAVfK3o9XZ/aIcR
St2PwrmKRzU/ZoYenWUMZ6ZRsVNlzFCEBcKop6f5TBy0bWAeebXRZ0Mot23DVX4pqVyFaQoXdmkm
56Vr2jGszkLic4M0JoKahUlQpnrZuHIWgFVd/RzXXP9HwYBRQTxaKnNX6eWTdksVvzAImMYoPa4G
PJJFf+gsNAKp5BIFXjwHfNC+Nerc6XzDmxe+pw==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
jfnJJlFHpbB8S3PjID3rEIRi4fzY1WUZaITx6CJ38mSZfYSA13DJislb1OQ17w4Hnv5eGM/0GVgA
2jPR4wYaMzC8v3iDfETrH4kyrFglo3a/NDlACuR1U65YoHUnUu0UmMMovxQEnd9ByAfOtabZPL4j
FTvCoVMpwI8rdT4YJQ5pYXryESdM3NUe29p9OWbY1EalisEVViKuSwS4LzwtaOmrPecCE56FGEp+
2iyBMICOFF2PpT8Bqp39Z2rx4xyIiudZKo3LNimTm/UYBCnPAJ7XBIS+JiCIOkHsPER+wNivbtUb
J02F8ZLbEtS0qmUdYDXO4qqhc1njU9O6Uk9yNA==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2022_10", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
uOK6pXmc+RsarhB5GcgUPkseiDLhaN7KZ4C18Aqea9NqSbvIERAENTml4U58cVlx6j599K+L2aW5
rVMZLtj8UE4yfEDhtivrSdBYh446mqbnToHhH5r4BmzYnr6BUuXVZ4NIUU29WnaJUZxwrvZeCln4
GQCdP1kUA1Ozy9B47ndTYgOzCcZSr9w36W7ZA1gm34lqVpXYuGsaRTvk1DhS96aFGCeiCTbs5HM3
e0JPkZ7YUsMgWuRzE+jHE1TEMVjbPkpPjFGCYOEeDf2bc/2s2fPLA3bxMs61xUFH5LAd7Qrs9D2v
Mx+Vcfvo7kmp3J5LW99NXfA9OvG1JgjJ7ykhmw==

`protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
OS52LCfxYaApFxxvQUqjJD8DSzwhbsM5irqCX6E4R0iBINlXI3QVmtLKp8vhPICYZWjEuTIVzohU
28vwAOP2ECPWOkJjN+ny9RQeAKmQhPbxHYOysXg4IgtMbK+ZODUoMyLIsJzz2yIFl5qvQeLBnc44
NvqDk7nFLhtrN9De4XV14FKtDvQG0BdWr2mXiS7WiEAQxiww87A0M8yP82JlG6ykYSwQh5G8K6pv
YHoqI8mKAC+KGuDltBnyBrKGip5pRq7Kf+0okVAOwt0lJwDvS0JMNEUg1HK/mEIR6TKUdd8B/fms
4qcaCBYsptjoZVCq4ygSG56x8uaQXMVsEALe2w==

`protect key_keyowner="Atrenta", key_keyname="ATR-SG-RSA-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=384)
`protect key_block
Aew/RSoMZUIh8oIZPhChM37w+R5unp+7pprfqezjGFUVX16UeT1spPFU1DaqTQvQkXhBe4/aNxvo
Y2eUJsQd8zSC9wBoevCnvwaHEv/IBc+OKmBzOPxO1hHXDVPtDZWdRCx+1y0ZYhQa+NA6jLP2zOJx
/emAZW55AWgZKKJS4QgantVgmUSyKVe/LlIVstraTkF4EzV092mOj1iPH/UqFFno9IwE1aOXuYuT
XrZU9D1dkPLBMg3CDwOi+bXRSgjvuueWT7ostJSFraLwDkurP1pYHHG4NDxYiDxMFWarWeII+T6v
hMJKd/8ZRrh5aHvGV5O/Hdc4rPitxa/cdQPAc0r2e2XWAJIdic09atzXXyU9o2vV/urpMsjSVva4
B5a/PwS16c18IMm6vAeFSLMo0T/jor1Q5SoxEC5QEkxvEfIUjjw7k0b1Crv5EfWz/sJ1LHwqlG7t
az+h03yAqvqGfOHC+7YoilYImR1NiLTCLgxnUfIvxo6woY4SgD+hLki4

`protect key_keyowner="Cadence Design Systems.", key_keyname="CDS_RSA_KEY_VER_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
iZJ+xdyrZbhNc8zYurF70yKiutV2IBjRXDiOZ/7w25UL6rCpY4Pd5gJN3+SNIoQ66bzRxlhaXMNu
tzoCM2kFY4N5ZbCy/S4rtBK0PUHKEVd7c5Btr5gn8BgQWiIafJ8Qa/8xqo95ocakFzN6/V+DNvyN
7FPkXDwuiaD0cmHW8XyOxnHM2b/XKHOibr7UKTRAomXyt7y80BVKpE50ddxXAxw9wlMn+gpW5Kpz
Dp8z4VH3uZrVv8Yl5RWELOQ3Uh0Xizb20mvc6Lu+BNoz0Ys9zZUaqKU71Kuv4s8vgPzrZXXNifo2
pU0aNj0oqAGlSTcTCBF8Tl6/jFvUXQEzYoIfiQ==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 12592)
`protect data_block
z1sxbN1KRmmLftjoGaZOR78H+HHCWMBiWkH/pVf8WJAvEoBVDb8YPhV+Qbr9vGYrUIKHqu8DSi9k
AQePcmAPgghySk2r5oXuDDCePgQ7ki/8D9DUb0TtgRCzFvM+BP4UuJqb0sK/J+SJYYkwBijmmwD9
gTo8jHHKiR5PFLAoQsunIyxS9h/IwJTeM9vnCEcyxR0jgN73dr9OpcgQF2CtzkwL1giVkGS+pnPp
A0zYUdc2cbsloziPV8JibjCiV2ZEXMotxZyWe4uTsWJVl90a9TwjEu2mVzhEpSYpqIkNKkZnDmJ2
M1/BQFIZf/imDAx1wylUql35X1Gga41CGJTZsEfta7HNVvo8gyi2dFiI1LVXRQbtLLc8OFM1yo9/
vWnfoNA7hV1G65poKjyLOcuUENbF1kYA8rPNwge03+0qyJW4rXgAeBPh6CcYmVeX2SxaHeB2hglV
/9mqLQQ7MLaGkDamVEXNP6hoiaFhivaaNNpVj/YPMTYLk9JkPwr5atu7/wgJ52SmHN5rUGSueMU0
BQuhrV1wNAAf5M8YcBiKE4GfhZ8P1LHFu13LP0zFIKo6B0VBj8lFf49k+oKETllvzeh3BGhk09L7
EcBvCBnsZpNo0IdQbczdSJjDCc2RyTY4d2uY4Zvksfy+oN9Hlcpk1whoqQDqxbSssP5bfdt+ie2w
hreoGdKRN1rlP67/epiTC57aGFvNgiAMVrqMjLA/IbSDdAqyYoV9tXqpHhShQ++yR3eeTY1sMjNB
WI44o8RLk69pEyfGbH8huZ5W9MGf0Iw/4vUFRdf35efj8ov1PsnjL51/o7IOpunMW5S86JF78HNP
95P/g8Cf4N8us/BI9zCrWPXXKYKDMqvg0x8FsRSFYXabIREKymcS3Luiz60r6fH1RNslDtHvcgzy
Vt+BmLuyqABQZMMfGa/+sR/UsYtuI1C3qcWukm30cDN0fazbqWJAP9dHhlf0sYGcHRJGUs+LAlTK
Y0QcoC1I0s5gEGcKj7kBhJRtgFFik67WAeEQaYvqOXXrv0QLfn9Km4/B/mVcyIuepls//8tV/LbV
f7bP3fGd/OyaZpCCc+xn/9Fjm7yyJb3r+ceEIXMm7xCYfLzNqmlJnXKU2rHLRRc5cxjZY0sK7eVn
+YuH0ucQFRVJ950naLZrAc+fE5CjQma6fOOf/9x7Wv+TRr7alDM1/BpGy2iiqMI57abgyJSpS4ku
oV3vd2JswTJ0hptkp84kSFKDV5M/8IdZXXUJjNyBsciQ3pqD0bV1c7iXJyX0sqaoimDM9hy1lobw
Sky4ji8ZlZ+a6h08hjM/z9810Ll6TnpMttXF6B1C6HvvMpAyna0JYbr6spHb2Sg58OpN/gGrYTFy
/wtSuAZAxoPI4YX0q9R8xCwHGZcQb/X46uBAQJJ8T+XpxXR/gVRnt8CAnNKcH9he4HV0+AYCPFTO
i6QG/lTjIBvMY+j6lixHSK2Q9ix6hnyFdxLUN/6IVHBwvrWUcKlXqyHI1dVoroatfOoaWC2A7O2J
gZSagP0pF580L0GBWwP4Z3TDR/H0VpH64crnbk3gVrSqu9YmPcBW+/kVlxYh/dEhZ9CVFruniEWZ
i5ZYHBRDXpm5w92r1OSp87X8vhY6bMvlEweaaUjkfTqeggriE+Z8mAGH2j8bPjwrYncRnhcz1fJ8
3ZfV3PftcdYJVT/sopgVJSOx1fXPUWLTP29t3smdNS6VgUxJ+RQcj7o+w6kUe0lAsogaLTEA8Plj
WxmVMT82UOZ6vhYuSDVUcKb88x5IcPeDb7qFON21Dyiq/xe68VP7xMdPkcZL7EhGWSUTb8pCBOzn
//LPCb7wPbiyGezIupGIOoffMOK4fVvQxr9yzuYfg57fJxxkdGlKckIypDT/R0IzQVoZtEd69nmq
7eKJWvUiwIB2T8QPG9k/rJBNKPpzRIFsJ2D9zRGL61jkWVgLKgONWRcbqWi35dDneJUH+LOqpXek
q65BG7JiGo4wuu80bVVzjNQSmOqMKpUFUBYDnMGy3PtWynSI0WtnTDwJKo/f2Imq7ENHHizFMaO0
CZrKhsfHxNm/iya7dsDmPlvFkZSEXqo8/lRg++yQNel/AH5ju/0l+8v1kIA3dZGKGRXBliljOu5o
y2WbXVnaX4ypyAbKH8JtOzd91t5uVdHHCWWYDaZYtGPW1IbBS8CffpsGf8fO7FwvNZ5sB25qmTHS
KQsj9pcCrw6H9Eu/HDQux6Hzv63x+qxuAQmGXCGJG0YJ2gfkNTahU3RKrEqjy+kFIfCVFkqz7VS+
N6aW5wReRqp0eeg1gqZMhL3/+zm4/Oy2ecmK1zjTTZoz9IHSlOdGCH7FdM6FO1y+ZSWvTbq+S2+4
yKNG07OgxpcICgBMmLDALOa6KIBwK9M5W3JPxVHvqZqZ0pqHrFZYw32KHIwYcbbRO1qMxhULl9QA
fe1PR7mSnqRFRP5WTHRKP2cX9TICjfnv8bHeCbMb1ZTsLwibtEbwHOwS+dmDSz75Ra93qv1HEmoe
UcSDHxCNLthjWAgmsMeu8wZFTt2E3m+x1iq1XhJWEiDjsAUfLkEOV5x+l+weS/TQDX1UWT5NhvcX
QnIKsERyQ2rIizqV8mqdqz5lDyoCj6lkLopc7nuUicSZxCAaFSwljA2ZE95hnnvAC3A58b0LChTo
Hefqvk6bdU5GPaREnucy7Dzecpc71+wqGB+ZclJx4MHN3H9p7VvXvkVAhj1nlshFpuRIWPt7lB8Y
+ULYZMOeId3QT2nO6w6vZNRcKjMEpfdYe6BFt99vBsc39snidHYhQUPjsYU+qK8FoAQ2jISJHs6V
bw1rq/wB7NStLE+ozkV4V1tKDzSqYN9m0ucoMIrNYuTWZ7m6/5JtSm5VFJljEqsCHTGt4V/LrnPb
nw0xoU89h5kEfx0s2LKgNepi+KxCnBgvrtMzLlYa1vBpJUIBrdJWr0aW26tVru4l+MS6iNPM3+Af
Zw4lFucaUDO1/Y+/nNOei0jlr3mWdcQeWGLT2HlPslhTLLbAa4Z1JKniT9haNZmrO4hjZnDCz7vj
+JzW+C/T+5/XEMEROXZsLJAWBePJGjkBfewR81p5NtiiNh4O1wiUxhVV3JqvjHX99hrsa5/WgR7p
hV/7AdieO+oglBnU180rPJwcdhjjqsiplqV48jZbjLfBSyXtgKo71KDqDJt7Xd8YLqF6VlEeXLUI
lBsU3NYNecwG+yOOwYNrMTZ6K0YNSbSkTTWAE1NomIFv2c4YrwzCIkHJGIjn4qR5BjaljVOm56J4
7xqd1E97G3iHp4kJwj6s5RDzGYzEyO/0M2PrXkk3WAZthf16RURPKU6RRMHBvf3r5T7WNrPNEsmD
+kq4Qi2Sz2PGrYZP1D20hReq6PzQk+LJ1AYP4nYH0uvhOdo7WF1eQ1sd2WpfiWIX1xyiWIJ2xQp9
oShTc9Qi8kFNXRx1pdiZR3B3/yGk5gGFqEKUJQqFj0kMgMWui5Gitrh83aESPxgv7YPvnUh/u3aG
RjAovsmVbdk9RR/cuCk8ByC2U3Krx/15NDkvy+gEbumU5W5hoJs954Bj9V1pGbdJ6LqxyKD+uX7I
E6QKKv1h8Sdp0FhMm1KhGYCsxNlx4r0+2htq+D1ZBooISRr4gFuJUQqJHIsTCz2BRTuEhWhTic0V
oLU9NlAuIctmZHb0KrKd4nammjHKYHaDZwcY/xkpOGhK9l97/Kpapks5c3j2bcIO0wHf7QZqfIgK
eYihpKyfBRx2LxkAagjZbs5xp5dfYu8SBZoxu5TCvDXwW5aF0LTOOIjf2PRhbaCHv+7vewIg1BT9
wxuZUPud3AmzIZr2uKbMVXgIQKmBMGFXEL+oyMoNfY4q+HSni9r4tmSvY4yWvDilXfXC1hWvpYCX
N14fUDIWiPVJKE6/WORilEm675hyBK48AYuZoWMPjjuNTWGUl3FmQW4uxjfS3SKkb/EvilEenUml
YPg2RvgZKe8VxZ5I5/DGEiZLuytuX4gFlFsW7Y6WGhiaFzALPSR0ybIsgL1As18RDqX5S+N8XU/z
2ZeSDkAXyMkCaQe+HmVJobVrSVfLh2x0yo3ErB4jLYDYT+7VSKeY0HxbCh940JWLVCTETs1ps2k2
3M/j+8wPOo7eufTo/dUaHUL/XSj1vrYqU5jk/5Uoobzii76AsgbtwisO2FO8z70rvfMDqt3vc0Is
W4QmCluM73VvX936er9ZbgcrQ8FSUn0tE3/s9qbFYu2+UN/vBPfI19guC3gqhLFsYOpHJxaI1Bz6
eRg4p9T+EHVPMHrqtEkpB9p4jUXTB+pYhtlaxr3o8OtOue0rHCm46Vaw+ZdLGlDxPmoLnnBtGlwE
VRtdiItLcRXqCk9vVeEzx/ZHqivlYVmfc4eFSv2OcQOwLeY7ded/MqlbUjnavZplMPr6eOHQFjlG
4HttfeqBonSg+Q8x6BmV+mxPcB1ThM/a5gflYNELaeR32rwa/Su8cHE4JHYabNAz79LHDzqRc91J
XP/xYxg/NU75dPfTAUWGsCcbMRr5H5M/2LrnjXY0SmhoCL/AJzhTxkAGcamg/g6fxCF5PxvuvyrR
QKLbQrqsVk/ht4T/T50YeVY9SQi9HkgGofgljhRawMk7CWh5ds5zOA9FJkCqgNriA5yW1L0yy5W3
IudFUguiXACwMrPILDKuXCw9uwalZpQY2JGDvzqdjn6OYeuOThrNHE6OpOnfHmCmUdV4C+1N6rKV
buqnbAPdy2LiYVFuwCE9aRJYs5AkAkvXLvCYb2SLmHhZ8gbq+j6G9esXm9ByKXE+YbdwbakccerO
UbILjJvaTw5byUQStw4gGzQEQ+kGQH7B70SJVJH9Y3j7OVDZDiwhzQFwTpyBIYLiyJ1dqLxfizSf
dMN5k4e0Jp3a936mN7e6fybVToVyzP84QA2PWGmQIgFVW4BetzRr0K2pvPtSM21HuboaZJxzzOVW
K8v2dIS1LMXNtT2/NNfAd4hbopfGKVliWFT/fO1RF9GGWpgTyLohMFSNR2FxjPbq0uajgflPks8s
61UuEcwSXja4rNZRzI2IyzieFHBbtjPSr4EMxaHIJcJzE1VJVinWZexlaUiPtnX+BrtAhgQ0Emsk
NqABeNSI8H6CBNyHcDSWuVF3ZgbSBMhZ2uoaXHOYemyxwA471ysAFqWuqYiMyDccLf0zn1XAFFzO
YGLW6IscS5+jZQFzd+J+3qf5nbO7jwI9PotglLf37FlSXwkccwKtH/NDjiywhxeb8+n8v3Jc1f+6
W4mZn+nXnZtuQOlD/zuxX1U6wutNVCPFVIITVVgt/47RnFFKy72/fU2YQaizQ7MxXgKA/sn3fB5X
tyCbn+DZSB5PcQbDVTM4pMAezeW6zoeZRiaoHocKl5outDvn5lQ3tctYhosWlrQdHsKR85nVlMTP
c2NrxRoMuXjqloYU8NIy+cRbKdbiwlwCWfWOxROA321LxGT+ESjqL1tJGbqn4xy184SPN10cjnVc
Fkp17HT9kRlNTqNGvwUyRzPq6cxLctUnauhUDPzL+P9zo0be0Sw7jV/AURKh5ukT6X+9GqeW0Sfj
smkGGTA5zn0nBqOpDcUdEvC/xw9S7l8tjmugWqKgynBmCii9DvVdg5MPnpRMxmo0S8fUXM0L+2YJ
7AKTjuUxjAC3tILGoKVMaql9jSZiOacqOgPCxuNsNCryjAyncBVpwi7zXimzoQJzlWSgWTSSN446
LSjQH+nlo3IWbW3q6+cqhDpUlP0Vf5gT39+sNEQAvKRJ1u5aEhpDebV4dm531fzszJFJPMff1aSS
tCYudyEJKuFGPszkhRNZF+He0ALUKO7nUuRF3cSpIse8u9B80UblcVy0hvXMEdQOGLTURYimvrnO
rg7Jego/SBwvOWDYo/UbOLgSQ+hFJ6wt0i29Rd9tCML2stKF9PwyptsSYY+X2pQZ5y+3esIONRQp
Z5UFyScwAkiWazQROy9Jvznyv6OwmifCEXz+YJElxTAl7BlF3jdwTbUWGve8Wy9DmpTqfcqupe4h
ltvcSpaIlZH7IYu0Yb1xFzMzVZhy5UmasqDIkO63997HQ7QCyjc14RI6taRvI6Q5Dkmz8Qg2AxGH
OoWXumq6tIwcJjDU9Es+ucjxaYFNyVXCyHldknVv7nH3v5ikZzliawSWrQYn2cdqvQ5jm/qFDUnY
GBWZGNRtnRWjrj9FCnp/x6O7oP/EmgZxmlJdQiFCBciyHD3tZp+7WaGkkBSIO+qg/9mgkGdZNcun
3M8awbxRCbmMhHHnw4Z1T2/APfZVy8AcjW/X5+tLSv7VLEn2ty159JdmIb49vuARlp7O/OAqgm/d
Vu3LC9mn/S02CMeVUo2JlS65fj4H3MrMzM57CsB+QUHw9Ff4uXHmRu9paEKuYlIjlY8vYfuenCCG
PA4/asiMhBXq3880L+gj3R+Ji4Kf4rkcr1t3M90nHPhbkQ2Sbh5+ALUOuf3wnULqP4ZDFLMgufqo
mydf/j91KPOfvrxIz3dpe15/O641HJiSOLXMyhcbU3u9gl/ySoEd4roPe/pG+7eKIHjaeT8rnKLw
hK6XcK8yESsIPKpHTeXpPayeYV881HE98Kyx/68pmSs6MmVNQ5qdMmKc+7MF4t44Mpy60oa0SW5L
LlERtnFRNuCe1ltGC4LjH3DvRZg1B0iWiUaExnUWZTByPghv6+RH47gjUcJFsjTFxKR7EDLrgNXJ
wK3cP3qyDw3YCqiL9j/fYDlVF0zgvo8jEMydNs48A7VrHF0cCsCsLwc1C6KFVWSv2LnU20/fxt+S
fxlbpPib1981O63VMsVnQGhNDdqssI9W3iwwRdXiPcTSKmPqowcC4i34cI1dpjBSklyeDkwDOf5f
K9zNPPiDUfIMd0GtOakeikee/UBNtMQ0IkcUbKL6PYWiFEV262hzENMqOhCgd2We+KLOFdB5RlGi
pXgORQuzihxNMjCm0JOE3MfOqTCFwVyOkOzGrHS9qh8p9D1Bem9ejF5Gxv7rSDmXN7xijgkWbnAV
eLWNuiEdS2xSE8tfJ5Y7dMddx+V9xAQWkyGUjrFd28BhslX2VY7rHWt6sfBQh6CVSjMqM/CBGlcS
eEDGL+KtGQ/RS9Mh1+DtJafx2VAfhbOYOAHoBEps8NUlZ6TEmxrg/Wszqy1dTtC7UXIfOi679/xH
jB17cgK7rcgmRgPiqP52vdk/yu6fFovBfyra7LNpt28wIZr6MqF7dUZBSULFIqe+24sRK3mG77FS
46q+wK5XF87KKKVh10eERCrvoxE5C7kedm1jHk0g2lsuI0ZbIyqF0+bKTg+i30NlIShsvBMm/T5v
Ap6Ixuz29KO1l62JDnSsY//iOQd1nZbswSLOsL/rGxsaAryQ5qBNwjrViQJle1oswMDcU58SuIEO
ZGM3AvqaOVhClRoFaWT+eJRixyyYwjZn6Kt+IJ/NSH7CDeSBgcBL7SST18nYWjXTh9Y1nwAifULL
vKx+jqETUyE0oASjTZ4mbW31xgjMBum6S5pBI/ZGYnDnhzbjSgW8DZA6vBs/+3Eu3QEVav1Rj7/3
FU8OcyNlg+bmM4gGs6RYzbbVUbnBQ3U+sODfelo0RqcBpyDSTyLJL3PVKkc56xMIwKNNl+4B86J3
60t4aWdyud11+uV2Cwe+YvGrf9UwsjX8kBcWzyP6JauuX+z6fL4IYvAIQST9MRJDOGm7E11ROLYp
4BLjfa2v8LBbMALyY5lnN9l16W+TiRMpboTLvPWIWLlJPdyNEr0m8zpGvcEusRVgrVN9rwxme78s
3ryIYLFDWgXvehAtD+DP+d5uIWZyyUUW5agI9o9cGjsLOxH4o3bXwd0+RYgrs5HfYRwac5iu+RDT
ANkWTl4/FOyyAFubJK9T1Io6rgUCPwV0qszGBtAHyeHpZFey5uJjYLJq6xPPU5OgTSPGt83ZfyFt
Kwq04cjI8t90Hgv0GMqy00OA8LodTRZyP0uY7SGw/lZ0UtM+/G/BtYBe+e8FckzQnWLV1jvBm+IZ
/BHoYSbP/EOAjYjXMisNr/xVPi9qMfKlPWrxN2dzdmltCLFqWsWxVMQ+gUghG+PBbumeFsAB4ZW0
a76HBSFkhQ6c4/Yea3whPauRZe66HgCPy8D/2PW0yka3XpZUDw3TLzViMpz5obknU/ziPD0K35UC
Q4lFTjj4bbXVxzpyYag21jy2yfHw2iRvJ9fpEpmhpGvHt9Si+ZR5Uaq85mrXpeQHjyvuyR4nfsVo
oukdf4QT/cMSpZNII7BDPPy08mddi9KVZ/fxHlspJi1aG5o8RwatV8c/zgKNd4JKrI7WXtDO8NjT
WKgjk4nmydC3vqQ1CnFWT29ycwWJXmWvmGaG2tXsETCb1ixSVLYdAbInufzSFXGz6lOKP0bl2xMJ
azrpTxQf2/co1Tcx+hpfjmYyi3ZrWBv+5KHd5sNiyPNx/2AlOfcDZTmvIxqScLhAtB6D8GVWI3Qk
rv2vp+qieciuilbh2pl6MUqG781lVz4Ud7EIRIzwfrtzBeGJoEUbQgCHvXfNToYbogZxK+AzFmQA
wJxZoy21NNlidBvG+DgdaNrIvRK8mcbPuOtu9G9CLBGImG3sC5u7qmxVp61zRQnldW1grPPceP0V
4P4Kh6PyFGDqGHK5h1bjj3AUgiJOBzYKMnJYpGGslJNYQCMecBvJgC/88ZYypSNzsqfHnFX+TbGN
ooOqbcaP6WqRwmk4QAd4laGCXr+GMIl1uaTbElnAj9o/4OXJx8rNBZA94WG/bKPtwTljknmHpIKz
qPXhAo0MTkDCv2NhriUWb2oICJFsK93nWhpmMONSBK/NBzkvNYscPnK8hMPkgianStgFh53bECAf
aLWFriunSLR4c95iRIASm9JeNpOXhZDUQFPY0I3YEdt+1HzjC7o8m60GAZ8LdDBUCsQ1qc+avD03
Zh+rRQwP3ugwWirVmhZtUDXJYVI7Bdy6t+3smSSNGUO52i6IvghAiwEgyDicQXMD7E1h39qgKQuS
DzKSBao3SyQZuj776ZL30Xtsb2laHIVNgh7rxIDPxqnOM5xG47LzSZbpOHFg7Xad4BMZnOxgwWU4
pGpzNDjBsHa37lcv3Xuk2tFqOT3aGRsBD6KfMSPV3RbLBuesGBAfTLGuYEVcTInv0TfM/CQqZFwb
vSxupLxhn2df3D1czHYKcucLFNoAWV5qHIFet3UPh9vnLJ+URx5WhuUxaNhq4c68d2Ao2lMxeqHg
Gm3Su0TQ3rc9PuTrgpZh3MScG7i1WdfWQdp5K6qvYEzyqN9Ig0oPcUF3yZjsoY5FXN1LZ+Cc2TQL
d3Es3eBelQ4x18ysnLnfyDiSImn9wxp9lNMZf+EVyFmp1S7cnYkeNY2nY0JncE71EGz16mugRqe1
25agfeSWkHEblSh4Mlca4L5ic/Xq8fa3R04vDbh65RY46zWZ7BaeAWppY3JIaje/pQKkODD5++1d
W20EP7x04eN94K9LxUcdDXd07GbyxW3AhF6B3PQeO1YNpbRdGEOrHhvJSa+6mT5aQFmSa1gJsWix
wrwKOynJKC3G6YkRlxxktu3svzoarKGlRBpz0h4/9gL6Hu4sRhf8mBYPiJg2FQTWumXfBts2J7J2
uFN++j6gzi3H4WauH2Kqlxl9HZA/0OgavI2pCnYorZup115o3l3+gMkhom+cmcreoX2eNVA2Zmx8
zUIahptRi35ZTMGtF2avIVs7XGdgK7a8q8ftInDiKlVo209NdsgAZpOjaOiidPIg2jsN00FzLJcA
MRe9g3RICBoGvaQSp4eGMZdlU8Rc21bH2J56F6Evn/vMTEX2ocnPZwVYhyx4bzz+vAvtsinVZNXP
8Ckc24f5/FZ8ErKDbY0j9irDPKpMCZsm91bx8AcldUY33lr12uj2K3EX2kd10sgkCpweSRFs3Htz
t/IuPkFiOMacLMAbiRxpk98lbkzlNyXLFSLNHUAdJQ/30Uflmfk+OZoyg5R552ozwVVS36W617Vq
JPYwy5S9Qyj8spTC3VWZ/6XK04sxw9vnjGLGBW4GXwKnH2bm7XFpHZ8iEeUs+IIUjJ9j5DgrpsDI
p/ic78G6+zgOWfEnZLEYhoKYiL/aBkr+bX2ENcOrJVOo15D5a9Ve1AtSzGiarpyO9wmCAxqZ4/q0
0TBgojnwyzeGdbc06oJp/WQnk0dWNZ+SUsNKVyNmGWgyFveKfG7M4EGw3VVS2moUj/ewQpL94Ck4
VulUVp6Sij1tezCyjkh5zvaUOAgHSQbS1EU7P3gVsVCXiqjZkVssucyaXzNaH8jyyvoWbdbiT635
/HrHrr7sdQBdk8FJOgBPrzeBRu99F6d08VvuKQnN2Dy3UF82i7lZZpsjnzdm2mgC7r/YIrbZx9od
n1Lj71ROSEk77TraGu4RNklUn8hOTOvrrMazyECtke+jg9CKwrqK4VnbMUCIM855zWnFgtmS/Cgp
hTMvB2/DEvkw7hEHlCiHhF4/no7lusIVlsKu0XAGpuv8tdxiFMgg7DrLAKfYHDj/WP7FDBjGrjdn
y5ucstvq7NfQUwCnDFeY1RkoPw0i0coO2d6ABnMR2v2AouUa2FLvGMRPoB3NKSlL0flc8W4zUUPH
JkiYF7h02ULQVweZLFnC+AflD8fNvF21ufdtJdby0ZVv5HSwOD7hXuzg7vgW5pTRqTuzMbqTAcdU
XHLIFQ1Li2txni+RMDr3VuB1MsXPPe7iXH2MM/8jWMk5nMGkheHCSFsQkThiR1+7B0AlU+kbi6lO
ITfxXfeEE9zF0D0ED5lXg6e9Ct+OljWaf7Zb19zDIyuVQZqsfoVAKHfFm5iHufK3CStpvTvnCMrE
/n6arZxO5wPnTHRJl3+FEAbjt/qiPFlaZRhAz6M/bjVEn+Duuot+dMD7ZkRt737y3TbB7iMInavw
L+h3ApD8STT1JRAUd+YH7ZSkmOIYJ1uJSbj64uXFT7oRgdOhWFdoVj4EgPpGi9vpSMruPgqFoKgq
oZqNjhkgyL2WaJJ4macUIfJIYjQZFHXhW1Ib26atWrryHkRlVHv0UqNkptfd2Pn2+E9uiiDnLusJ
COVoq3Bclb1YZvHokWrJTD1vnXUtGoDwHa6CWnTMIMJKDFZLLMKI1smDR4eO4ihLLKdr856e910u
j9XiPeLg21S0oeOSVJoc7IZArf6P7WYp6AFgG8zvKOggHUmzUi2sPkLTq0akEn2eSICu2UcvoNwh
hni4JTMf6pTs5RSRtlkjkU3sEHSjmmIyyJPcC/BGCmatF0zBrmvYSmmJ3+sbDzVupfFFeNDB2xym
D/vhd2y+Sd6o+Hhb6ylf2O5+zUJHbx9IvIqonQj+lwyxKZFouQn9ACCEDNUi9jfuVhILw73nvIA9
81kuk1Bd7H7Wfpiio+cNd+GZMfV8rsmWVlqhfa9u5624DB5QXi0FOVl02hGqOJ6wqXq0gE1LkEgI
QBMvPNWvMCOyAOQste1F1fJoIjljfdlDyJRguCi+GzyLHymEai/wY5tO6ncXMRlYrIbCU1AOk4Ar
6b4BRHQw92jAoUi3ni9RL4PJ0Uy3V/zUQJ8U3wqZ056dZVAD5FzmM8IJwbE5a3GPkD6HWhV9A/Qv
KB4zgw/8smK5LqltQ/DvFz98yUHCSdiHv05XJqpG9R6OknBTU854rNR4v938qMSWTbIwWybI4gWz
mKU+4Osa6QTBy/o/XP833hkZSx81RmGnSRloCxo9tTYH8niJmBeHrjzZHkdho1HYEA/B20nWLd2L
vGgmtTUbVE341cix1l4G0/JR6wyiVu4g0vD2N03jnLIbtknUdhSI7mD2dZbkKRiHB1a+PjFE2XBT
0QunlXF2jXktIAfwx96ivqQidIKpAgdBjGKOsEwuaJBueF2eOjZ/2KiFr5U3ESO8jmDzVZFaXx9R
a5P+4hKVl+3vhvKlYwqtPp/Rk5ND8kT8d2hIK7EgkRQ/30csWR/tgF8dWpIOFDrnDwH+rfI6jryC
7xAYwQt0/avvlDynbyupvlNoLlvT16b/dlKPui7z9ir77ttSxRh0rVN2QDjO5oWTvZs/QGcPuUUQ
hyfDbphIAfHrtSv49Wt8PrOg/+BEzn9ihoUwsUepxjwFhMCOj7oJvUMUD/1sDAu7wtKdmwu9OHYS
wkFOmW5mXOkrCbHYtaQErHI+R1aH+58ZvjIErs8wpTxiGBWaRbvWcxLN+CFuJBjE0ylwOSyYUPMb
/yPMmwfZoNyjzvpSiUDSRv/TR7C3d0dylqjBDxCExAagIbq2JkgbgPBtHyiO5iygQ+uHB2GoTsCJ
RJHdxdVLCfTjuzZHWZBCgEnHxJzyiYFLMyWPLrNgabLM3d9Bc1ZPp0TxetGOlfFz++B7+FwTu0Fd
8rhXuuLmN8GH8aRSyiH9y45ZeRkihRY3dPXVZAKfFlLH2YEVa8SKlWlT09wv+zpOPcTuF0TUnEmQ
HDCI99+VV7KywnXYoYDvynH6CvM+rbQOliVZRw858XEdiKpgQsmrMg5PE04NCZb2/Nc4d5+oULeL
jyB+5pQooDIQRRJPD2z+HBCC1es/RwPZnfE0DggN7o/aKhkip5+XF2d+Wqp+AKdcIdnmXeQWFe96
49wv5QxR7zJriIcxoZEIw7I+GrhjIuaeY5qNAUNWDWSi4sFK1fomf1SoBKfRpZNjcKQR20vdAz82
d5wUPj9xnCZmPs4vT/4V3g0v/6BnW0U+6ygix6o0e4977qQ/t6/70jO150n3mjvmN8npW3ZdrncC
BrRwWyjNsjAEg7nSV55y0LNTKu4udJRMX0M0DYf+Os/iL8wmH6m7XCXs70p+bC96HNA1qQIduu6n
WuC9nUlJND6+G37PibqTjGRL9U3Xbzi1/PpuNT2YE1f4u+TCbrb6RnPGGISmmX4C+oY8shDAXyJP
Cl2cdjc+Q57/aihZrEPoE3hfIsL3c3gWVYRfdoVq3vrg3ux46bVNsnWhJdaJ8fXuT2RlWHvyqkYY
xKAEVcsbtL+0sTL3o9euJiqKn1RIOq+/c34Na0XzfSN3gkAmFGfTwixp7J2X8IQKcRPzx/BrUO1C
UhmgIznoxUbENSRZW+OpSPb0k4mU3gxM4B2EKvKk/FDaK9NhqFxIbYO0ACuddS7LBQaOsAfY4f4J
FMH/jJDmBjgAeCwvdE+nBbKlW/8Vop4mAClLYM6zRtOnvFIMYDMnuEZYFDtLRtG2/xkuYsVU8C0m
GtB1JQVXiB2ik+PlgqvlKUaHdb6iV0xA5/Frki4B4CsQ7xIW24TNS/BU+tZ0zv0GxfL9NnChdMfk
gfUmTeWe+QQE7khBs3RHCtLAYXRjVORqEuDrYpXH+LrYDm+LbwdWbBWuqWD/uneSzcOXZEa3T7DJ
Lef4O1vdKcQc+DdBJyZM1+zCm23L0FQ17YtlNswCf4Fsqye4ca7KTrZJ+p6M3NLcuMdl+2Yhvivi
BLptKt6FtYe3KLfrAR3wkbW//U94v8xAPF/izf+MaDaVJ/dBINK8ekl5iPj4BiMvfATU6q1dL3XY
V46n1MM9mFIyzS4293CNBmCfKxIIi9jFMwNsDMmVJM3xoVJYmhFBwUtQJ7DuOZqgA/ZAzNT/I495
49qbfYkbgbzRFIX9xvldoEUxyCy85Z12WS2vCz9G3PA+gnVeqG51xBX+tkUskqaeBndB9g40hrbR
OMNaOQKCQixuLos7c7QW0in9vtmFiZU703p2sHQ9IpSS2K50OZW3UdpWpHjc6uZlBhKZTl09umuz
d8niGRLRr9GZ8r+7Q2/miH056mG+7UE+ZsmALcn2QneJnSivOz1NC3gdg0x+AygLxEZuzYsFKPV2
m12y7FbpTy6EmSgQlm2IsG+0m6Y336iDmrrqkc63dYCh4F0GWL+S2p38Aj4pz0ycXbVfTHz5u07a
hPgnFKoX+Pv088gI4G85nuq4kdI1mdGJy2EV7CJoNVRQcZD19EDlc409bd0bUEY4SDLfJ5QHSChG
XvoCGZmSlA0/K4CVLV1wF/vc4p8FstIMfXWaAGrwb8DCJWz+7aGHJ2Ly8jucz7Y8OHli13d371Fc
l4GaltHx3csNB/CARcvMp5t1w3MQ6hsgxFslvGnNn9WiYM8yu0OWaqCy7ec28SPgXW46DLR8ZWiO
dSM8B4xjF5DXnThANJEggFWbOLDXJk5iNx4MR/Zc+xN/KXAxwrsZ5/DIqYQLqaqxFGvEIyIG65j+
6+a+QVDPJI47cGnOQ2cfA7ISoI4rB4plTDsCMU7brN4rpvMlmus8rDcLDRBdT2+Dc5OkYAZp9M0S
amZoMC597519zC0lCOirGeg0POyp780lNr7ixg10lr/gnYrF9wQ6/QM+/6Nk4z3wKhdKagGrc3Gx
qHa+ws5F8pjoRqspYFDwXg2pJM4B/0zFYG8TAEYbriKkCx7rzrcjyZURTHjXOalURs8/h7kj313x
jtRGh4KkzpcY/F1RtIXIh1isOdqTgmbSl8opRgCo9hbbHCWWFK8Kvn0byAuYgqXof9aPSEEnY6jb
koW3TlAIVZa8K3loU3fG45qrQQv/LIB+iWuVmGNtrg88v0vZr0ocub0NtMNOpfyI2/Ti0rj9Xos7
MUm00ary0EbkY+3rJKdNMH3PqNSFcv3XkIHYyiiad4xnQdozoPihLhHowQKr0JLr8aw5w8AhcQSy
2GURbIt+9PLsjqAxHIWH9eG8Wh0uLm7k5JcLK7T9e7oC+BhDsCEdiy26zOzMp2wRkoSBlxL280f5
xovpqDh2DZcqDe4WIuFJRTMx1gfjsR43nD9EeMpkeOihuOoP+bR3GOKkFtVssQIp0/toGhc3CXZR
MOAFu6cLv31rljZzKi73UrSAHqe6fLkitr4P+w3alYrc1Z5lbanFcbwl9xS4RZs/qfFcb8GTlvOA
TctLaTOFYjiKCMI3i/iq/qZz33nAQBg0LHgAeJJIt2ynuUp9/XIK0RsJOKusJ1SnxElRS63uDo9U
l0VP8p7aP/KGJuKAfh9D97LeiAooq7f2OK+Pd4RfoBAfWBK5w17uf/rV9hwbx3iJZZBtONnnePzG
WT717yoeDzJ5wqcn6fwdSHKPhm6kGILxKewvwjseV0eBRmX8DT56uz77jP80sNIz+WsYfuIzBq1F
Cd9JL1d5tNNKQyWQwgHjewb+W/dPSu6ltLhoedZ4M/STaVlIwJaHHzJjMKIKEnuwlGsGJxarZv7s
6q5+cTv0IKXKtmMwa3wiZdUqzabWwHtB3FUpX+wjb0kJayt9uCoDBb/kg5guRk30flhSGvsiojzO
AQVaSaIazoZOooDVnbRdoT4AjPhSWpwuseZ1vjbjWPlrcDlVh30wOJ+XDl+mh1JLqX5uk4dNPyM4
oxAl9reIM9RFWTQRZHAi2YmNw5zuPj1cfs8NiAuVSyrQEUzKZuFlOFuFLJ2U4EGDW+cSo34SRHjZ
2Z1o6yG1zNTL99O8juDrzvM8vZJmg4xd0bRwngOWXQVmiJzpDM2IQhODEcnT+PIB3L/6+GPXZYAv
bbuQK61PWoNO58BHGPO9cO8c7GoTPt62YMHwaH4UE2dio09JoGDcd9o0KIIZVklJjUuuzYlMIAt3
iRBQekENeWfON4XC6u5tj2+1BuJFbp1m4qDqzerQgBHi9PHKA7YRDF0VvvFdW09jEn9Wci+VYOeI
26xeKiN7GpM5lgv2q9vWHmvW4UHV8phsUvvtbjMTWje4sMNcRDjHZwrTUNSF0PCTAYTktuH6/3E2
6hKUtAiP/pvAtqwSkUEYgrtWteSvaAq1y3Tr7vZHkcLAzdZXzCEW0Kmdyrp5Iz7ivG8Bd5a62BDV
7alTFVWqFxaKH410UualUWANRt9G7k6s6GwnUMroDdVeFNq+9hLAuS13Nmud/5Vz7qH9vzWEFgmb
45BnMHHfRqPDvyrh5OeAOmGgJmpc3cLbP43N2OY1rJz99Daq1zH0fHj+m3P6GUGINENwwhfYaXTp
5uvrsaemgGJWzZ26gp2QSTMOICC2LtJUZE6ZMztQhjdTL+to4Rkpdb5Qo0VIjQAKBMdhG+bsDVEQ
hu4c8WulOLCgAAzQ6RNMS93kBtZKdtkf37Yr/RCqwiZTN+FDl/MsYzYhBK5/RKMANPbWaLi6nidc
5wnnYt+tlRxuQNJWxzp5iP8Fw6MLgAq07GCHZepZgOnNl1XVoBmJIquTHOwP60STgH0ILsruDe9Y
EygnOwoh4ST8XgdLsewCW+TyN6eyiOlxsPOZDRPrRqtRNBRBeR1jfN0qOlq8go2saoZPig3fJ0te
1zaj+KBUlVBIRmEFwcVVZUDKcCWV5aMjHiDjNGk8UGEMGaVOYvUkfyinJWk7n6hIJpvfEdqn+rVv
ubhj8afv075+MrxH2iQ8wkaIT4w7hu96KCPFGLQ8TvH1Hy2yKQFDErddluXlRFikz55ptAOy8lBx
jZIIz5kyN3hucSWwqd+dzsszKYKZ9+7OFiLQhU59E3gEqTkt9357R2LNLPQsPzPAmjoQOzw8sHCE
OwYUMdDkWDY9FntC8vRENStJyvFJp+Nxj0lwFdg4RbDZ6t48+93UTjEmdzVXOix+MOVYJmBsdKnf
0HTJBskbJu/HGGYBK5NyBiAN9UuRHv6SKj0ke1wxtH5qIcBybJUyASD2yeF5u0TIH90y5FiumFfI
UspK7uLE/pyxlbU8G4TCEFxFZMBdZJDSVYSLkrE9Fl00U1+VESI0ZTblV5ftyfA3OW3npGj4LDr7
SNRDPvQx8ihu3emHmJApsWZD/YSIYJQ5tXYzmpQjHCjNtSlo9fKJ1ml32dSp6T0JyqfsoptEBgXG
EUTvUzloRlIZ3I2SZ6hK6U90pBZQj937xmGrU75b0TrK/9tLkO68XOhV+sp+rgQxK3xVOA==
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity dist_mem_gen_0 is
  port (
    a : in STD_LOGIC_VECTOR ( 7 downto 0 );
    spo : out STD_LOGIC_VECTOR ( 15 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of dist_mem_gen_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of dist_mem_gen_0 : entity is "dist_mem_gen_0,dist_mem_gen_v8_0_13,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of dist_mem_gen_0 : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of dist_mem_gen_0 : entity is "dist_mem_gen_v8_0_13,Vivado 2023.1";
end dist_mem_gen_0;

architecture STRUCTURE of dist_mem_gen_0 is
  signal NLW_U0_dpo_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_U0_qdpo_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_U0_qspo_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  attribute C_FAMILY : string;
  attribute C_FAMILY of U0 : label is "artix7";
  attribute C_HAS_D : integer;
  attribute C_HAS_D of U0 : label is 0;
  attribute C_HAS_DPO : integer;
  attribute C_HAS_DPO of U0 : label is 0;
  attribute C_HAS_DPRA : integer;
  attribute C_HAS_DPRA of U0 : label is 0;
  attribute C_HAS_I_CE : integer;
  attribute C_HAS_I_CE of U0 : label is 0;
  attribute C_HAS_QDPO : integer;
  attribute C_HAS_QDPO of U0 : label is 0;
  attribute C_HAS_QDPO_CE : integer;
  attribute C_HAS_QDPO_CE of U0 : label is 0;
  attribute C_HAS_QDPO_CLK : integer;
  attribute C_HAS_QDPO_CLK of U0 : label is 0;
  attribute C_HAS_QDPO_RST : integer;
  attribute C_HAS_QDPO_RST of U0 : label is 0;
  attribute C_HAS_QDPO_SRST : integer;
  attribute C_HAS_QDPO_SRST of U0 : label is 0;
  attribute C_HAS_WE : integer;
  attribute C_HAS_WE of U0 : label is 0;
  attribute C_MEM_TYPE : integer;
  attribute C_MEM_TYPE of U0 : label is 0;
  attribute C_PIPELINE_STAGES : integer;
  attribute C_PIPELINE_STAGES of U0 : label is 0;
  attribute C_QCE_JOINED : integer;
  attribute C_QCE_JOINED of U0 : label is 0;
  attribute C_QUALIFY_WE : integer;
  attribute C_QUALIFY_WE of U0 : label is 0;
  attribute C_REG_DPRA_INPUT : integer;
  attribute C_REG_DPRA_INPUT of U0 : label is 0;
  attribute c_addr_width : integer;
  attribute c_addr_width of U0 : label is 8;
  attribute c_default_data : string;
  attribute c_default_data of U0 : label is "0";
  attribute c_depth : integer;
  attribute c_depth of U0 : label is 256;
  attribute c_elaboration_dir : string;
  attribute c_elaboration_dir of U0 : label is "./";
  attribute c_has_clk : integer;
  attribute c_has_clk of U0 : label is 0;
  attribute c_has_qspo : integer;
  attribute c_has_qspo of U0 : label is 0;
  attribute c_has_qspo_ce : integer;
  attribute c_has_qspo_ce of U0 : label is 0;
  attribute c_has_qspo_rst : integer;
  attribute c_has_qspo_rst of U0 : label is 0;
  attribute c_has_qspo_srst : integer;
  attribute c_has_qspo_srst of U0 : label is 0;
  attribute c_has_spo : integer;
  attribute c_has_spo of U0 : label is 1;
  attribute c_mem_init_file : string;
  attribute c_mem_init_file of U0 : label is "dist_mem_gen_0.mif";
  attribute c_parser_type : integer;
  attribute c_parser_type of U0 : label is 1;
  attribute c_read_mif : integer;
  attribute c_read_mif of U0 : label is 1;
  attribute c_reg_a_d_inputs : integer;
  attribute c_reg_a_d_inputs of U0 : label is 0;
  attribute c_sync_enable : integer;
  attribute c_sync_enable of U0 : label is 1;
  attribute c_width : integer;
  attribute c_width of U0 : label is 16;
  attribute is_du_within_envelope : string;
  attribute is_du_within_envelope of U0 : label is "true";
begin
U0: entity work.dist_mem_gen_0_dist_mem_gen_v8_0_13
     port map (
      a(7 downto 0) => a(7 downto 0),
      clk => '0',
      d(15 downto 0) => B"0000000000000000",
      dpo(15 downto 0) => NLW_U0_dpo_UNCONNECTED(15 downto 0),
      dpra(7 downto 0) => B"00000000",
      i_ce => '1',
      qdpo(15 downto 0) => NLW_U0_qdpo_UNCONNECTED(15 downto 0),
      qdpo_ce => '1',
      qdpo_clk => '0',
      qdpo_rst => '0',
      qdpo_srst => '0',
      qspo(15 downto 0) => NLW_U0_qspo_UNCONNECTED(15 downto 0),
      qspo_ce => '1',
      qspo_rst => '0',
      qspo_srst => '0',
      spo(15 downto 0) => spo(15 downto 0),
      we => '0'
    );
end STRUCTURE;
