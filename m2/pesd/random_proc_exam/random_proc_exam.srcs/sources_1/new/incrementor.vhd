library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity incrementor is
    Port ( I0 : in STD_LOGIC_VECTOR (15 downto 0);
           I1 : in STD_LOGIC_VECTOR (15 downto 0);
           S0 : in STD_LOGIC;
           S1 : in STD_LOGIC;
           O : out STD_LOGIC_VECTOR (15 downto 0));
end incrementor;

architecture Behavioral of incrementor is
    signal interm: std_logic_vector(15 downto 0);
begin 

    with S1 select interm <= 
        I1 + '1' when '1',
        I1       when others;
      
    
    with S0 select O <= 
        I0     when '1',
        interm when others;
        
end Behavioral;
