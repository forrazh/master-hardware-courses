----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/25/2023 11:22:14 AM
-- Design Name: 
-- Module Name: alu - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity alu is
    GENERIC (NB_ITER_RND : INTEGER := 1; RAND_START : INTEGER := 0);

    
    Port ( src1 : in STD_LOGIC_VECTOR (15 downto 0);
           src2 : in STD_LOGIC_VECTOR (15 downto 0);
           op : in STD_LOGIC_VECTOR (3 downto 0);
           clk : in STD_LOGIC;
           carry : out STD_LOGIC;
           dest : out STD_LOGIC_VECTOR (15 downto 0));
end alu;

architecture Behavioral of alu is
component random is
    GENERIC (TO_SKIP : INTEGER := 1; RAND_START : INTEGER := 0);

    Port ( clk : in STD_LOGIC;
           CE : in STD_LOGIC;
           Q : out STD_LOGIC_VECTOR (15 downto 0));
end component;

component mult_gen_0 IS
  PORT (
    A : IN STD_LOGIC_VECTOR(6 DOWNTO 0);
    B : IN STD_LOGIC_VECTOR(6 DOWNTO 0);
    P : OUT STD_LOGIC_VECTOR(13 DOWNTO 0)
  );
END component;


    constant code_SUBC   : STD_LOGIC_VECTOR := b"0000"; 
    constant code_ADD    : STD_LOGIC_VECTOR := b"0001"; 
    constant code_SUB    : STD_LOGIC_VECTOR := b"0010"; 
    constant code_ADDC   : STD_LOGIC_VECTOR := b"0011"; 
    constant code_INV    : STD_LOGIC_VECTOR := b"0100"; 
    constant code_AND    : STD_LOGIC_VECTOR := b"0101"; 
    constant code_OR     : STD_LOGIC_VECTOR := b"0110"; 
    constant code_INC    : STD_LOGIC_VECTOR := b"0111"; 
    constant code_CPL2   : STD_LOGIC_VECTOR := b"1000"; 
    constant code_CONCAT : STD_LOGIC_VECTOR := b"1001"; 
    constant code_ID     : STD_LOGIC_VECTOR := b"1010";
    constant code_RAND   : STD_LOGIC_VECTOR := b"1011";
    constant code_MULT   : STD_LOGIC_VECTOR := b"1100";
    constant code_SHR8   : STD_LOGIC_VECTOR := b"1101";
    constant code_LE     : STD_LOGIC_VECTOR := b"1110";
    constant code_PIXY: STD_LOGIC_VECTOR := b"1111";

    signal interm   : STD_LOGIC_VECTOR (16 downto 0) := (others => '0');
    signal inverted : STD_LOGIC_VECTOR (15 downto 0) := (others => '0');
    signal rnd_i    : STD_LOGIC_VECTOR (15 downto 0) := (others => '0');
    signal mul_i    : STD_LOGIC_VECTOR (13 downto 0) := (others => '0');
    signal pow2_i   : STD_LOGIC_VECTOR (13 downto 0) := (others => '0');
    signal le_i     : STD_LOGIC_VECTOR (15 downto 0) := (others => '0');
    
    signal rand_pixy : std_logic_vector (15 downto 0) := (others => '0');
    signal x2_i, y2_i: STD_LOGIC_VECTOR (13 downto 0) := (others => '0');
    signal d_i     : STD_LOGIC_VECTOR (15 downto 0) := (others => '0');

    constant PIXY_COMP : STD_LOGIC_VECTOR := x"3F01";
    
    signal enable : STD_LOGIC := '0';
    signal pixy_out : STD_LOGIC := '0';
begin
    with op select enable <= 
        '1' when code_RAND,
        '1' when code_PIXY,
        '0' when others;
        
    rand : random GENERIC MAP(TO_SKIP=>NB_ITER_RND, RAND_START=>RAND_START) PORT MAP (clk=>clk, CE=>enable, Q=>rnd_i);
    -- mult : mult_gen_0 PORT MAP (A=>src1(6 downto 0), B => src2(6 downto 0), P => mul_i);
    pow2 : mult_gen_0 PORT MAP (A=>src1(6 downto 0), B => src1(6 downto 0), P => pow2_i);
    
    x2 : mult_gen_0 PORT MAP (A=>rnd_i(6 downto 0), B => rnd_i(6 downto 0), P => x2_i);
    y2 : mult_gen_0 PORT MAP (A=>rnd_i(14 downto 8), B => rnd_i(14 downto 8), P => y2_i);
    
    d_i <= '0' &  (('0'&x2_i) + ('0'&y2_i));
    pixy_out <= '1' when d_i <= PIXY_COMP else '0';
     
    le_i <= x"0001" when src2 >= src1 else x"0000";

    inverted <= not src1;
    with op select interm <=
        ('0' & src1) - ('0' & src2) - x"0001"      when code_SUBC,
        ('0' & src1) + ('0' & src2)                when code_ADD,
        ('0' & src1) - ('0' & src2)                when code_SUB,
        ('0' & src1) + ('0' & src2) + x"0001"      when code_ADDC,
        '0' & inverted                             when code_INV,
        ('0' & src1) and ('0' & src2)              when code_AND,
        ('0' & src1) or ('0' & src2)               when code_OR,
        ('0' & src1) + '1'                         when code_INC,
        '0' & (inverted + '1')                     when code_CPL2,
        '0' & src1(7 downto 0) & src2 (7 downto 0) when code_CONCAT,
        '0' & src1                                 when code_ID,
        -- '0' & rnd_i                                when code_RAND,
        '0' & '0' & '0' & mul_i                    when code_MULT, 
        '0' & x"00" & src1(15 downto 8)            when code_SHR8,
        '0' & le_i                                 when code_LE,
        ('0' & src1) + (x"0000" & pixy_out)        when code_PIXY, 
        '0' & x"0000"                              when others;

    carry <= interm(16);
    dest <= interm(15 downto 0);    
end Behavioral;
