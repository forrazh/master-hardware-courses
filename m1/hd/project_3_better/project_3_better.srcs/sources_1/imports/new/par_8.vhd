----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/14/2023 03:32:13 PM
-- Design Name: 
-- Module Name: par_8 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity par_8 is
    Port ( word : in STD_LOGIC_VECTOR (7 downto 0);
           SUM : out STD_LOGIC);
end par_8;

architecture Behavioral of par_8 is

    signal count_0, count_1, count_2, count_3 : std_logic;
    signal mid_count_0, mid_count_1 : std_logic;
begin -- tree xor
    count_0 <= word(0) xor word(4);
    count_1 <= word(1) xor word(5);
    count_2 <= word(2) xor word(6);
    count_3 <= word(3) xor word(7);
    
    mid_count_0 <= count_0 xor count_1;
    mid_count_1 <= count_2 xor count_3;
    
    SUM <= mid_count_0 xor mid_count_1;
end Behavioral;

--begin
    
--    process (word)
--    variable par : std_logic;
--    begin
--        par := '0';
--        for I in 0 to 7 loop
--            par := par XOR word(I);
--        end loop;
--        SUM <= par;
--    end process;
--end Behavioral;
