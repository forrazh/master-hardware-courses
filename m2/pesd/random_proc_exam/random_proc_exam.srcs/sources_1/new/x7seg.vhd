----------------------------------------------------------------------------------

-- Company: 

-- Engineer: 

-- 

-- Create Date:    10:48:25 05/18/2016 

-- Design Name: 

-- Module Name:    x7seg - Behavioral 

-- Project Name: 

-- Target Devices: 

-- Tool versions: 

-- Description: 

--

-- Dependencies: 

--

-- Revision: 

-- Revision 0.01 - File Created

-- Additional Comments: 

--

----------------------------------------------------------------------------------

library IEEE;

use IEEE.STD_LOGIC_1164.ALL;


-- Uncomment the following library declaration if using

-- arithmetic functions with Signed or Unsigned values

--use IEEE.NUMERIC_STD.ALL;


-- Uncomment the following library declaration if instantiating

-- any Xilinx primitives in this code.

--library UNISIM;

--use UNISIM.VComponents.all;


entity x7seg is

    Port ( sw : in  STD_LOGIC_VECTOR (3 downto 0);

           sevenseg : out  STD_LOGIC_VECTOR (6 downto 0));

end x7seg;


architecture Behavioral of x7seg is


begin

with sw select

sevenseg <=

            "1000000" when x"0" ,

            "1111001" when x"1" ,

            "0100100" when x"2" ,

            "0110000" when x"3" ,

            "0011001" when x"4" ,

            "0010010" when x"5" ,

            "0000010" when x"6" ,

            "1111000" when x"7" ,

            "0000000" when x"8" ,

            "0010000" when x"9" ,

            "0001000" when x"A" ,

            "0000011" when x"B" ,

            "1000110" when x"C" ,

            "0100001" when x"D" ,

            "0000110" when x"E" ,

            "0001110" when others; 


end Behavioral;

----------------------------------------------------------------------------------

-- Company: 

-- Engineer: 

-- 

-- Create Date:    11:15:49 09/27/2016 

-- Design Name: 

-- Module Name:    E190 - Behavioral 

-- Project Name: 

-- Target Devices: 

-- Tool versions: 

-- Description: 

--

-- Dependencies: 

--

-- Revision: 

-- Revision 0.01 - File Created

-- Additional Comments: 

--

----------------------------------------------------------------------------------

library IEEE;

use IEEE.STD_LOGIC_1164.ALL;

use IEEE.STD_LOGIC_unsigned.ALL;


-- Uncomment the following library declaration if using

-- arithmetic functions with Signed or Unsigned values

--use IEEE.NUMERIC_STD.ALL;


-- Uncomment the following library declaration if instantiating

-- any Xilinx primitives in this code.

--library UNISIM;

--use UNISIM.VComponents.all;

entity E190 is
    Port ( 
        clk   : in  STD_LOGIC;            
        -- reset : in  STD_LOGIC;            
        E190, clk190 : out  STD_LOGIC
    );
end E190;

architecture Behavioral of E190 is
    signal clkin: std_logic :='0';
begin
--clock divider     
    process(clk)     
        variable q: std_logic_vector(23 downto 0):= X"000000";     
    begin            
        -- if reset ='1' then             
        if q =X"FFFFFF" then             
            q := X"000000";             
            clkin <= '0';         
        elsif rising_edge(clk) then             
            q := q+1;             
            if Q(18)='1' and clkin='0' then    
                E190 <= '1' ;      
            else           
                E190 <= '0';      
            end if;         
        end if;         
        clkin<= Q(18);     
    end process;     
    clk190 <= clkin;
end Behavioral;

library IEEE;

use IEEE.STD_LOGIC_1164.ALL;


entity afficheur is

    Port ( data : in  STD_LOGIC_VECTOR (15 downto 0);

           byte : out  STD_LOGIC_VECTOR (3 downto 0);

           anodes : out  STD_LOGIC_VECTOR (3 downto 0);

           clk : in  STD_LOGIC);

end afficheur;


architecture Behavioral of afficheur is

  type state_type is (digit1, digit2, digit3, digit4); 

   signal state, next_state : state_type := digit1; 

   --Declare internal signals for all outputs of the state-machine

   signal anodes_i : std_logic_vector(3 downto 0);  

	signal byte_i : std_logic_vector (3 downto 0);

   --other outputs

 

begin

   SYNC_PROC: process (clk)

   begin

      if (clk'event and clk = '1') then      

            state <= next_state;

            anodes<= anodes_i;

				byte <= byte_i;   

      end if;

   end process;

 

   --MOORE State-Machine - Outputs based on state only

   OUTPUT_DECODE: process (state,data)

   begin

      --insert statements to decode internal output signals

      --below is simple example

      case state is

			when digit1 =>

				anodes_i<="1110";

				byte_i<=data (3 downto 0);

			when digit2 =>

				anodes_i<="1101";

				byte_i<=data (7 downto 4);

			when digit3 =>

				anodes_i<="1011";

				byte_i<=data (11 downto 8);

			when digit4 =>

				anodes_i<="0111";

				byte_i<=data (15 downto 12);

			when others =>

				anodes_i <= "1111";

				byte_i<="0000";

			end case;

				

   end process;

 

   NEXT_STATE_DECODE: process (state)

   begin


      next_state <= state;  


      case (state) is

         when digit1 =>

              next_state <= digit2;

         when digit2 =>

              next_state <= digit3;

			when digit3 =>

              next_state <= digit4;

			when digit4 =>

              next_state <= digit1;


         when others =>

            next_state <= digit1;

      end case;      

   end process;



end Behavioral;