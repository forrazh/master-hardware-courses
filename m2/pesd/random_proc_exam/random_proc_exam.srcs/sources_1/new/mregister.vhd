----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/11/2023 08:54:04 AM
-- Design Name: 
-- Module Name: mregister - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity mregister is
  Port ( 
         d: in STD_LOGIC_VECTOR(15 downto 0);
         R2B: in STD_LOGIC;
         B2R : in STD_LOGIC;
         clk : in STD_LOGIC;
         q_in: out STD_LOGIC_VECTOR(15 downto 0);
         q: out STD_LOGIC_VECTOR(15 downto 0)
       );
end mregister;

architecture Behavioral of mregister is
    signal I: std_logic_vector(15 downto 0) := x"0000";
begin
process (clk)
begin
    if rising_edge(clk) then 
        if B2R = '1' then
           I <= d;
        end if;
    end if;
end process;
    q <= I when R2B = '1' else "ZZZZZZZZZZZZZZZZ";
    q_in <= I;
end Behavioral;
