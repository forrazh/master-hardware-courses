----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/03/2023 11:13:00 PM
-- Design Name: 
-- Module Name: clock_2_secs - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity clock_2_secs is
 Port ( 
        clk190 : in  STD_LOGIC;            
        clk2   : out  STD_LOGIC
    );
end clock_2_secs;

architecture Behavioral of clock_2_secs is

begin
    process(clk190)
        variable counter : unsigned(9 downto 0) := (others => '0');
        -- variable l_started : std_logic := '0';
    begin
    
    -- if (START = '1') then
    --     l_started := '1';
    --     counter   := b"0000000000";
    -- end if;
        if (rising_edge(clk190)) then
            counter := counter + 1;
            if counter = x"17C" then
                -- if (l_started = '1') then
                    clk2 <= '1';
                counter   := b"0000000000";
                -- l_started := '0';
            else
                clk2 <= '0';
            end if;
        end if;
    end process;

end Behavioral;
