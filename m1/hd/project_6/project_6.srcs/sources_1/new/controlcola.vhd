----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/04/2023 03:10:47 PM
-- Design Name: 
-- Module Name: controlcola - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity controlcola is
    Port (
        piece20, piece50, piece100 : in STD_LOGIC;
        reset : in std_logic;
        clk : in STD_LOGIC;
        euro_in : out STD_LOGIC_VECTOR (15 downto 0);
        canette, retour_euro : out STD_LOGIC
    );
end controlcola;

architecture Behavioral of controlcola is
   type state_type is (st1_empty, st2_20c, st3_40c, st4_60c, st5_80c, st6_50c, st7_100c, st8_waiting_for_reset);
   signal state, next_state : state_type;

   -- find if we can handle it through another method
   -- Maybe return a single bit with the base result 
   -- like we did with the special displayer ?
   constant ERROR_VALUE : STD_LOGIC_VECTOR  (15 downto 0) := x"FFFF";
   constant LED_OFF : STD_LOGIC := '0'; 
   constant LED_ON  : STD_LOGIC := '1'; 

   constant NO_MONEY : STD_LOGIC_VECTOR  (15 downto 0) := x"0000";
   constant CENTS_20 : STD_LOGIC_VECTOR  (15 downto 0) := x"0020";
   constant CENTS_40 : STD_LOGIC_VECTOR  (15 downto 0) := x"0040";
   constant CENTS_60 : STD_LOGIC_VECTOR  (15 downto 0) := x"0060";
   constant CENTS_80 : STD_LOGIC_VECTOR  (15 downto 0) := x"0080";
   constant CENTS_50 : STD_LOGIC_VECTOR  (15 downto 0) := x"0050";
   constant CENTS_100: STD_LOGIC_VECTOR  (15 downto 0) := x"0100";

   signal canette_i, retour_euro_i : STD_LOGIC := '0';
   signal euro_in_i : STD_LOGIC_VECTOR (15 downto 0) := x"0000";

begin
   SYNC_PROC: process (clk)
   begin
      if (rising_edge(clk)) then
         if (reset = '1') then
            state <= st1_empty;
         else
            state <= next_state;
         end if;
         euro_in <= euro_in_i;
         retour_euro <= retour_euro_i;
         canette <= canette_i;
      end if;
   end process;

   --MOORE State-Machine - Outputs based on state only
   OUTPUT_DECODE: process (state)
   begin

    euro_in_i <= NO_MONEY;
    canette_i <= LED_OFF;
    retour_euro_i <= LED_OFF;


    case state is 
        when st2_20c => euro_in_i <= CENTS_20;
        when st3_40c => euro_in_i <= CENTS_40;
        when st4_60c => euro_in_i <= CENTS_60;
        when st5_80c => euro_in_i <= CENTS_80;
        when st6_50c => euro_in_i <= CENTS_50;
        when st7_100c => 
            euro_in_i <= CENTS_100;
            canette_i <= LED_ON;
        when st8_waiting_for_reset => retour_euro_i <= LED_ON;
        when others => euro_in_i <= NO_MONEY;
    end case;
   end process;

    NEXT_STATE_DECODE: process (state, piece20, piece50, piece100)
    begin
        next_state <= state;  

        case state is 
            when st1_empty =>
                if piece20 = '1' then
                    next_state <= st2_20c;
                elsif piece50 = '1' then 
                    next_state <= st6_50c;
                elsif piece100 = '1' then 
                    next_state <= st7_100c;
                end if;
            when st2_20c =>
                if piece20 = '1' then 
                    next_state <= st3_40c;
                elsif piece50 = '1' or piece100 = '1' then
                    next_state <= st8_waiting_for_reset;
                end if;
            when st3_40c =>
                if piece20 = '1' then 
                    next_state <= st4_60c;
                elsif piece50 = '1' or piece100 = '1' then
                    next_state <= st8_waiting_for_reset;
                end if;
            when st4_60c => 
                if piece20 = '1' then 
                    next_state <= st5_80c;
                elsif piece50 = '1' or piece100 = '1' then
                    next_state <= st8_waiting_for_reset;
                end if;
            when st5_80c => 
                if piece20 = '1' then 
                    next_state <= st7_100c;
                elsif piece50 = '1' or piece100 = '1' then
                    next_state <= st8_waiting_for_reset;
                end if;
            when st6_50c =>
                if piece50 = '1' then
                    next_state <= st7_100c;
                elsif piece20 = '1' or piece100 = '1' then
                    next_state <= st8_waiting_for_reset;
                end if;
            when others =>
                if piece20 = '1' or piece50 = '1' or piece100 = '1' then
                    next_state <= st8_waiting_for_reset;
                end if;
        end case;
   end process;

				
			
end Behavioral;
