----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/03/2023 11:23:00 PM
-- Design Name: 
-- Module Name: led_displayer - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity led_displayer is
    Port ( default_clk : in STD_LOGIC;
           clk190, e190 : in STD_LOGIC;
           input : in STD_LOGIC_VECTOR (1 downto 0);
           output : out STD_LOGIC_VECTOR (7 downto 0));
end led_displayer;

architecture Behavioral of led_displayer is
    component clock_2_secs is
        Port ( 
                clk190 : in  STD_LOGIC;            
                clk2   : out  STD_LOGIC
     -- change_clk: clock_2_secs PORT MAP (clk190 => clk190, clk2 => clk2);
    
    -- pulse: btn_pulse port map (button=>intermediate_displaying_crawler, clk=>clk2, E=>e190, button_out=>displaying_crawler);
    -- process(clk2)
    --     variable displ : std_logic := '0';
    -- begin
    --     if rising_edge(clk2) then
    --         if input = "10" then 
    --             displ := '1';
    --         else
    --             displ := '0';
    --         end if;
    --     end if;
    --     intermediate_displaying_crawler <= displ;
    -- end process;           );
    end component;

    component btn_pulse is
        Port ( 
            button : in STD_LOGIC;
            E : in STD_LOGIC;
            clk : in STD_LOGIC;
            button_out : out STD_LOGIC
        );
    end component;
    signal intermediate_displaying_crawler : STD_LOGIC := '0';
    signal displaying_crawler : STD_LOGIC := '0';

    signal clk2 : STD_LOGIC := '0';
    signal not_gonna_stop_it : STD_LOGIC_VECTOR (7 downto 0) := "00000000";
begin
    -- change_clk: clock_2_secs PORT MAP (clk190 => clk190, clk2 => clk2);
    
    -- pulse: btn_pulse port map (button=>intermediate_displaying_crawler, clk=>clk2, E=>e190, button_out=>displaying_crawler);
    -- process(clk2)
    --     variable displ : std_logic := '0';
    -- begin
    --     if rising_edge(clk2) then
    --         if input = "10" then 
    --             displ := '1';
    --         else
    --             displ := '0';
    --         end if;
    --     end if;
    --     intermediate_displaying_crawler <= displ;
    -- end process;

    process 
        variable going_out: STD_LOGIC_VECTOR(7 downto 0);
    begin
        -- if displaying_crawler = '1' then
        if input = "10" then
            not_gonna_stop_it <= "11111111";
        elsif input = "01" then
            not_gonna_stop_it <= "00000001";
        else 
            not_gonna_stop_it <= "00000000";
        end if;
    end process;

    output <= not_gonna_stop_it;

end Behavioral;
