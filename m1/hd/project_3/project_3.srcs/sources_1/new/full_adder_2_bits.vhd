
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity full_adder_2bits is
    Port ( A, B, C, D : in STD_LOGIC;
           S0, S1 : out STD_LOGIC;
           cout : out STD_LOGIC);
end full_adder_2bits;

architecture Behavioral of full_adder_2bits is
    component full_adder_1bit
        Port(
            a, b, cin : in std_logic;
            s, cout  : out std_logic
        );
     end component;

begin
    S0 <= A xor B;
    fa: full_adder_1bit port map (a=>C, b=>D, cin=>(A and B), s=>S1, cout=>cout);
end Behavioral;
