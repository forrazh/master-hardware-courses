----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/07/2023 05:40:10 PM
-- Design Name: 
-- Module Name: operator_dispatcher - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity operator_dispatcher is
    Port ( word_1 : in STD_LOGIC_VECTOR (3 downto 0);
           word_2 : in STD_LOGIC_VECTOR (3 downto 0);
           SUM : out STD_LOGIC_VECTOR (4 downto 0);
           operator : in STD_LOGIC_VECTOR (1 downto 0));
end operator_dispatcher;

architecture Behavioral of operator_dispatcher is
    component add_4 
         port (
            word_1, word_2 : in std_logic_vector (3 downto 0);
            SUM : out std_logic_vector (4 downto 0)
         );
    end component;

    component and_4 
         port (
            word_1, word_2 : in std_logic_vector (3 downto 0);
            SUM : out std_logic_vector (4 downto 0)
         );
    end component;
    
    component or_4 
         port (
            word_1, word_2 : in std_logic_vector (3 downto 0);
            SUM : out std_logic_vector (4 downto 0)
         );
    end component;
        
    component xor_4 
         port (
            word_1, word_2 : in std_logic_vector (3 downto 0);
            SUM : out std_logic_vector (4 downto 0)
         );
    end component;
    
    signal ADD_RES, AND_RES, OR_RES, XOR_RES : std_logic_vector (4 downto 0);
begin
    add_task: add_4 port map ( word_1=>word_1, word_2=>word_2, SUM=>ADD_RES);
    and_task: and_4 port map ( word_1=>word_1, word_2=>word_2, SUM=>AND_RES);
    or_task :  or_4 port map ( word_1=>word_1, word_2=>word_2, SUM=>OR_RES);
    xor_task: xor_4 port map ( word_1=>word_1, word_2=>word_2, SUM=>XOR_RES);
    
   with operator select
        SUM <= ADD_RES when "00",
               AND_RES when "01",
                OR_RES when "10",
               XOR_RES when others;

end Behavioral;
