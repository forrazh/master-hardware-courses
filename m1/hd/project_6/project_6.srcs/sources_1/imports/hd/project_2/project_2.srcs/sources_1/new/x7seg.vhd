----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/02/2023 05:06:56 PM
-- Design Name: 
-- Module Name: x7seg - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity x7seg is
    Port ( input_data : in STD_LOGIC_VECTOR (3 downto 0);
           output_seg : out STD_LOGIC_VECTOR (6 downto 0) );
end x7seg;

architecture Behavioral of x7seg is

begin
    with input_data select
        output_seg <= b"1000000" when x"0",
               b"1111001" when x"1",
               b"0100100" when x"2",
               b"0110000" when x"3",
               b"0011001" when x"4",
               b"0010010" when x"5",
               b"0000010" when x"6",
               b"1111000" when x"7",
               b"0000000" when x"8",
               b"0011000" when x"9",
               b"0001000" when x"A",
               b"0000011" when x"B",
               b"1000110" when x"C",
               b"0100001" when x"D",
               b"0000110" when x"E",
               b"0001110" when x"F",
               b"0111111" when others;

end Behavioral;
