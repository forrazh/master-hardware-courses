----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/16/2023 03:31:11 PM
-- Design Name: 
-- Module Name: full_adder_4bits - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity full_adder_4bits is
    Port ( A, B : in STD_LOGIC_VECTOR (3 downto 0);
           cin : in STD_LOGIC;
           S : out STD_LOGIC_VECTOR (2 downto 0);
           cout : out STD_LOGIC);
end full_adder_4bits;

architecture Behavioral of full_adder_4bits is
    component full_adder_1bit
        Port(
            a, b, cin : in std_logic;
            s, cout  : out std_logic
        );
     end component;
    component full_adder_2bits is
        Port ( A, B, C, D : in STD_LOGIC;
               S0, S1 : out STD_LOGIC;
               cout : out STD_LOGIC);
    end component;
    signal l0, l1, lout : STD_LOGIC;
    signal r0, r1, rout : STD_LOGIC;
    signal c0, c1 : STD_LOGIC;
begin
    fa2_0: full_adder_2bits port map (A=>A(0),B=>A(1),C=>A(2),D=>A(3),S0=>l0,S1=>l1,cout=>lout);
    fa2_1: full_adder_2bits port map (A=>B(0),B=>B(1),C=>B(2),D=>B(3),S0=>r0,S1=>r1,cout=>rout);
    
    fa1_0: full_adder_1bit port map (a=>l0, b=>r0,cin=>'0',s=>S(0), cout=>c0);
    fa1_1: full_adder_1bit port map (a=>l1, b=>r1,cin=>c0,s=>S(1), cout=>c1);
    fa1_2: full_adder_1bit port map (a=>lout, b=>rout,cin=>c1,s=>S(2), cout=>cout);
end Behavioral;
