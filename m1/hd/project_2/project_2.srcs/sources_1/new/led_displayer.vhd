----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/07/2023 04:54:10 PM
-- Design Name: 
-- Module Name: led_displayer - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity led_displayer is

    Port ( input_data : in  STD_LOGIC_VECTOR (4 downto 0);
           leds : out  STD_LOGIC_VECTOR (15 downto 0)
           );
end led_displayer;

architecture Behavioral of led_displayer is

begin
    leds <= "00000000000" & input_data;
end Behavioral;
