----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/21/2023 04:53:31 PM
-- Design Name: 
-- Module Name: clock_4hz - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity clock_4hz is
    Port ( clk : in STD_LOGIC;
           clk_4 : out STD_LOGIC);
end clock_4hz;

architecture Behavioral of clock_4hz is

begin
process (clk) 
  variable count : unsigned(23 downto 0):= (others => '0');
begin
    if rising_edge(clk) then
        count := count + 1;
        if (count = x"BEBC20") then
            clk_4 <= '1';
            count := (others=>'0');
        else 
            clk_4 <= '0';
        end if;
    end if;
end process;
end Behavioral;
