----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/25/2023 08:46:59 AM
-- Design Name: 
-- Module Name: demultiplexer - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity demultiplexer is
    Port ( E : in STD_LOGIC := '0';
           A : in STD_LOGIC_VECTOR (3 downto 0);
           D : out STD_LOGIC_VECTOR (15 downto 0));
end demultiplexer;

architecture Behavioral of demultiplexer is
    constant c_IN_R1    : STD_LOGIC_VECTOR := x"1";
    constant c_IN_R2    : STD_LOGIC_VECTOR := x"2";
    constant c_IN_R3    : STD_LOGIC_VECTOR := x"3";
    constant c_IN_R4    : STD_LOGIC_VECTOR := x"4";
    constant c_IN_R5    : STD_LOGIC_VECTOR := x"5";
    constant c_IN_Rin   : STD_LOGIC_VECTOR := x"6";
    constant c_IN_Rout1 : STD_LOGIC_VECTOR := x"7";
    constant c_IN_Rout2 : STD_LOGIC_VECTOR := x"8";
    constant c_IN_Rsrc1 : STD_LOGIC_VECTOR := x"9";
    constant c_IN_Rsrc2 : STD_LOGIC_VECTOR := x"A";
    constant c_IN_Rdest : STD_LOGIC_VECTOR := x"B";
    constant c_IN_RAM   : STD_LOGIC_VECTOR := x"C";
    constant c_IN_RDM   : STD_LOGIC_VECTOR := x"D";
    constant c_IN_CO    : STD_LOGIC_VECTOR := x"E";
    constant c_IN_RI    : STD_LOGIC_VECTOR := x"F";

    constant c_OUT_R1    : STD_LOGIC_VECTOR := b"0000000000000010";
    constant c_OUT_R2    : STD_LOGIC_VECTOR := b"0000000000000100";
    constant c_OUT_R3    : STD_LOGIC_VECTOR := b"0000000000001000";
    constant c_OUT_R4    : STD_LOGIC_VECTOR := b"0000000000010000";
    constant c_OUT_R5    : STD_LOGIC_VECTOR := b"0000000000100000";
    constant c_OUT_Rin   : STD_LOGIC_VECTOR := b"0000000001000000";
    constant c_OUT_Rout1 : STD_LOGIC_VECTOR := b"0000000010000000";
    constant c_OUT_Rout2 : STD_LOGIC_VECTOR := b"0000000100000000";
    constant c_OUT_Rsrc1 : STD_LOGIC_VECTOR := b"0000001000000000";
    constant c_OUT_Rsrc2 : STD_LOGIC_VECTOR := b"0000010000000000";
    constant c_OUT_Rdest : STD_LOGIC_VECTOR := b"0000100000000000";
    constant c_OUT_RAM   : STD_LOGIC_VECTOR := b"0001000000000000";
    constant c_OUT_RDM   : STD_LOGIC_VECTOR := b"0010000000000000";
    constant c_OUT_CO    : STD_LOGIC_VECTOR := b"0100000000000000";
    constant c_OUT_RI    : STD_LOGIC_VECTOR := b"1000000000000000";
  
    constant c_OUT_none  : STD_LOGIC_VECTOR := b"0000000000000000";

    signal interm : STD_LOGIC_VECTOR (15 downto 0);
begin
        -- with E select D <= 
        --     interm     when '1',
        --     c_OUT_none when others;

        with A select interm <= 
            c_OUT_R1    when c_IN_R1,
            c_OUT_R2    when c_IN_R2,
            c_OUT_R3    when c_IN_R3,
            c_OUT_R4    when c_IN_R4,
            c_OUT_R5    when c_IN_R5,
            c_OUT_Rin   when c_IN_Rin,
            c_OUT_Rout1 when c_IN_Rout1,
            c_OUT_Rout2 when c_IN_Rout2,
            c_OUT_Rsrc1 when c_IN_Rsrc1,
            c_OUT_Rsrc2 when c_IN_Rsrc2,
            c_OUT_Rdest when c_IN_Rdest,
            c_OUT_RAM   when c_IN_RAM,
            c_OUT_RDM   when c_IN_RDM,
            c_OUT_CO    when c_IN_CO,
            c_OUT_RI    when c_IN_RI,
            c_OUT_none  when others;
            
            
        D <= interm when E = '1' else x"0000";
        
end Behavioral;
