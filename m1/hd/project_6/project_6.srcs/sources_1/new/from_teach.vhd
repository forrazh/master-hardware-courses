library IEEE;

use IEEE.STD_LOGIC_1164.ALL;

use IEEE.STD_LOGIC_SIGNED.ALL;

entity from_teach is

 Port (    a : in STD_LOGIC_VECTOR (15 downto 0);

           b : in STD_LOGIC_VECTOR (15 downto 0);

           c : in STD_LOGIC_VECTOR (15 downto 0);

           d : in STD_LOGIC_VECTOR (15 downto 0);

           sel : in STD_LOGIC;

           sum : out STD_LOGIC_VECTOR (15 downto 0));

end from_teach;

architecture Behavioral of from_teach is
    signal lhs, rhs, xhs : STD_LOGIC_VECTOR (15 downto 0) := b"0000000000000000";
begin

process(a,b,c,d,sel)

begin

    if sel = '1' then
        lhs <= a;
        rhs <= b;
        xhs <= x"0000";
    else
        lhs <= c;
        rhs <= d;
        xhs <= a;
    end if;

    sum <= lhs + rhs xor xhs;

end process;

end Behavioral;