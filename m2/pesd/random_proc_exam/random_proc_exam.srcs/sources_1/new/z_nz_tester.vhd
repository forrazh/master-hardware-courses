----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10/02/2023 09:02:26 AM
-- Design Name: 
-- Module Name: z_nz_tester - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity z_nz_tester is
    Port ( d : in STD_LOGIC_VECTOR (15 downto 0);
           test_Z : in STD_LOGIC;
           test_NZ : in STD_LOGIC;
           S : out STD_LOGIC);
end z_nz_tester;

architecture Behavioral of z_nz_tester is
    signal all_zeros : STD_LOGIC := '0';
begin
    
    with d select all_zeros <= 
        '1' when x"0000",
        '0' when others;

    S <= (test_Z and all_zeros) 
        OR (test_NZ and not all_zeros) 
        OR (not test_Z and not test_NZ);
end Behavioral;
