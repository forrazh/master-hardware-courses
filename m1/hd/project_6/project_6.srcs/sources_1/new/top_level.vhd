----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/04/2023 02:16:08 PM
-- Design Name: 
-- Module Name: top_level - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity top_level is
    Port ( 
        seg : out STD_LOGIC_VECTOR (6 downto 0);
        an  : out STD_LOGIC_VECTOR (3 downto 0);
        led : out STD_LOGIC_VECTOR (1 downto 0);
--        sw  : in STD_LOGIC_VECTOR (15 downto 0);
        clk : in STD_LOGIC;
        btns: in STD_LOGIC_VECTOR (2 downto 0);
        btnC: in STD_LOGIC
    );
end top_level;

architecture Behavioral of top_level is
    component DIVIDE_CLOCK is
        Port (
            clk   : in STD_LOGIC;
            value : in STD_LOGIC_VECTOR (15 downto 0);
            anode : out STD_LOGIC_VECTOR (3 downto 0);
            segs : out STD_LOGIC_VECTOR (6 downto 0)
        );
    end component;

    component enable190 is
        Port ( 
            clk   : in  STD_LOGIC;            
            E190, clk190 : out  STD_LOGIC
        );
    end component;

    component btn_pulse is
        Port ( button : in STD_LOGIC;
            E : in STD_LOGIC;
            clk : in STD_LOGIC;
            button_out : out STD_LOGIC);
    end component;

    component controlcola is
        Port (
            piece20, piece50, piece100 : in STD_LOGIC;
            reset : in std_logic;
            clk : in STD_LOGIC;
            euro_in : out STD_LOGIC_VECTOR (15 downto 0);
            canette, retour_euro : out STD_LOGIC
        );
    end component;

    -- signal rev_switches : STD_LOGIC_VECTOR (15 DOWNTO 0) := B"0000000000000000";
    signal clk190, e190 : STD_LOGIC;
    signal BTN_20C, BTN_50C, BTN_1E, BTN_REFUND : std_logic;
    signal fundS : STD_LOGIC_VECTOR (15 DOWNTO 0) := B"0000000000000000";
begin
    handle_left : btn_pulse port map (button=>btns(2), E=>e190, clk=>clk190, button_out=>BTN_20C);
    handle_down : btn_pulse port map (button=>btns(1), E=>e190, clk=>clk190, button_out=>BTN_50C);
    handle_right: btn_pulse port map (button=>btns(0), E=>e190, clk=>clk190, button_out=>BTN_1E);
    handle_reset: btn_pulse port map (button=>btnC   , E=>e190, clk=>clk190, button_out=>BTN_REFUND);

    -- reverse_bits: for i in 0 to 15 generate
    --     rev_switches(i) <= sw(15-i);
    -- end generate;
    
    handle_clock: enable190 port map (clk=>clk, E190=>e190, clk190=>clk190);
    handle_seg_disp: DIVIDE_CLOCK PORT MAP (clk=>clk190, value=>funds, anode=>an, segs=>seg);
    -- Ugh, my teeth !
    to_cola: controlcola port map (
        piece20=>BTN_20C,
        piece50=>BTN_50C,
        piece100=>BTN_1E,
        reset=>BTN_REFUND,
        clk=>clk190,
        euro_in=>funds,
        canette=>led(0),
        retour_euro=>led(1)
    );
end Behavioral;
