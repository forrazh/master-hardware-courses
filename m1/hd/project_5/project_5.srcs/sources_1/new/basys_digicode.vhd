----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/30/2023 03:05:15 PM
-- Design Name: 
-- Module Name: basys_digicode - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity basys_digicode is
    Port ( 
            btns         : in STD_LOGIC_VECTOR (3 downto 0) := b"0000";
            btnC         : in STD_LOGIC                     := '0';
            clk          : in STD_LOGIC;
            sw           : in STD_LOGIC_VECTOR (7 downto 0) := b"00000000";
            -- led_ok       : out STD_LOGIC                     := '0';
            -- led_continue : out STD_LOGIC                     := '0'
            led          : out STD_LOGIC_VECTOR (7 downto 0)
        );
end basys_digicode;

architecture Behavioral of basys_digicode is
    component btn_pulse
        Port (
            button : in STD_LOGIC;
            E      : in STD_LOGIC;
            clk    : in STD_LOGIC;
            button_out : out STD_LOGIC
        );
    end component;

    component enable190
        Port ( 
            clk   : in  STD_LOGIC;            
            -- reset : in  STD_LOGIC;            
            E190, clk190 : out  STD_LOGIC
        );
    end component;

    component digicode 
        Port ( 
            btns : in STD_LOGIC_VECTOR (3 downto 0);
            reset : in STD_LOGIC;
            sw : in STD_LOGIC_VECTOR (7 downto 0);
            clk : in STD_LOGIC;
            leds : out STD_LOGIC_VECTOR (7 DOWNTO 0)
    );
    end component;

    component led_displayer is
    Port ( default_clk : in STD_LOGIC;
           clk190 : in STD_LOGIC;
           input : in STD_LOGIC_VECTOR (1 downto 0);
           output : out STD_LOGIC_VECTOR (7 downto 0));
    end component;
    signal btn0, btn1, btn2, btn3 : STD_LOGIC := '0';
    signal reset_btn : STD_LOGIC := '0';
    signal clk190 : STD_LOGIC := '1';
    signal E190 : STD_LOGIC := '1';
    signal digi_out : STD_LOGIC_VECTOR(1 downto 0) := "00";
begin
    handle_190k_clk: enable190 PORT MAP (clk=>clk, E190=>E190, clk190=>clk190);

    handle_btn0: btn_pulse PORT MAP (button=>btns(0), E=>E190, clk=>clk190, button_out=>btn0     );
    handle_btn1: btn_pulse PORT MAP (button=>btns(1), E=>E190, clk=>clk190, button_out=>btn1     );
    handle_btn2: btn_pulse PORT MAP (button=>btns(2), E=>E190, clk=>clk190, button_out=>btn2     );
    handle_btn3: btn_pulse PORT MAP (button=>btns(3), E=>E190, clk=>clk190, button_out=>btn3     );
    handle_btn4: btn_pulse PORT MAP (button=>btnC   , E=>E190, clk=>clk190, button_out=>reset_btn);

    -- led <= reset_btn & btn3 & btn2 & btn1 & btn0 & btnC & btns; 
    handle_digicode: digicode PORT MAP (btns=>btn3&btn2&btn1&btn0, reset=>reset_btn, clk=>clk190, sw=>sw, leds=>led);
    
    -- handle_leds: led_displayer PORT MAP (default_clk=>clk, clk190=>clk190, input=>digi_out, output=>led);
end Behavioral;
