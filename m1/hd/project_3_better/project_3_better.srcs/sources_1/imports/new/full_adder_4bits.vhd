----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/16/2023 03:31:11 PM
-- Design Name: 
-- Module Name: full_adder_4bits - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity full_adder_4bits is
    Port ( A, B : in STD_LOGIC_VECTOR (3 downto 0);
           cin : in STD_LOGIC;
           S : out STD_LOGIC_VECTOR (2 downto 0);
           cout : out STD_LOGIC);
end full_adder_4bits;

architecture Behavioral of full_adder_4bits is
    component full_adder_1bit
        Port(
            a, b, cin : in std_logic;
            s, cout  : out std_logic
        );
     end component;
    component full_adder_2bits is
        Port ( A, B, C, D : in STD_LOGIC;
               S0, S1 : out STD_LOGIC;
               cout : out STD_LOGIC);
    end component;
    signal l0, l1 : STD_LOGIC;
    signal m0, m1 : STD_LOGIC;
    signal r0, r1 : STD_LOGIC;
    signal c0, c1 : STD_LOGIC;
begin
    
    fa1_0: full_adder_1bit port map (a=>A(0), b=>A(1),cin=>A(2),s=>l0, cout=>l1);
    fa1_1: full_adder_1bit port map (a=>A(3), b=>B(0),cin=>B(1),s=>m0, cout=>m1);
    fa1_2: full_adder_1bit port map (a=>B(2), b=>B(3),cin=>cin,s=>r0, cout=>r1);

    fa2_0: full_adder_2bits port map (A=>l0,B=>m0,C=>r0,D=>'0',S0=>S(0),S1=>S(1),cout=>(S2));
    fa2_1: full_adder_2bits port map (A=>l1,B=>m1,C=>r1,D=>'0',S0=>r0,S1=>r1,cout=>rout);

end Behavioral;
