// Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
// Copyright 2022-2023 Advanced Micro Devices, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2023.1 (lin64) Build 3865809 Sun May  7 15:04:56 MDT 2023
// Date        : Sun Oct 15 17:36:02 2023
// Host        : enji running 64-bit EndeavourOS Linux
// Command     : write_verilog -force -mode funcsim
//               /home/hf/courses/m2/pesd/random_proc_exam/random_proc_exam.gen/sources_1/ip/dist_mem_gen_0/dist_mem_gen_0_sim_netlist.v
// Design      : dist_mem_gen_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35tcpg236-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "dist_mem_gen_0,dist_mem_gen_v8_0_13,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "dist_mem_gen_v8_0_13,Vivado 2023.1" *) 
(* NotValidForBitStream *)
module dist_mem_gen_0
   (a,
    spo);
  input [7:0]a;
  output [15:0]spo;

  wire [7:0]a;
  wire [15:0]spo;
  wire [15:0]NLW_U0_dpo_UNCONNECTED;
  wire [15:0]NLW_U0_qdpo_UNCONNECTED;
  wire [15:0]NLW_U0_qspo_UNCONNECTED;

  (* C_FAMILY = "artix7" *) 
  (* C_HAS_D = "0" *) 
  (* C_HAS_DPO = "0" *) 
  (* C_HAS_DPRA = "0" *) 
  (* C_HAS_I_CE = "0" *) 
  (* C_HAS_QDPO = "0" *) 
  (* C_HAS_QDPO_CE = "0" *) 
  (* C_HAS_QDPO_CLK = "0" *) 
  (* C_HAS_QDPO_RST = "0" *) 
  (* C_HAS_QDPO_SRST = "0" *) 
  (* C_HAS_WE = "0" *) 
  (* C_MEM_TYPE = "0" *) 
  (* C_PIPELINE_STAGES = "0" *) 
  (* C_QCE_JOINED = "0" *) 
  (* C_QUALIFY_WE = "0" *) 
  (* C_REG_DPRA_INPUT = "0" *) 
  (* c_addr_width = "8" *) 
  (* c_default_data = "0" *) 
  (* c_depth = "256" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_clk = "0" *) 
  (* c_has_qspo = "0" *) 
  (* c_has_qspo_ce = "0" *) 
  (* c_has_qspo_rst = "0" *) 
  (* c_has_qspo_srst = "0" *) 
  (* c_has_spo = "1" *) 
  (* c_mem_init_file = "dist_mem_gen_0.mif" *) 
  (* c_parser_type = "1" *) 
  (* c_read_mif = "1" *) 
  (* c_reg_a_d_inputs = "0" *) 
  (* c_sync_enable = "1" *) 
  (* c_width = "16" *) 
  (* is_du_within_envelope = "true" *) 
  dist_mem_gen_0_dist_mem_gen_v8_0_13 U0
       (.a(a),
        .clk(1'b0),
        .d({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .dpo(NLW_U0_dpo_UNCONNECTED[15:0]),
        .dpra({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .i_ce(1'b1),
        .qdpo(NLW_U0_qdpo_UNCONNECTED[15:0]),
        .qdpo_ce(1'b1),
        .qdpo_clk(1'b0),
        .qdpo_rst(1'b0),
        .qdpo_srst(1'b0),
        .qspo(NLW_U0_qspo_UNCONNECTED[15:0]),
        .qspo_ce(1'b1),
        .qspo_rst(1'b0),
        .qspo_srst(1'b0),
        .spo(spo),
        .we(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2023.1"
`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
bEktTo8XfP53J4LC9J1bzNOsr+DeYSQtsSeSeRwv1ROtu7MJT7BubpFM5B3JNITvmmXMIQ7cHCcM
BFy5Vu0fdwcQmgznzr1F4XAF5OH/PlBVKmCiA5IZpd+UQUMuy8l823afh4u8+Fg3bwZX7B36A3bn
Zez9yHjSKD7JGdQ9zA8=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
vAZQ8ZTe/MermX+omywGuwEzd7SLijiaDbuX0B9K4vjWUXvRoI6Em0qizreOX/qdo4JlybEpt70i
jJhVvWv69a9yKb8TMuvLagWbQydSwTJKTY6VSR/CtA2Uive8NvQyiQKFXLjR8k8OBlgOYmyzZEEM
vYgZLdnM3d2xSMMmeGF+dNh8tCJpM10LRaCrnj5w8L73RtOImlhI/zlR8cC5oo1TbyRV+JuHvvMZ
sYS3+4qn/f80Ugvao3cYMW0LtoTftK9oYpzhiyqg6hnJnbGsAENom2wqBpcRJf1vsI98WiJqDCuh
LIdMFI+M5KuqToM8D+FTQUOT2NniYpTmj5qTFg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
VpwnevLJi/mNDesLbbdRntRX/1KkSUuxvcBO6/opCSkxKA2w7s8Eyh+CvZJvHhBMtWZquJPlWZsE
d3toYaeyczcrzAzfKryx5nnTvscAyYnKl8QyY0fWsE1UqWjg6tazMCtzxlfF3HfKx/GSm3D/0NEz
xzyxLBgRosbKCX4YRV0=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MhGbYf5xy0E517prDNoCHbf/sVQ5JHlfzlh1Fz+rfDm8S3/Zt1g/AR2QuQPNwJUQO22hvTTB491a
xRG5ct3upD6ZdXgMesPA9KgwjRjoBp/uriYuT6Sb/yE2jugYl2qBGpqxN9n2OgAVfK3o9XZ/aIcR
St2PwrmKRzU/ZoYenWUMZ6ZRsVNlzFCEBcKop6f5TBy0bWAeebXRZ0Mot23DVX4pqVyFaQoXdmkm
56Vr2jGszkLic4M0JoKahUlQpnrZuHIWgFVd/RzXXP9HwYBRQTxaKnNX6eWTdksVvzAImMYoPa4G
PJJFf+gsNAKp5BIFXjwHfNC+Nerc6XzDmxe+pw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
jfnJJlFHpbB8S3PjID3rEIRi4fzY1WUZaITx6CJ38mSZfYSA13DJislb1OQ17w4Hnv5eGM/0GVgA
2jPR4wYaMzC8v3iDfETrH4kyrFglo3a/NDlACuR1U65YoHUnUu0UmMMovxQEnd9ByAfOtabZPL4j
FTvCoVMpwI8rdT4YJQ5pYXryESdM3NUe29p9OWbY1EalisEVViKuSwS4LzwtaOmrPecCE56FGEp+
2iyBMICOFF2PpT8Bqp39Z2rx4xyIiudZKo3LNimTm/UYBCnPAJ7XBIS+JiCIOkHsPER+wNivbtUb
J02F8ZLbEtS0qmUdYDXO4qqhc1njU9O6Uk9yNA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2022_10", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uOK6pXmc+RsarhB5GcgUPkseiDLhaN7KZ4C18Aqea9NqSbvIERAENTml4U58cVlx6j599K+L2aW5
rVMZLtj8UE4yfEDhtivrSdBYh446mqbnToHhH5r4BmzYnr6BUuXVZ4NIUU29WnaJUZxwrvZeCln4
GQCdP1kUA1Ozy9B47ndTYgOzCcZSr9w36W7ZA1gm34lqVpXYuGsaRTvk1DhS96aFGCeiCTbs5HM3
e0JPkZ7YUsMgWuRzE+jHE1TEMVjbPkpPjFGCYOEeDf2bc/2s2fPLA3bxMs61xUFH5LAd7Qrs9D2v
Mx+Vcfvo7kmp3J5LW99NXfA9OvG1JgjJ7ykhmw==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
OS52LCfxYaApFxxvQUqjJD8DSzwhbsM5irqCX6E4R0iBINlXI3QVmtLKp8vhPICYZWjEuTIVzohU
28vwAOP2ECPWOkJjN+ny9RQeAKmQhPbxHYOysXg4IgtMbK+ZODUoMyLIsJzz2yIFl5qvQeLBnc44
NvqDk7nFLhtrN9De4XV14FKtDvQG0BdWr2mXiS7WiEAQxiww87A0M8yP82JlG6ykYSwQh5G8K6pv
YHoqI8mKAC+KGuDltBnyBrKGip5pRq7Kf+0okVAOwt0lJwDvS0JMNEUg1HK/mEIR6TKUdd8B/fms
4qcaCBYsptjoZVCq4ygSG56x8uaQXMVsEALe2w==

`pragma protect key_keyowner="Atrenta", key_keyname="ATR-SG-RSA-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=384)
`pragma protect key_block
Aew/RSoMZUIh8oIZPhChM37w+R5unp+7pprfqezjGFUVX16UeT1spPFU1DaqTQvQkXhBe4/aNxvo
Y2eUJsQd8zSC9wBoevCnvwaHEv/IBc+OKmBzOPxO1hHXDVPtDZWdRCx+1y0ZYhQa+NA6jLP2zOJx
/emAZW55AWgZKKJS4QgantVgmUSyKVe/LlIVstraTkF4EzV092mOj1iPH/UqFFno9IwE1aOXuYuT
XrZU9D1dkPLBMg3CDwOi+bXRSgjvuueWT7ostJSFraLwDkurP1pYHHG4NDxYiDxMFWarWeII+T6v
hMJKd/8ZRrh5aHvGV5O/Hdc4rPitxa/cdQPAc0r2e2XWAJIdic09atzXXyU9o2vV/urpMsjSVva4
B5a/PwS16c18IMm6vAeFSLMo0T/jor1Q5SoxEC5QEkxvEfIUjjw7k0b1Crv5EfWz/sJ1LHwqlG7t
az+h03yAqvqGfOHC+7YoilYImR1NiLTCLgxnUfIvxo6woY4SgD+hLki4

`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="CDS_RSA_KEY_VER_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
iZJ+xdyrZbhNc8zYurF70yKiutV2IBjRXDiOZ/7w25UL6rCpY4Pd5gJN3+SNIoQ66bzRxlhaXMNu
tzoCM2kFY4N5ZbCy/S4rtBK0PUHKEVd7c5Btr5gn8BgQWiIafJ8Qa/8xqo95ocakFzN6/V+DNvyN
7FPkXDwuiaD0cmHW8XyOxnHM2b/XKHOibr7UKTRAomXyt7y80BVKpE50ddxXAxw9wlMn+gpW5Kpz
Dp8z4VH3uZrVv8Yl5RWELOQ3Uh0Xizb20mvc6Lu+BNoz0Ys9zZUaqKU71Kuv4s8vgPzrZXXNifo2
pU0aNj0oqAGlSTcTCBF8Tl6/jFvUXQEzYoIfiQ==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 7232)
`pragma protect data_block
6+3TYbKYIDf+wcxNne9f4T2ErlKcUKeagyE7rfGoMTxnq9yFm0bFquuHgqHi6VIiRvTzBLuS5H9S
C3en3vY0f5i9aBIfIlkxllEmmkQJ7UpaqzorKW07CIqmCqC+pBAnAHMYANKdizKVth0tESyycOO6
QhVoewlOPrnDv0xy+Q9sETvYnApAkCzAOV4jOpQfcffYx5JZeJqj5c0EKf6lp/lM+XGUdkMmcHGu
rX91SVnbiTp8JzyO6fNUBtQV1fF7XTcmNmQQ8/tXzkTTqbay5u5Wm7cgXDCr7P1At7bBMyl0VKoG
mRd/W33ObGXKewFWGxSsalhKGy64DTwGxaQayO4gsma8VR2HgWfQaJ/KVK2mJLWuE/Pj4akvaDWz
bAGr8rlldMg71x5HAqEsjbKvvXTnR/u2OIBEcF70hTw8EayChUC5x3lWTpX7VuFEBC9kCym/seEq
zYXyZmnd+tZmPuwUmMCSoA5jPa/Xf4kM4VOrRD5n6Y3loVMH+kR+s2/UqJLtLE6fEfmcsZNw3sEH
mWYELQYQ3c0pHGYL0Pevpvb3Rrpe785eNsw5inLERsIeqcalR14jfsTEz0/zFEAUmdH2ZAFnmd+T
QCL26RBAyAQ3HHKuVA9s00A4iGS9thAYE6/VaPKIvGx110NlXvffUSkNN1P2Nm9kr8DIVb+SUIxx
zi21Ms6PDYuo4n070niUTGZeia47ICNZPCrEVGJTAtw7snd2j4kATOwYEMd2K9Iy8gCq3de0H26y
6CE7ywIj2gJlljHgaFKzua8tpo4mm+k28GDCAQbYDaoWp8gZfXfrZjhEP/p0oEDKyZvgZJcdokpK
iZ29EFWuTI6xmTlJQDJo/5bAjZoLxB8c7L4enFte2rspNBzS9MkQApO+7ppNKs6fr83prA8+WO+l
i2+uAf1yXMs8qAKhlWyE3DzELnYpKVbPogQ7fRvAbL8oxki0qTLBLStmC3NfJ1jsbsfG6ESDfFKi
0VPYIlE9ROo+K1K9DPKoTj0er5Go+tl9dRMV1c4up+sBTDCtylhcroHfhdEmGvz3t3WzzFOc+JBM
2UlrjAkltNbiX1oU0/rAiFfdRoqVD0HjJJeBca3mZhJS6MlP7pca/3OzHEQfSBDPsxRBGGs1Itb5
Mzpj7eZiyaJFFrGUJnEEDcCrszVKKEj6DfQqw3cokjAXLmbdKThTRBSOHSOYWk71/UEwbty5xP5M
tuqfvDMedXawCjETFX+sYRpt774GbdRL0FZP2k68p6mRmf7GhAHeBw+taYMvi3Cm+ifWIQjajWc7
y1WneH2z1TsB0pjYVwX65Plyq1oq06SfZfJtj1MdWlFvvZY4e5BrinZfT2W5XXIl3tghS94M7uod
ysbdYGTFcdL9tBzJFx2cMItM56qZWnQVDv1xJm/h0nWUYDNgPIodOdWOvFdHMdM23VcfGDI62plv
ZOA1z1Xc8Eb1Vj/4qO0kQEV3LiLD3EnvoO1aVTsOaHvena+xw4db+Zx7Wwrto6H3HshjqVNSXSGq
31j1Pu+o0mJcE73tklZ53VjXxwj6ua3wvzn4ju4XdUpyatqpSkKCzz+XqoqsezxHfKX9K+/4uuVV
JPoOPEciuKkVQ0l2DJhlMqcrT1fosA7alXC4ZGTBzaqKaXLbTdPFUpq7dB48TDPWB7+jFfgJmPQd
yRJCGOQUKNVfsRkJWhQcIuPbCBba7QxEPj03Xvv+4hFsCUYUl16loImdL9qOCvRv5NB3pLv3jzm/
YgpiPpxQEcvfNOKQc5nZ/3p87CrebMn5Kps/N6EcZv5RSB6GIbYOHZq6JIGOe0oyipeGhxG9VjtM
hAG/v+sJzGvwuwu+m+j0YKkkukfmV1wAxcKgWHWMGzpVV4lkn173BmCro2+47ulARXkpm3cwqQIs
5e4EsXu8V2nc4g5L1Sbx2IpjHTBFnlIYKtPQGQLAX9uetNo6VkC9pMQTCUZNon6ek+1yD2EQFzIK
EJkW9/GckuMxZcyC1AS6ToE7Kf8QEIDmYUn5uiHYQl5qbbofxt6qxjaHN5a88lNxAKQ7OCDlxTNT
2aARi9auw1j98aNZwSXaqIBBrhLvupX44UY4ZzXTxzr4Ysjn8gNzhKdN+x28DprAzD4TZWwg8Xe7
s8Qu0CDg7miJwV6wTOCecjKDPZbaB4PomOo4tlYSXQ2pBG5Dmt6OJi4DtgS7XmZLPX35arSz0aRk
rhsgFSIORVNnJD8hYWxSIJUJCbOS5OZEJidr1WKkxUps0gnpEhEyR0GsIRxD4YG/nnMPLsdkMxXj
El7et/9yv2NOTnOhcfMAFnuZ/BPsG0ZcZXDwOpbqXLmnDglOOdLrWIXXNH2p2GvUhDg+Vnv9AdE1
ivZgUGKkm03vZPQfY8QAWxA2ppES4LUVEn8Xh5jQbNn/qOExklinMDX6gPS+ts1loeFhqHsWMvvk
yU/Ow1dzxtJb9sRX84rEba2EorTyF7OJ87I/1qJ1qKV+aXLV+f6CbfXa8uFmMGrzJGLfZC8q+PW/
cd1dLleSDBDhGG+w/L/krHrEPe25hoG2JOpAdKIQ8SGSokWAPA0nbua1P6UYa8rQVD6qC91IzFNE
sRmQDtX1jgx8oQ8lAQ2WCnYG1YeukjwHDzBC88MnvrF3AlxFB4liq0zjOBMJ6AAmp0p6ilFuqxDv
GpDpuYykvaxEDn2jZXJ6TOGRi/P0potOYcemXjIovrJIdNFqsAJD8bINXwUP7klxfheNeqPWGuj1
GZWz3LSa7WjJ2Lfnmr53K6KfQYBr7y9VbjCBem7EEhZ3J35uwR2tQHvgeKTE5hVv5EhvEpE4KvzA
gzILdwnPLkPYt05e66SvPomR63TaWHMmttfRUKAyyHyPvpa0cf4BYxjh/7UuTFThb5CuBKssDQmU
Ix6uIWowhT6truIlSvvAPZbCo1k165Zg3prRqATpHWy5GKoaMdQx0Ru78I6yk3SAyUEjnUYzn0lF
EYzyEiEnZfk5sGjem1Sp/8kFi4BDZsxGcC3ckUfZidLJeV32uAvfqrKNCYyp3goE4iJbhzyJRU7R
I1YEd2QtefwdYcAJ2I1zvCLRJOkowNFJ3vDPARjLQMkmNQMGLUo5YCMMr2o2wubXrhc/3VBI11kt
SYKyqrmCVZx51EfwbY/TrhI9vUH/aiOFGW2t6ZnbyyLM59+IkWxAp34rUN6klbtb/K7JGsN/RuDL
Kg+ulzjdUEDCSNHniKliAr9k+6hzbTbaC611QBz6sF2kJw8YNXmz7W8bmbdIwg5aIDZ7Ky0yhUsM
CrnGMUkhHaxVxQEUhJDGU8x8e6YXLy+IU0lu0UdEW8yzzA0e7BJbaTfHiLL2TMgOPuDtEVtkpT70
gzqkMDpw8AvZaFO+FFw9w5zKUKAyh9riYuxs6EV9PYmcbVjlaAGQcZVF92DmQRzfk9joM7bwOvRO
VWOHYNTyBfn3fLyVOsO5vvPvxoLavU+8jjIuJXP6ogutH6DcM1mFJoOR+8n9ozUOYprkX9yUuuJn
rxgJh4crlW61SocazihbMcSTblEgWbN7ZM9chgJbnrz5fEwdSdVjfb4AGTrgKetGnnEyeq+v/OeS
Oxx3XO54ckeL8gOB5xTNhRPbWv1pH5M0lmv/yYhsjOgPU6zzgm2P+BMPXXQJXPKlJcwIQBf6ezRw
QJQkz3wFFK/1cmZA3XkB1MG7op4jPKaoNOIODwyIfkDjBjLlgyyD4FJz5TLKEWYKwyp75AWQLnqa
YDiJeD3DTmO4qUDJBjpCw7IrD1LlWPS8CQ3LS0f2+R62jLfI02QilzJzd/L5pJGZQr5zw9Gr2YB6
9tePq2tT/ITirF0fbJORCxApYrJc1ry7or97R5rJpwMe1Lp7/y2dlrRpUgGkUt4gTDdjAbgzbKI8
kJxWqotdxlkpZsAFgsr8ThZpiqEPnvEKatyJHPQ9ooTMdp5ezVjsO4EJjWY0LgsgLOjxQ5vQufnI
scKN1ce8QWI0v1yYjpoEd+8vsw7OkwXbeWS2lzLVTjl+tkxqeZ5oElp1d1zcP3o8A+5jUnsMsAUN
Rch7U7kV+CGBTfjGZKO9If7lAsMiP/FzQu55aGw4g+6UhWHeCBNxjdn/w/Q9tj7kNB7aHAaab3HG
MnGx5lm3FT7z+Zg/9SpWunnWEOmwubDUmTpqk3ZV5G+mn4Ua31a+F/KpXrYfuzWeYVw1cEmBcLio
BXJj5ZpLQ4Gfqn6S563FnMHIK6cfdJmqEGdRRrMz2pCYq3qbdx1MesVthfe0ZmDfwCEOAd285C0N
FYvDVknf7r0Bnl6JxP0x4fvyRExNvXbXWOM2nwzcjJi/K0eXlAxFArQgv1sfMEWmbLdLwK0M/3fu
pzrSTFlF23zwjpTfDA9Vpbt0efFExRkuyhPH/9bA+qaTWY57tkq2XWTYzpP8UeG1ABThXr2S4OVX
l3NhipSqGh8lq2OzW8IOfNvQSo1lluwNnn5C6g/itDMiTudlZIH917bJHxvnBlwDo+Zoc4fx99WH
/vBUnzCoxpk44GqQF5sgUXJd0Ta5nBZL3DA0HzM8Y+Ur8FqSu4viXxcavAIFz3aGeYJFX6qfvhdD
laXNPoBJUBx/I2thQj/fSvd/USZw2frOEAdywWNH0v92gEZiIuvT4gS8yf+iQSlwQ8rf1NlnmY9E
ouX7wPASZj/ICq2VrAFbaF9FDHbgNTqICoXkNtGznXvjHMNCEk46G2cInTt9H3OetG7HaBjMfIpq
fR/jPKpKYzYDaTFacKik2mKpFLJ/Re8DrbCpI1TFkMH7dzBk6tmPSuZgCzNyf67LVroOGdE+WmNf
JdaaQxeWKOCCwshPzal1NUarvGl92LRKDcnio7ceQmCglS8P8nZOGWQghjNBQLdXjVzFAe7tSp7J
p2w71s7UX2cR5OaFi0dO+NvQcLbsgTP+BXva9o2IRIk0a5WA3+e5cgXAslr6pP5/nC4xs9PXbON1
wg55cnkFvfR8679V7UEQNaablKikqcYKU0jmk9KdWWWJsE7IZCEV/dgWnSbsmbJmrrfHIS37WbhE
psZyx/68bQckTbRc4kevHhv6hPRoHU1n9aMngTayaJcLh2B7L++wSF5R+HIlMPRQIQdTR14ioI08
H1vLH+iyTsShpiqKuMl24QbqcjZcBswmOnL8zug5glVaba8G6mA/QROJQ/WZFNZZjorLu3M1atp5
fhAB/ETT8uuSrENiCce7NaOBV4y55hLInvQqZQiB3WFcLYTIij6tIcjiL21cvg7H0eHYRcDjEVr2
nJrDDEHHbLct6NsKQje+FmFWxX4jy3OhgkHBUtdTbO4gv7jfXrZGdl5XH5Xjuh/fZyklDDYoDdP3
Ndm4Z8D6W0Vd0BflQPBXnGP8cyYs55ymo4Cmu4H33xW8E4y5MXNb7b85ecSil5hXCoMvSXx/cajg
YGhE9vnKLirGmRwc56wgza9oOZzwCP3Fns2E/6Yu9expnTpzrjIv7CW3WQw0M+YIfDZbFPk24Ift
GWN7gsDuRBds6aZHmRHqbH5vhjs9Tx4SKXnIkgnXVrk4vYvT+hvG5N1Ss8wD6Mk+KzPqzvi1aN3z
hEvXdjfJ4kBe9t9hoXA8nElrpDNjbIsWZhokGNFJVwfxUU44CGh6HuCn/rvKj07OBvarh60rvcAD
oJTcnGjtZ5z2CZVwpsOZo8bPHnI+k5RkKmQH007ynZAp2RoCFcbNUBtU5S3OV5QWdAE48QUDjPZr
tvxDualE12VuG2i9h/tTrgUiVxXg6yzKWQFqnbSR1HruNjaxMuie01efkQkynh/knA/sCNQ1ZQ8S
QQOUB6G0LHpi1pf8umWyn+liJqj9Bjyr/GW89F2C8q3+b0NpRlmvWKPWN9AN/IJqZcJ0goOGK/a+
2FPwQpAz9aTctHmPBFqsJnMUkbWdhdDvxdqIOtM7/x/LJLkmr9S+BgDzyMKJPmGLVYmOWY4wN1G9
QHV3cru8H2QnQjRYLPpjBof2C/UXW4KlauitbCi31MXgnFNvqeuOHq8C0wbhhjKslanK23Tzp1aw
JEpFVz5EjNAIlBjSidxBoHi0UvyPpcUQVhycnsq8DzG3jvwXQgvnFq1Er/luck94vggTZr6qQgB0
KhfJCE4gl5xDAaqNKQTVxz0ZYIISF9o3FsIqfTfB3DNWxb3YgFKJJmCSACvpeY/92XmUc+iKpnVV
4IxtA9u7eQhI7dkvGGsi6AUKedZJPZv8u8BRoBJ5AcimuwUSAifLGH1CHULl68+PkQ90OiiLljZP
PeMtNmjpnHkm4Lf8i0g0/wB1fUHM6kef5bJBbTxVnr/Q6XXed7XaEtnivnSFHpZ8wMzFiYRxszip
iQjZaEF2gaWCB4oGdroZFaDAgm0xORPLlrL8lIYN8+43/pv85m1t6+xtRxfLp6OkPlSSqCmwND76
sPEi5XO+2avIFsJK8W5HLd27IhSwUJYrcCjO5+Y7lJaxELe6Gw+2xjTZv/Pnyko8ULdJ08+XVE0O
mdHgu8JcTNur8zToegOzB05Ry82tucPrfmUtFipeVmtT7AuH1wB+9q3RbEpBP4M/TcvjBmL0rMhs
iVyK53u3ndYJFhRCMCOlMUS1Qfpoc9EAZxwUgIlXkgN05PmEMyNffH/g1YyGacusYMD1aG+UNRwH
HhmEvjXRn6lIc25WyufkWQj46hFeTyc3D9nU9pe5NwZaROo1+uklcYHpSkl91YoOLH10bcq5EfqP
ep5ERvdJEo6UFsfm4Z8L47PWXQgtLmfdKTcuKeY5JdoqGYO69J73QAtojX21dufle8r39lNbgfUc
RvMe27St6FDu/lawuAUy8L34kHz5OOAjbhFLksI4W2FUgcKz5oJe03Xiw+CbbAyFS7wxzEm2PSrX
9XuP89hNDXOQT8pKT+AZ3ENcZCVbyR96E8eawv/sA7jxGvXy+mps+RSMGtCv3ChfMDcDCy9sZyAF
DnO9iAm1TuxOyMuZbJK8Ds+k5yWuN1fcBbxB+BOpymcmukAaorjM0hCRWGmgsW0tDRgmOrvX4Xj/
AtKFaY+wQLD9wb2I1UWyUmNIX8rsb56nHWq7t8nZ/tHa0jgAtNzLpc9NYKQnoaut8RrXT8t07g/1
MEkZjOKx+GTKwPPbSBO6TG4UeB6rB72vIIxU5Kvu8lXZZUKE9riCxGgtPyAeyxTyGuk/1iGTf4ZG
SwW/MQEAXlmAFW+JnMcOh0l++NSQHvafuO5FlOsoBwjez2pQuxNCVu8gNkece7JfPfMK27QBUGFe
JvDRMl03ZLPKJBpTVMv+/IQi06qPC4zx33W9h8SM7FWs3bgQYYaago4Tq7OhMgCbwHSywPzSuxjv
uPmxRBRLhTfDJUUtWAiyO+mz/LyVbXQCOLuQb64eHCmcLmLWnS7sLMsADa+RWTX+WfMcIG5uxTl9
bjm7lyL2xFFYy++xegh6aXW50G8TI9yCPG21o1HxH9i4rURF8lvrOh1jyd8q8T7iAaP34WL58Rem
T6i+tefuyvmeFE3UQJjvxHjSi82HUTU+MHezrpH2YFmTSKGfvHkTGtnPsPM/G3aRIas7MWi/4cbD
EGeshI+bQzv4lX43fbESPo7jBMMIHyXX/PxuuQqXP7Dlcxn7RB3Xl9MFemVrnRTltED6D5yDpci/
Lzvj6K60GbhQivFMt48/JmnY8qIyhZ3vC5N0y51TEhAIKHXGmW+Wgle/67fnZwoMpsATQFLfOTyZ
JlXquqlu9j845xwCOuxVF009znHqakIQdgLX2WboLxgkvb8IZmC3TNBoAhn7dHJ6oHhJ9lgXbzp5
ZAOtuimQgKBoSSwBmuaxvLvlx0fHcd8Qy73b55Ao+vy/wHwniGpNZfnXDIC8HmCgU71c9UzbLWsN
JGnrNtmqLW05Sv9UaLLDD6vgfo4L9D9s5INWwjinnzJI696gzqiBwR8sM4ux9nPua0E2VUuLjjmo
9Lqq6HVj2Q8pVNsVUqqCtTm2OJmuCEDl2IBZUjEGcjdqX9QvNzv8j3QwBhcRLKjrafRzJBZcb36n
NRpIHUnDzO5HDoncW/pNFp0NulmvDyu5iguqJouEM2GQ7PF3N4qUi4A+k9WcBeRsGnNkeuOzON3m
lNSaqwuSbtjP+eAPhfIjXwA872bbZ2U8Chk89i3HFpi8YbPlMtyTPSO57+xyhbxGbbm8U/50xKF7
fqLBFOp4mO1FeiNRq9aU2Xjq6yrTg1+RerQiBa9KVPamQUg7LtZi5+dnYerOQL46sapgWVan51Ac
SXGA4cHVZW+MNMxR4BtX4KxjScq2peOpfXVrTFY+fOcGqEus5OrITHdSj5YtILpp1Aad8T2B2m5T
xSuvFjV9iEfKMRobB/BVBqp8mJy9y7V0A168wg6ZrctEeOYprFFY2teQyQ8WUA2iLgUJxkB/vkDV
reZDFO14kwPFGor3NXJDFVlDXo0jZTf2HzhorGaPjSSx8ZNhsZGyI2F9xLieU8r1WVkbcz/k0h/R
SUHahIVjNi+9qG9C6f9/NJS+qzyLT0+PK9x4GAilp5GDPZL51IRsB30V+2N7NGlLqPHO5ohXLqCi
H4MCcRsewU0jAVUC722HvqK/T0CwjXbovzrcMmMM/O/Xiy4ddGEsJzR6xSPINTr6dt27RylJklGB
43z9ql2+h3VDdf44uQ58HvWnWk2xJIS/GwNsr5dCe6/EVFymWL8qE+ctgmNBinnrMZdF8d5rFwMt
15M041FsgdVe3lfKyGqYc9RFAJNHeM/H8ej6aoy5q5iqB2DTAQGs7eLYhufwIuGBDJ6ChqXMEdwx
E+eatlq68vwRBhoq4EcuNxPg1/VhdL8CzUIsuYVBHaAcyMo+m9cZlZ//WNf7q3wN4gMRJ+q/QmwM
X4fgy060tNBD54ISFoJMoabzMNbSpWkBuNVoBUuiHrJVjBufuDbyJfbmwFhko/gYYJUKFQU1HgIn
Ce7BdWpfIIDLJwKqp8NMaS2xkVoZy8bYVCZf+O5uf56J+uzNFbW5+Nus7JTsr6rf8/CV+KgiE8oX
/lsvcO0pNUQzx02xvPMnjMPRLprWj7VxPZf50mDpICO5rRl0hVs7EL0rvPmaKMvl3xIcK2/HeMrK
K+kmUGi51QYLSjVFnoE008xarF9RWQOk2gdSzOd0sh4OFxwsIMs7Sr8+LgCz5kKwQudTfteVhLPt
Om8YvppY8wpUWU9NJwUkIa5zqMm3kT2CIFXmfPRNtSfOPmyKMMTw8xC3v4gGekfNbCp6NdEclyIR
8XvmNJI+vGOKTwqXTY2YjdJuuBXtQPa6atzMmh8WyZ3P1gPK78T8AMYT2HdvFjRol7eOr6EWe5a7
Nxg4Ze/wD3G81ch4HdOhIR8eKjP13Fy+YwMnn/MPZ+LsO4M9KoJ6NQpAVyYSzUR+5K9w3cWP/gWe
j/N7VSo2lcINWxj52DlhZwLEuf7RlLMo9ZqSuQbPc0/RsVUDYF0LmDkusLpDhdfTXyeL5YXSXNIg
wD6L5KmlX9N/qP3xmhZ4WZ99zAFQlEFnkgMukWh95mMkhIz0ea+umTAsCpqswM94Al7A/OKXyVZq
xawbRn5LTrerYMMfNr7s5rUohnZE+utxEwOH8QBiNrVmCTThxlO2wZwDY9kNBiTV3FSr3Im/P4Lv
v3IgdW6nLX9LPam5wVB7u46WOyiapuI7Jv7p4yUB7n3xhsyAsNU12cIhsRUgNoSgIe4=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
