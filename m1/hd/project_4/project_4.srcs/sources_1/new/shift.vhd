----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/21/2023 03:35:54 PM
-- Design Name: 
-- Module Name: shift - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity shift is
    Port ( btn : in STD_LOGIC_VECTOR (4 downto 0)   := b"00000";
           sw  : in STD_LOGIC_VECTOR (15 downto 0)  := x"0000";
           led : out STD_LOGIC_VECTOR (15 downto 0) := x"0000";
           clk : in std_logic := '0');
end shift;

architecture Behavioral of shift is
component fpd is
GENERIC (init_value: STD_Logic  := '0');
        Port ( d : in STD_LOGIC;
           q : out STD_LOGIC;
           clk : in STD_LOGIC);
end component;

signal Q : std_logic_vector(15 downto 0) := b"0000000000000001";
signal D : std_logic_vector(15 downto 0) := x"0000";

begin
task_15: fpd GENERIC MAP (init_value => '1') PORT MAP(d => D(15), q => Q(15), clk => clk);
    D(0) <= Q(15);
F:  for i in 0 to 14 generate 
    begin
        task: fpd GENERIC MAP (init_value => '0') PORT MAP(d => D(i), q => Q(i), clk => clk);
        D(i+1) <= Q(i)    ;
    end generate F;
    
   led<=Q; 
end Behavioral;
