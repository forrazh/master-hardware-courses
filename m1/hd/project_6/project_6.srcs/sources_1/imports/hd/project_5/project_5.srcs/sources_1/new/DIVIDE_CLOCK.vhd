----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/04/2023 01:21:39 PM
-- Design Name: 
-- Module Name: DIVIDE_CLOCK - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity DIVIDE_CLOCK is
    Port (
        clk   : in STD_LOGIC;
        value : in STD_LOGIC_VECTOR (15 downto 0);
        anode : out STD_LOGIC_VECTOR (3 downto 0);
        segs : out STD_LOGIC_VECTOR (6 downto 0));
end DIVIDE_CLOCK;

architecture Behavioral of DIVIDE_CLOCK is

    component x7seg is
        Port ( 
            input_data : in STD_LOGIC_VECTOR (3 downto 0);
            output_seg : out STD_LOGIC_VECTOR (6 downto 0)
        );
    end component;
       type state_type is (st1_anode0, st2_anode1, st3_anode2, st4_anode3);
   signal state : state_type := st1_anode0;
   signal next_state : state_type := st2_anode1;
   signal data_to_translate: STD_LOGIC_VECTOR (3 downto 0) := "0000";
begin

-- This is a sample state-machine using enumerated types.
-- This will allow the synthesis tool to select the appropriate
-- encoding style and will make the code more readable.

--Insert the following in the architecture before the begin keyword
   --Use descriptive names for the states, like st1_reset, st2_search

   --Declare internal signals for all outputs of the state-machine
   --other outputs
   translator: x7seg port map (input_data=>data_to_translate, output_seg=>segs);

--Insert the following in the architecture after the begin keyword
   SYNC_PROC: process (clk)
   begin
      if (rising_edge(clk)) then
            state <= next_state;
         -- assign other outputs to internal signals      
      end if;
   end process;

   --MOORE State-Machine - Outputs based on state only
   OUTPUT_DECODE: process (state)
   begin
      --insert statements to decode internal output signals
      --below is simple example
        if state = st1_anode0 then
            anode <= "1110";
            data_to_translate <= value(3 downto 0);
        elsif state = st2_anode1 then
            anode <= "1101";
            data_to_translate <= value(7 downto 4);
        elsif state = st3_anode2 then
            anode <= "1011";
            data_to_translate <= value(11 downto 8);
        else
            anode <= "0111";
            data_to_translate <= value(15 downto 12);
      end if;
    

   end process;

   NEXT_STATE_DECODE: process 
   begin
      next_state <= state; 
      
      case (state) is
         when st1_anode0 =>
               next_state <= st2_anode1;
         when st2_anode1 =>
               next_state <= st3_anode2;
         when st3_anode2 =>
            next_state <= st4_anode3;
         when others =>
            next_state <= st1_anode0;
      end case;
   end process;
end Behavioral;
