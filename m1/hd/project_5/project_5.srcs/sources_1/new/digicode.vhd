----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/29/2023 03:09:21 PM
-- Design Name: 
-- Module Name: digicode - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity digicode is
    Port ( 
         btns : in STD_LOGIC_VECTOR (3 downto 0);
         reset : in STD_LOGIC;
         sw : in STD_LOGIC_VECTOR (7 downto 0);
         clk : in STD_LOGIC;
         -- led_ok : out STD_LOGIC;
         -- led_enter : out STD_LOGIC
         leds : out STD_LOGIC_VECTOR (7 DOWNTO 0)
   );
end digicode;

architecture Behavioral of digicode is
--Insert the following in the architecture before the begin keyword
   --Use descriptive names for the states, like st1_reset, st2_search
  type state_type is (st1_start, st2_digit_1_ok, st3_digit_2_ok, st4_digit_3_ok, st5_digit_4_ok);
  signal state, next_state : state_type;
  --Declare internal signals for all outputs of the state-machine
--   signal <output>_i : std_logic;  -- example output signal
   --other outputs
   -- constant NOTHING_TO_SHOW : STD_LOGIC_VECTOR := b"11111111";
   constant NOTHING_TO_SHOW : STD_LOGIC_VECTOR := b"00000000";
   constant CONTINUE_SIGNAL : STD_LOGIC_VECTOR := b"00000001";
   constant PERFECT_SIGNAL : STD_LOGIC_VECTOR  := b"00000010";

   pure function determine_correct_button (
         sw   : std_logic_vector (1 downto 0) ;
         btns : std_logic_vector (3 downto 0) 
      ) return std_logic is
      variable value : std_logic := '0';
   begin
      if sw = "00" then
         value := btns(0);
      elsif sw = "01" then
         value := btns(1);
      elsif sw = "10" then
         value := btns(2);
      else
         value := btns(3);
      end if; 
      return value;
   end function;

   component crawler is
          Port ( clk : in STD_LOGIC;
           output : out STD_LOGIC_VECTOR (7 downto 0));
   end component;

   -- signal counter_delay : integer := 0;
   signal crawler_sig : STD_LOGIC_VECTOR (7 downto 0) := "00000000";
   signal counter_delay : integer := 0;
begin
   handle_crawler : crawler port map (clk=>clk, output=>crawler_sig);

--Insert the following in the architecture after the begin keyword
   SYNC_PROC: process (clk)
   begin
      -- if (<clock>'event and <clock> = '1') then
      if (rising_edge(clk)) then
         if (reset = '1' ) then
            state <= st1_start;
            -- leds <= NOTHING_TO_SHOW;
         elsif state = st5_digit_4_ok then
               if counter_delay < 380 then
                    state <= st5_digit_4_ok;
                    counter_delay <= counter_delay + 1;
               else
                  counter_delay <= 0;
                  state <= st1_start;
               end if;
         else
            state <= next_state;
   --            <output> <= <output>_i;
   --         -- assign other outputs to internal signals
         end if;
      end if;
   end process;

--   --MOORE State-Machine - Outputs based on state only
  OUTPUT_DECODE: process (state)
  begin
--      --insert statements to decode internal output signals
--      --below is simple example
      if state = st1_start then
         leds <= NOTHING_TO_SHOW;
      elsif state = st5_digit_4_ok then 
         -- activate chenillard
         leds <= crawler_sig;
      else
         leds <= CONTINUE_SIGNAL;
     end if;
  end process;

   NEXT_STATE_DECODE: process (state, btns, sw)
   begin
      next_state <= state; 
      
      case (state) is
         when st1_start => 
            if determine_correct_button(sw(1 downto 0), btns) = '1' then
               next_state <= st2_digit_1_ok;
            elsif btns /= "0000" then
               next_state <= st1_start;
            else
               next_state <= state;
            end if;
         when st2_digit_1_ok =>
            if determine_correct_button(sw(3 downto 2), btns) = '1' then
               next_state <= st3_digit_2_ok;
            elsif btns /= "0000" then
               next_state <= st1_start;
            else
               next_state <= state;
            end if;
         when st3_digit_2_ok =>
            if determine_correct_button(sw(5 downto 4), btns) = '1' then
               next_state <= st4_digit_3_ok;
            elsif btns /= "0000" then
               next_state <= st1_start;
            else
               next_state <= state;
            end if;
         when st4_digit_3_ok =>
            if determine_correct_button(sw(7 downto 6), btns) = '1' then
               next_state <= st5_digit_4_ok;
            elsif btns /= "0000" then
               next_state <= st1_start;
            else
               next_state <= state;
            end if;

         when others =>
            next_state <= st1_start;
      end case;   
   end process;

				
			
end Behavioral;
