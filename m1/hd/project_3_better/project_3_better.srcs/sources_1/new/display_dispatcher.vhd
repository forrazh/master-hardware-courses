----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/17/2023 05:41:06 PM
-- Design Name: 
-- Module Name: display_dispatcher - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity display_dispatcher is
    Port ( is_special: in STD_LOGIC;
           in_data   : in STD_LOGIC_VECTOR (4 downto 0);
           leds      : out STD_LOGIC_VECTOR (15 downto 0);
           o_seg     : out STD_LOGIC_VECTOR (6 downto 0);
           an        : out STD_LOGIC_VECTOR (3 downto 0));
end display_dispatcher;

architecture Behavioral of display_dispatcher is
    component x7seg
        port ( 
                is_special : in std_logic;
                input_data : in std_logic_vector (3 downto 0);
                anodes     : out std_logic_vector (3 downto 0);
                output_seg : out std_logic_vector (6 downto 0)
             );
    end component;
    
    component led_displayer
        port (
                input_data : in std_logic_vector (4 downto 0);
                leds : out std_logic_vector (15 downto 0)
             );
    end component;

begin
    seg: x7seg port map (is_special=>is_special, input_data=>in_data(3 downto 0), anodes=>an, output_seg=>o_seg);
    led: led_displayer port map (input_data=>in_data, leds=>leds);
end Behavioral;
