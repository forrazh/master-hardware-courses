----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10/08/2023 04:30:04 PM
-- Design Name: 
-- Module Name: random - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity random is
    Port ( clk : in STD_LOGIC;
           CE : in STD_LOGIC;
           Q : out STD_LOGIC_VECTOR (15 downto 0));
end random;

architecture Behavioral of random is
    signal I : STD_LOGIC := '0';     
    signal Q_i : STD_LOGIC_VECTOR(15 downto 0) := x"8000";
begin

    process (CE, clk)
    begin 
        if rising_edge(clk) then 
           if CE = '1' then 
                Q_i(14 downto 0) <= Q_i(15 downto 1);
                Q_i(15) <= I;
           end if;
        end if;
    end process;    

    Q <= Q_i;
    I <= Q_i(0) XOR Q_i(1) XOR Q_i(3) XOR Q_i(12);
end Behavioral;
