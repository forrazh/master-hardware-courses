----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/14/2023 03:32:13 PM
-- Design Name: 
-- Module Name: par_8 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity par_8 is
    Port ( word : in STD_LOGIC_VECTOR (7 downto 0);
           SUM : out STD_LOGIC);
end par_8;

architecture Behavioral of par_8 is

begin
    process (word)
    variable par : std_logic;
    begin
        par := '0';
        for I in 0 to 7 loop
            par := par XOR word(I);
        end loop;
        SUM <= par;
    end process;
end Behavioral;
