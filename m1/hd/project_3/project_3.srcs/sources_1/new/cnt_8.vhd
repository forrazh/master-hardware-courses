----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/14/2023 03:32:13 PM
-- Design Name: 
-- Module Name: cnt_8 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;


-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity cnt_8 is
    Port ( word : in STD_LOGIC_VECTOR (7 downto 0);
           SUM : out STD_LOGIC_VECTOR (3 downto 0));
end cnt_8;

architecture Behavioral of cnt_8 is
    signal count_0, count_1, count_2, count_3 : std_logic_vector (3 downto 0) := "0000";
    signal mid_count_0, mid_count_1 : std_logic_vector (3 downto 0) := "0000";
begin
    count_0 <= ("000" & word(0)) + ("000" & word(4));
    count_1 <= ("000" & word(1)) + ("000" & word(5));
    count_2 <= ("000" & word(2)) + ("000" & word(6));
    count_3 <= ("000" & word(3)) + ("000" & word(7));
    
    mid_count_0 <= count_0 + count_1;
    mid_count_1 <= count_2 + count_3;
    
    SUM <= mid_count_0 + mid_count_1;
end Behavioral;
