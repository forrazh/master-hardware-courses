----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/17/2023 07:07:22 PM
-- Design Name: 
-- Module Name: special_display - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity special_display is
        Port (
             input_data: in STD_LOGIC_VECTOR (3 downto 0);
             anodes    : out std_logic_vector (3 downto 0);
             output_seg: out STD_LOGIC_VECTOR (6 downto 0)
         );
end special_display;

architecture Behavioral of special_display is
    constant ERROR_CASE   : std_logic_vector := b"1111";
    constant ERROR_ANODE  : std_logic_vector := b"0000";
    constant ERROR_VALUE  : std_logic_vector := b"0000000";
    
    constant UNDEFINED_ANODE : std_logic_vector := b"1111";
    constant UNDEFINED_VALUE : std_logic_vector := b"1111111";
    
    constant PAR_MASK: std_logic_vector (3 downto 0) := "0010";
    constant CMP_MASK: std_logic_vector (3 downto 0) := "0100";

    constant OK_ANODE     : std_logic_vector := b"1110";
    constant PAR          : std_logic_vector := b"0001100";
    constant IMP          : std_logic_vector := b"1001111";
    constant TRUE         : std_logic_vector := b"0000111";
    constant FALSE        : std_logic_vector := b"0001110";
   
    constant IS_EQUAL, IS_IMPAR : std_logic := '1';
    
    signal l_par_and, l_cmp_and : std_logic_vector (3 downto 0) := "0000";
begin
    l_par_and<=input_data AND PAR_MASK;
    l_cmp_and<=input_data AND CMP_MASK;
    process (input_data)
    begin
        if (input_data=ERROR_CASE) then
            anodes<=ERROR_ANODE;
            output_seg<=ERROR_VALUE;
        elsif (l_par_and=PAR_MASK) then 
            anodes<=OK_ANODE;
            if (input_data(0)=IS_IMPAR) then
                output_seg<=IMP;
            else
                output_seg<=PAR;
            end if;
        elsif (l_cmp_and=CMP_MASK) then 
            anodes<=OK_ANODE;
            if (input_data(0)=IS_EQUAL) then
                output_seg<=TRUE;
            else
                output_seg<=FALSE;
            end if;
        else
            anodes<=UNDEFINED_ANODE;
            output_seg<=UNDEFINED_VALUE;
        end if; 
    end process;
end Behavioral;
