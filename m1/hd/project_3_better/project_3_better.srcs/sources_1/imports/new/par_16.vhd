----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/14/2023 07:21:02 PM
-- Design Name: 
-- Module Name: par_16 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity par_16 is
    Port ( word : in STD_LOGIC_VECTOR (15 downto 0);
           SUM : out STD_LOGIC);
end par_16;

architecture Behavioral of par_16 is
    signal left, right : std_logic;
    
    component par_8
        Port (
            word : in std_logic_vector(7 downto 0);
            SUM : out std_logic
        );
    end component;
begin
    left_task : par_8 port map (word=>word(15 downto 8), SUM=>left);
    right_task: par_8 port map (word=>word(7 downto 0), SUM=>right);
    SUM <= left XOR right;

end Behavioral;
