----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/11/2023 11:57:05 AM
-- Design Name: 
-- Module Name: toplevel - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
-- use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity toplevel is
    Port ( sw  : in STD_LOGIC_VECTOR (15 downto 0);
           clk : in STD_LOGIC;
           btnE: in STD_LOGIC;
           led : out STD_LOGIC_VECTOR (15 downto 0);
           an  : out STD_LOGIC_VECTOR(3 downto 0);
           seg : out STD_LOGIC_VECTOR (6 downto 0));
end toplevel;

architecture Behavioral of toplevel is
    component mbus is 
    GENERIC (NB_ITER_RND : INTEGER := 0; RAND_START : INTEGER := 0);
    Port ( 
--    d_in: in STD_LOGIC_VECTOR(15 downto 0);
    clk : in STD_LOGIC;
    continue : in STD_LOGIC;
    d_in: in STD_LOGIC_VECTOR(15 downto 0) := (others => '0');
    d_out1: out STD_LOGIC_VECTOR(15 downto 0) := (others => '0');
    d_out2: out STD_LOGIC_VECTOR(15 downto 0) := (others => '0')
  );
    end component;
    component x7seg is
        Port ( sw : in  STD_LOGIC_VECTOR (3 downto 0);
           sevenseg : out  STD_LOGIC_VECTOR (6 downto 0));
    end component;
    component afficheur is
        Port ( data : in  STD_LOGIC_VECTOR (15 downto 0);

           byte : out  STD_LOGIC_VECTOR (3 downto 0);

           anodes : out  STD_LOGIC_VECTOR (3 downto 0);

           clk : in  STD_LOGIC);
    end component;
    component E190 is
        Port ( clk : in  STD_LOGIC;
           E190, clk190 : out  STD_LOGIC);
    end component;
    
    component btn_pulse is
    Port ( button : in STD_LOGIC;
           E : in STD_LOGIC;
           clk : in STD_LOGIC;
           button_out : out STD_LOGIC);
    end component;

    CONSTANT NB_PROC : INTEGER := 16;
    constant NB_BIT_TRANSF : INTEGER := 16;
    constant NB_DATA : INTEGER := NB_PROC * NB_BIT_TRANSF - 1; 
    CONSTANT NB_ITER : INTEGER := 65536;
    signal data_transfer: std_logic_vector(NB_DATA downto 0);
    signal sum : std_logic_vector(15 downto 0);

    signal enable, lclk, cntn_i: STD_LOGIC;
    signal data_seg: std_logic_vector(3 downto 0);

    pure function SUM_TRANSFER (
        data_transfer : STD_LOGIC_VECTOR(NB_DATA downto 0)
    ) return STD_LOGIC_VECTOR is
        variable helper : std_logic_vector(15 downto 0);
    begin
        if NB_PROC = 1 then
            return data_transfer(15 downto 0);
        end if;

        helper := data_transfer(15 downto 0) + data_transfer(31 downto 16);
        for j in 3 to NB_PROC loop
            helper := helper + data_transfer(NB_BIT_TRANSF * (j)-1 downto NB_BIT_TRANSF * (j-1));
        end loop;

        return helper;
    end function;

begin
    pls: btn_pulse PORT MAP(button=>btnE, clk=>clk, E=>enable, button_out=>cntn_i);
    gen_bus : for PROC_ID in 0 to (NB_PROC-1) generate
        ba: mbus GENERIC MAP (RAND_START=>PROC_ID * (NB_ITER / NB_PROC)) PORT MAP(clk=>clk, continue=>cntn_i, d_in=>sw, d_out1=>OPEN, d_out2=>data_transfer(NB_BIT_TRANSF * (PROC_ID+1)-1 downto NB_BIT_TRANSF * PROC_ID));
    end generate;

    sum <= SUM_TRANSFER(data_transfer);

    cseg: x7seg PORT MAP(sw=>data_seg, sevenseg=>seg);
    display: afficheur PORT MAP(data=>sum, byte=>data_seg, anodes=>an, clk=>lclk);
    clk190: E190 PORT MAP(clk=>clk, E190=>enable, clk190=>lclk);
    
end Behavioral;
