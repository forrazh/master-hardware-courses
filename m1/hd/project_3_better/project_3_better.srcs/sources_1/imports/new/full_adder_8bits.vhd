----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/16/2023 03:31:11 PM
-- Design Name: 
-- Module Name: full_adder_8bits - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity full_adder_8bits is
    Port ( word_1 : in STD_LOGIC_VECTOR (7 downto 0);
           word_2 : in STD_LOGIC_VECTOR (7 downto 0);
           SUM   : out STD_LOGIC_VECTOR (4 downto 0));
end full_adder_8bits;

architecture Behavioral of full_adder_8bits is
    component full_adder_1bit
        Port(
            a, b: in std_logic;
            cin : in std_logic;
            s   : out std_logic;
            cout: out std_logic
        );
     end component;
    component full_adder_4bits
        Port(
            A, B: in std_logic_vector(3 downto 0);
            cin : in std_logic;
            S   : out std_logic_vector(2 downto 0);
            cout: out std_logic
        );
     end component;
    signal c0, c1 : std_logic;
    signal left, right :std_logic_vector(2 downto 0);
    signal r_s0, r_s1, r_s2, r_s3 : std_logic;
    signal r_c0, r_c1, r_c2, r_c3 : std_logic;
begin
    fa4_0 : full_adder_4bits port map (A=>word_1(3 downto 0), B=>word_2(3 downto 0), cin=>'0', S=>left,  cout=>c0);
--    fa4_1 : full_adder_4bits port map (A=>word_1(7 downto 4), B=>word_2(7 downto 4), cin=>'0', S=>right, cout=>c1);

--    fa1_0 : full_adder_1bit port map (a=>left(0), b=>right(0), cin=>'0', s=>r_s0,  cout=>r_c0);
--    fa1_1 : full_adder_1bit port map (a=>left(1), b=>right(1), cin=>r_c0, s=>r_s1,  cout=>r_c1);
--    fa1_2 : full_adder_1bit port map (a=>left(2), b=>right(2), cin=>r_c1, s=>r_s2,  cout=>r_c2);
--    fa1_3 : full_adder_1bit port map (a=>c0, b=>c1, cin=>r_c2, s=>r_s3,  cout=>r_c3);

    SUM <= "0" & c0 & left;
end Behavioral;
