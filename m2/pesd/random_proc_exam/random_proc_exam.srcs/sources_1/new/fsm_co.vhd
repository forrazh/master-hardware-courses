library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity fsm_co is
    Port ( 
         clk         : in STD_LOGIC := '0';
         continue    : in STD_LOGIC := '0';
         instruction : in STD_LOGIC_VECTOR(15 downto 0) := x"0000";
         COinc       : out STD_LOGIC := '0';
         RILoad      : out STD_LOGIC := '0';
         Z           : out STD_LOGIC := '0';
         NZ          : out STD_LOGIC := '0';
         src         : out STD_LOGIC_VECTOR (3 downto 0) := x"0";
         dst         : out STD_LOGIC_VECTOR (3 downto 0) := x"0";
         op          : out STD_LOGIC_VECTOR (3 downto 0) := x"0" 
    );
end fsm_co;

architecture Behavioral of fsm_co is

   type state_type is (st_fetch_execute, st_pause);
   signal state, next_state : state_type := st_fetch_execute;

   signal  COinc_i : STD_LOGIC := '0';
   signal RILoad_i : STD_LOGIC := '0';
   signal  pause_i : STD_LOGIC := '0';
   signal      Z_i : STD_LOGIC := '0';
   signal     NZ_i : STD_LOGIC := '0';
   signal    dst_i : STD_LOGIC_VECTOR (3 downto 0) := x"0";
   signal    src_i : STD_LOGIC_VECTOR (3 downto 0) := x"0";
   signal     op_i : STD_LOGIC_VECTOR (3 downto 0) := x"0";

   -- Registers (useful ?)

   constant c_no     : STD_LOGIC_VECTOR := x"0";
   constant c_R1    : STD_LOGIC_VECTOR := x"1";
   constant c_R2    : STD_LOGIC_VECTOR := x"2";
   constant c_R3    : STD_LOGIC_VECTOR := x"3";
   constant c_R4    : STD_LOGIC_VECTOR := x"4";
   constant c_R5    : STD_LOGIC_VECTOR := x"5";
   constant c_Rout1 : STD_LOGIC_VECTOR := x"6";
   constant c_Rin   : STD_LOGIC_VECTOR := x"7";
   constant c_Rout2 : STD_LOGIC_VECTOR := x"8";
   constant c_Rsrc1 : STD_LOGIC_VECTOR := x"9";
   constant c_Rsrc2 : STD_LOGIC_VECTOR := x"A";
   constant c_Rdest : STD_LOGIC_VECTOR := x"B";
   constant c_RAM   : STD_LOGIC_VECTOR := x"C";
   constant c_RDM   : STD_LOGIC_VECTOR := x"D";
   constant c_CO    : STD_LOGIC_VECTOR := x"E";
   constant c_RI    : STD_LOGIC_VECTOR := x"F";

   -- Instructions
   constant SELEC_CODEOP_ALU : STD_LOGIC_VECTOR := x"1";
   constant SELEC_CODEOP_POZ : STD_LOGIC_VECTOR := x"F";
   constant CODEOP_MOV       : STD_LOGIC_VECTOR := x"0";
   -- constant CODEOP_NOOP      : STD_LOGIC_VECTOR := x"1";
   constant CODEOP_MVI       : STD_LOGIC_VECTOR := x"2";
   constant CODEOP_MVZ       : STD_LOGIC_VECTOR := x"3";
   constant CODEOP_MNZ       : STD_LOGIC_VECTOR := x"4";
   constant CODEOP_MIZ       : STD_LOGIC_VECTOR := x"5";
   constant CODEOP_MINZ      : STD_LOGIC_VECTOR := x"6";

begin

SYNC_PROC: process (clk, continue)
   begin
      if (rising_edge(clk)) then
         state <= next_state;
         Coinc <= COinc_i;
         RILoad <= RILoad_i;
         src <= src_i;
         dst <= dst_i;
         op <= op_i;
         Z <= Z_i;
         NZ <= NZ_i;
      end if;
   end process;

   OUTPUT_DECODE: process (state, instruction)
   begin
      op_i <= x"0";
      COinc_i <= '0';
      RILoad_i <= '1';
      Z_i <= '0';
      NZ_i <= '0';
      src_i <= c_no;
      dst_i <= c_no;


      case (state) is
         when st_fetch_execute =>
            COinc_i <= '1';
            RILoad_i <= '1';
            pause_i <= '0';

            case (instruction(15 downto 12)) is
               when CODEOP_MOV =>
                  src_i <= instruction(7 downto 4);
                  dst_i <= instruction(3 downto 0);
               when SELEC_CODEOP_ALU =>
                  op_i <= instruction(11 downto 8);
                  dst_i <= c_Rdest; 
               when SELEC_CODEOP_POZ =>
                  COinc_i <= '0';
                  RIload_i <= '0';
                  pause_i <= '1'; 
               when CODEOP_MVI =>
                  src_i <= c_RI;
                  dst_i <= instruction(3 downto 0);
               when CODEOP_MVZ =>
                  Z_i <= '1';
                  src_i <= instruction(7 downto 4);
                  dst_i <= instruction(3 downto 0);
               when CODEOP_MNZ =>
                  NZ_i <= '1';
                  src_i <= instruction(7 downto 4);
                  dst_i <= instruction(3 downto 0);
               when CODEOP_MIZ =>
                  Z_i <= '1';
                  src_i <= c_RI;
                  dst_i <= instruction(3 downto 0);
               when CODEOP_MINZ =>
                  NZ_i <= '1';
                  src_i <= c_RI;
                  dst_i <= instruction(3 downto 0);
               when others =>
            end case;
         when st_pause => 
            pause_i <= '1';
      end case;
   end process;

   NEXT_STATE_DECODE: process (state, continue, pause_i)
   begin
      next_state <= state;
      case (state) is
         when st_fetch_execute =>
            if pause_i = '1' then
                next_state <= st_pause;
            end if;
         when st_pause =>
            if continue ='1' then
                next_state <= st_fetch_execute;
            end if;
         when others =>
            next_state <= state;
      end case;
   end process;

end Behavioral;