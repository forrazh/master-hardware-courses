----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/30/2023 11:01:03 AM
-- Design Name: 
-- Module Name: btn_pulse - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity btn_pulse is
    Port ( button : in STD_LOGIC;
           E : in STD_LOGIC;
           clk : in STD_LOGIC;
           button_out : out STD_LOGIC);
end btn_pulse;

architecture Behavioral of btn_pulse is
    signal q0, q1, q2, q3, q4, q5 : std_logic;
    
    pure function and3 (
           i0 : STD_LOGIC;
           i1 : STD_LOGIC;
           i2 : STD_LOGIC
        ) return STD_LOGIC is
    begin
        return (i0 and i1 and i2);
    end function;
begin
    process (button, E, clk)
    begin 
        if rising_edge(clk) then -- FDE 0
            if E = '1' then 
                q0 <= button;
            end if;
        end if;
    end process;
    process (button, E, clk)
    begin 
        if rising_edge(clk) then -- FDE 1
            if E = '1' then 
                q1 <= q0;
            end if;
        end if;
    end process;
    process (button, E, clk)
    begin 
        if rising_edge(clk) then -- FDE 2
            if E = '1' then 
                q2 <= q1;
            end if;
        end if;
    end process;
    process (button, clk)
    begin 
        if rising_edge(clk) then -- FD 0
            q3 <= and3(q0, q1, q2);
        end if;
    end process;
    process (button, clk)
    begin 
        if rising_edge(clk) then -- FD 1
            q4 <= q3;
        end if;
    end process;
    process (button, clk)
    begin         
        if rising_edge(clk) then -- FD 2
            q5 <= q4;
        end if;
        button_out <= and3(q3, q4, not q5);
    end process;
end Behavioral;
