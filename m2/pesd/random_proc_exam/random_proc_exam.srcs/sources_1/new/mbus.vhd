----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/11/2023 09:50:37 AM
-- Design Name: 
-- Module Name: mbus - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity mbus is
  GENERIC (NB_ITER_RND : INTEGER := 2; RAND_START : INTEGER := 0);
  Port ( 
    clk : in STD_LOGIC;
    continue: in STD_LOGIC;
    d_in: in STD_LOGIC_VECTOR(15 downto 0) := (others => '0');
    d_out1: out STD_LOGIC_VECTOR(15 downto 0) := (others => '0');
    d_out2: out STD_LOGIC_VECTOR(15 downto 0) := (others => '0')
  );
end mbus;

architecture Behavioral of mbus is
    signal s_out: STD_LOGIC_VECTOR(15 downto 0) := (others => '0');
    signal CO_i: STD_LOGIC_VECTOR(15 downto 0)  := (others => '0');
    signal CO_o: STD_LOGIC_VECTOR(15 downto 0) := (others => '0');
    signal src1_i: STD_LOGIC_VECTOR(15 downto 0) := (others => '0');
    signal src2_i: STD_LOGIC_VECTOR(15 downto 0) := (others => '0');
    signal dest_i: STD_LOGIC_VECTOR(15 downto 0) := (others => '0');
    signal B2R: STD_LOGIC_VECTOR(15 downto 0) := (others => '0');
    signal R2B: STD_LOGIC_VECTOR(15 downto 0) := (others => '0');
    signal ri_out, rd_i: STD_LOGIC_VECTOR(15 downto 0) := (others => '0');
    signal src_i: STD_LOGIC_VECTOR(3 downto 0) := (others => '0');
    signal dst_i: STD_LOGIC_VECTOR(3 downto 0) := (others => '0');
    signal v_dst_i: STD_LOGIC_VECTOR(3 downto 0) := (others => '0');
    signal op_i: STD_LOGIC_VECTOR(3 downto 0) := (others => '0');

    signal COinc_i: STD_LOGIC := '0';
    signal RILoad_i: STD_LOGIC := '0';
    signal carry_i: STD_LOGIC := '0';
    signal Z_i: STD_LOGIC := '0';
    signal NZ_i: STD_LOGIC := '0';
    signal cond_jump_i: STD_LOGIC := '0';
    
    signal din, s_bus, instruction : STD_LOGIC_VECTOR(15 downto 0):= (others => '0');

    component mregister
        Port (
         d: in STD_LOGIC_VECTOR(15 downto 0);
         R2B: in STD_LOGIC;
         B2R : in STD_LOGIC;
         clk : in STD_LOGIC;
         q_in: out STD_LOGIC_VECTOR(15 downto 0);
         q: out STD_LOGIC_VECTOR(15 downto 0)
    );
    end component;
    component register_immediate
        Port (
         d: in STD_LOGIC_VECTOR(15 downto 0);
         R2B: in STD_LOGIC;
         B2R : in STD_LOGIC;
         clk : in STD_LOGIC;
         q_in: out STD_LOGIC_VECTOR(15 downto 0);
         q: out STD_LOGIC_VECTOR(15 downto 0)
    );
    end component;
    component fsm_co is
    Port ( clk : in STD_LOGIC;
            continue : in STD_LOGIC;
            instruction : in STD_LOGIC_VECTOR(15 downto 0) := x"0000";
            COinc : out STD_LOGIC := '0';
            RILoad : out STD_LOGIC := '0';
            Z           : out STD_LOGIC := '0';
            NZ          : out STD_LOGIC := '0';
            src : out STD_LOGIC_VECTOR (3 downto 0) := x"0";
            dst : out STD_LOGIC_VECTOR (3 downto 0) := x"0";
            op : out STD_LOGIC_VECTOR (3 downto 0) := x"0"
         );
  end  component;

  component incrementor is
    Port ( I0 : in STD_LOGIC_VECTOR (15 downto 0);
           I1 : in STD_LOGIC_VECTOR (15 downto 0);
           S0 : in STD_LOGIC;
           S1 : in STD_LOGIC;
           O : out STD_LOGIC_VECTOR (15 downto 0));
end component;

COMPONENT dist_mem_gen_0 IS
  PORT (
    a : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    spo : OUT STD_LOGIC_VECTOR(15 DOWNTO 0)
  );
END COMPONENT;

component demultiplexer is
    Port ( E : in STD_LOGIC;
           A : in STD_LOGIC_VECTOR (3 downto 0);
           D : out STD_LOGIC_VECTOR (15 downto 0));
end component;

component alu is
    GENERIC (NB_ITER_RND : INTEGER := 2; RAND_START : INTEGER := 0);
    Port ( src1 : in STD_LOGIC_VECTOR (15 downto 0);
           src2 : in STD_LOGIC_VECTOR (15 downto 0);
           op : in STD_LOGIC_VECTOR (3 downto 0);
           clk : in STD_LOGIC;
           carry : out STD_LOGIC;
           dest : out STD_LOGIC_VECTOR (15 downto 0));
end component;

   component z_nz_tester is
      Port ( d : in STD_LOGIC_VECTOR (15 downto 0);
            test_Z : in STD_LOGIC;
            test_NZ : in STD_LOGIC;
            S : out STD_LOGIC);
   end component;

--   component dest_enabler is
--    Port ( dest_in : in STD_LOGIC_VECTOR (3 downto 0);
--           dest_out : out STD_LOGIC_VECTOR (3 downto 0);
--           enable : in STD_LOGIC);
--end component;

  constant c_R1    : integer :=  1;
  constant c_R2    : integer :=  2;
  constant c_R3    : integer :=  3;
  constant c_R4    : integer :=  4;
  constant c_R5    : integer :=  5;
  constant c_Rin   : integer :=  7; -- switches
  constant c_Rout1 : integer :=  6;
  constant c_Rout2 : integer :=  8;
  constant c_Rsrc1 : integer :=  9; 
  constant c_Rsrc2 : integer := 10;
  constant c_Rdest : integer := 11;
  constant c_RAM   : integer := 12;
  constant c_RDM   : integer := 13;
  constant c_CO    : integer := 14;
  constant c_RI    : integer := 15;

begin
        demux_r2b: demultiplexer PORT MAP (E=>'1', A=>src_i, D=>R2B);
        demux_b2r: demultiplexer PORT MAP (E=>cond_jump_i, A=>dst_i, D=>B2R);

        fsm_co_i: fsm_co PORT MAP (clk => clk, continue=>continue, instruction=>instruction, COinc => COinc_i, RILoad => RILoad_i, src=>src_i, dst=>dst_i, OP=>op_i, Z=>Z_i, NZ=>NZ_i);
        -- fsm_old_i: fsm_old PORT MAP (clk => clk, COinc => COinc_i, RILoad => RILoad_i, Rout2 => Rout2_i, RI2B => RI2B_i, B2CO => B2CO_i);
        inc_co: incrementor PORT MAP (I0 => din, I1 => CO_o, S0 => B2R(c_CO), S1 => COinc_i, O => CO_i);
        dmg: dist_mem_gen_0 PORT MAP (a =>CO_o(7 downto 0), spo => instruction);

        m_alu: alu GENERIC MAP (NB_ITER_RND=>NB_ITER_RND, RAND_START=>RAND_START) PORT MAP(src1 => src1_i, src2=>src2_i, op=>op_i, clk=>clk, carry=>carry_i, dest=>dest_i);

        z_nz_tst : z_nz_tester PORT MAP(d=>rd_i, test_Z=> Z_i, test_NZ=>NZ_i, S=>cond_jump_i);
--        dst_en : dest_enabler PORT MAP(dest_in=>dst_i, dest_out=>v_dst_i, enable=>cond_jump_i);
        
        R2:    mregister          PORT MAP (d=>din,         R2B=>R2B(c_R2),    B2R=>B2R(c_R2),    clk=>clk,               q=>s_bus );
        R1:    mregister          PORT MAP (d=>din,         R2B=>R2B(c_R1),    B2R=>B2R(c_R1),    clk=>clk,               q=>s_bus );
        R3:    mregister          PORT MAP (d=>din,         R2B=>R2B(c_R3),    B2R=>B2R(c_R3),    clk=>clk,               q=>s_bus );
        R4:    mregister          PORT MAP (d=>din,         R2B=>R2B(c_R4),    B2R=>B2R(c_R4),    clk=>clk,               q=>s_bus );
        R5:    mregister          PORT MAP (d=>din,         R2B=>R2B(c_R5),    B2R=>B2R(c_R5),    clk=>clk,               q=>s_bus );
        Rout1: mregister          PORT MAP (d=>din,         R2B=>R2B(c_Rout1), B2R=>B2R(c_Rout1), clk=>clk, q_in=>d_out1, q=>s_bus );
        Rin:   mregister          PORT MAP (d=>d_in,        R2B=>R2B(c_Rin),   B2R=>'1',          clk=>clk,               q=>s_bus );
        Rout2: mregister          PORT MAP (d=>din,         R2B=>R2B(c_Rout2), B2R=>B2R(c_Rout2), clk=>clk, q_in=>d_out2, q=>s_bus );
        Rsrc1: mregister          PORT MAP (d=>din,         R2B=>R2B(c_Rsrc1), B2R=>B2R(c_Rsrc1), clk=>clk, q_in=>src1_i, q=>s_bus );
        Rsrc2: mregister          PORT MAP (d=>din,         R2B=>R2B(c_Rsrc2), B2R=>B2R(c_Rsrc2), clk=>clk, q_in=>src2_i, q=>s_bus );
        Rdest: mregister          PORT MAP (d=>dest_i,      R2B=>R2B(c_Rdest), B2R=>B2R(c_Rdest), clk=>clk, q_in=>rd_i,   q=>s_bus );
        RAM:   mregister          PORT MAP (d=>din,         R2B=>R2B(c_RAM),   B2R=>B2R(c_RAM),   clk=>clk,               q=>s_bus );
        RDM:   mregister          PORT MAP (d=>din,         R2B=>R2B(c_RDM),   B2R=>B2R(c_RDM),   clk=>clk,               q=>s_bus );
        CO:    mregister          PORT MAP (d=>CO_i,        R2B=>R2B(c_CO),    B2R=>'1',          clk=>clk, q_in=>CO_o,   q=>s_bus );
        RI:    register_immediate PORT MAP (d=>instruction, R2B=>R2B(c_RI),    B2R=>RIload_i,     clk=>clk, q_in=>ri_out, q=>s_bus );
        
        
        din <= s_bus;
end Behavioral;
