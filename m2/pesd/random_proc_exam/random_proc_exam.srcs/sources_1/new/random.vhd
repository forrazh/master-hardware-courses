----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10/08/2023 04:30:04 PM
-- Design Name: 
-- Module Name: random - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity random is
    GENERIC (TO_SKIP : INTEGER := 1; RAND_START : INTEGER := 0);

    Port ( clk : in STD_LOGIC;
           CE : in STD_LOGIC;
           Q : out STD_LOGIC_VECTOR (15 downto 0));
end random;

architecture Behavioral of random is

    pure function step (
        curr_val : STD_LOGIC_VECTOR(15 downto 0) := x"8000";
        iter : INTEGER := 1
    ) return STD_LOGIC_VECTOR is
        variable helper : std_logic_vector(15 downto 0);
    begin
        helper := curr_val;
        for j in 1 to iter loop
            helper(14 downto 0) := helper(15 downto 1);
            helper(15) := helper(0) XOR helper(1) XOR helper(3) XOR helper(12);
        end loop;
        return helper;
    end function;


    signal Q_i : STD_LOGIC_VECTOR(15 downto 0) := step(x"8000", RAND_START);

begin

    process (CE, clk)
    begin 
        if rising_edge(clk) then 
           if CE = '1' then 
                q_i <= step(q_i, 1);
           end if;
        end if;
    end process;    

    Q <= Q_i;
end Behavioral;
