----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/02/2023 05:06:56 PM
-- Design Name: 
-- Module Name: x7seg - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity x7seg is
    Port ( 
         input_data: in STD_LOGIC_VECTOR (3 downto 0);
         is_special: in std_logic;
         anodes    : out std_logic_vector (3 downto 0);
         output_seg: out STD_LOGIC_VECTOR (6 downto 0) 
    );
end x7seg;

architecture Behavioral of x7seg is
    component classic_display 
        Port (
             input_data: in STD_LOGIC_VECTOR (3 downto 0);
             anodes    : out std_logic_vector (3 downto 0);
             output_seg: out STD_LOGIC_VECTOR (6 downto 0)
         );
    end component;
    
    component special_display
            Port (
             input_data: in STD_LOGIC_VECTOR (3 downto 0);
             anodes    : out std_logic_vector (3 downto 0);
             output_seg: out STD_LOGIC_VECTOR (6 downto 0)
         );
    end component;

    signal output_special : std_logic_vector (6 downto 0) := "0000000";
    signal output_classic : std_logic_vector (6 downto 0) := "0000000";
    signal anodes_special : std_logic_vector (3 downto 0) := "0000";
    signal anodes_classic : std_logic_vector (3 downto 0) := "1110";
begin
    handle_classic: classic_display port map (input_data=>input_data, anodes=>anodes_classic, output_seg=>output_classic);
    handle_special: special_display port map (input_data=>input_data, anodes=>anodes_special, output_seg=>output_special);

    output_seg <= output_special when is_special = '1' else output_classic;
    anodes     <= anodes_special when is_special = '1' else anodes_classic;
end Behavioral;
