----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/21/2023 04:40:09 PM
-- Design Name: 
-- Module Name: shift_process - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity shift_process is
 Port ( 
 
 btn : in STD_LOGIC_VECTOR (4 downto 0)   := b"00000";
-- btnC : in STD_LOGIC:= '0';
           sw  : in STD_LOGIC_VECTOR (14 downto 1)  := b"000000000000000";
           led : out STD_LOGIC_VECTOR (15 downto 0) := x"0000";
           clk : in std_logic := '0');
end shift_process;

architecture Behavioral of shift_process is
component clock_4hz is
    Port ( clk : in STD_LOGIC;
           clk_4 : out STD_LOGIC);
end component;
    signal clk_o : std_logic := '0';
    constant LED_ON : std_logic := '1';
    constant WALL : std_logic := '1';
    constant LEFT : std_logic := '1';
    constant RIGHT : std_logic := '0';
    constant DON_T_CHANGE : std_logic_vector := x"0000";
begin
    change_clk: clock_4hz PORT MAP (clk=>clk, clk_4=>clk_o);
    

process (clk_o)  
    variable Q    : std_logic_vector(15 downto 0) := b"0000000000000001";
    variable last : std_logic := '0';
    variable change_me : std_logic_vector(15 downto 0);
    variable direction : std_logic := RIGHT;
begin
    if rising_edge(clk_o) then
        if not (change_me = DON_T_CHANGE) then 
            direction := not direction;
        else
            direction := direction;
        end if;
        
        if direction = LEFT then
            last := Q(15);
            Q(15 downto 1) := Q(14 downto 0);
            Q(0) := last;
        else
            last := Q(0);
            Q(14 downto 0) := Q(15 downto 1);
            Q(15) := last;
        end if;
     end if;
     
     change_me := WALL & sw & WALL and Q;
     led<=Q;

end process;
   
end Behavioral;
