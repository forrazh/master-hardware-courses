----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/14/2023 07:21:02 PM
-- Design Name: 
-- Module Name: cnt_16 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity cnt_16 is
    Port ( word : in STD_LOGIC_VECTOR (15 downto 0);
           SUM : out STD_LOGIC_VECTOR (4 downto 0));
end cnt_16;

architecture Behavioral of cnt_16 is
    signal left, right : std_logic_vector(3 downto 0);
    
    component cnt_8
        Port (
            word: in std_logic_vector(7 downto 0);
            SUM : out std_logic_vector(3 downto 0)
        );
    end component;
begin
    left_task : cnt_8 port map (word=>word(15 downto 8), SUM=>left);
    right_task: cnt_8 port map (word=>word(7 downto 0), SUM=>right);
    SUM <= '0' & left + right;

end Behavioral;