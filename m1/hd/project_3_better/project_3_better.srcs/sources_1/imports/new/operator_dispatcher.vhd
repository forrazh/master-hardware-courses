----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/07/2023 05:40:10 PM
-- Design Name: 
-- Module Name: operator_dispatcher - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity operator_dispatcher is
    Port ( word_1 : in STD_LOGIC_VECTOR (7 downto 0);
           word_2 : in STD_LOGIC_VECTOR (7 downto 0);
           SUM : out STD_LOGIC_VECTOR (4 downto 0);
           is_special : out std_logic;
           operator : in STD_LOGIC_VECTOR (4 downto 0));
end operator_dispatcher;

architecture Behavioral of operator_dispatcher is
    component add_4 
         port (
            word_1, word_2 : in std_logic_vector (3 downto 0);
            SUM : out std_logic_vector (4 downto 0)
         );
    end component;

    component and_4 
         port (
            word_1, word_2 : in std_logic_vector (3 downto 0);
            SUM : out std_logic_vector (4 downto 0)
         );
    end component;
    
    component or_4 
         port (
            word_1, word_2 : in std_logic_vector (3 downto 0);
            SUM : out std_logic_vector (4 downto 0)
         );
    end component;
        
    component xor_4 
         port (
            word_1, word_2 : in std_logic_vector (3 downto 0);
            SUM : out std_logic_vector (4 downto 0)
         );
    end component;
    component cmp_8
         port (
            word_1, word_2 : in std_logic_vector (7 downto 0);
            SUM : out std_logic
         );
    end component;
    component cnt_16 
         port (
            word: in std_logic_vector (15 downto 0);
            SUM : out std_logic_vector (4 downto 0)
         );
    end component;
    component par_16 
         port (
            word: in std_logic_vector (15 downto 0);
            SUM : out std_logic
         );
    end component;
    
    signal l_cnt_res, l_add_res, l_xor_res, l_and_res, l_or_res : std_logic_vector (4 downto 0);
    signal l_cmp_res, l_par_res : std_logic;
    
    constant SPECIAL_SEG : std_logic := '1';
    constant CLASSIC_SEG : std_logic := '0';
    constant PAR_OFFSET : std_logic_vector := "0001";
    constant CMP_OFFSET : std_logic_vector := "0010";
    constant ERROR_CASE  : std_logic_vector := b"11111";
    
    constant ADD_OPE : std_logic_vector := "00000";
    constant AND_OPE : std_logic_vector := "00001";
    constant  OR_OPE : std_logic_vector := "00010";
    constant XOR_OPE : std_logic_vector := "00011";
    constant CNT_OPE : std_logic_vector := "10000";
    constant CMP_OPE : std_logic_vector := "01000";
    constant PAR_OPE : std_logic_vector := "00100";
begin
    add_task:  add_4 port map ( word_1=>word_1(3 downto 0), word_2=>word_2(7 downto 4), SUM=>l_add_res);
    and_task:  and_4 port map ( word_1=>word_1(3 downto 0), word_2=>word_2(7 downto 4), SUM=>l_and_res);
    or_task :   or_4 port map ( word_1=>word_1(3 downto 0), word_2=>word_2(7 downto 4), SUM=>l_or_res);
    xor_task:  xor_4 port map ( word_1=>word_1(3 downto 0), word_2=>word_2(7 downto 4), SUM=>l_xor_res);
    cmp_task:  cmp_8 port map ( word_1=>word_1, word_2=>word_2, SUM=>l_cmp_res);
    par_task: par_16 port map ( word=>word_2 & word_1, SUM=>l_par_res);
    cnt_task: cnt_16 port map ( word=>word_2 & word_1, SUM=>l_cnt_res);
      
    process (operator)
    begin 
        case operator is
        when ADD_OPE =>
            is_special <= CLASSIC_SEG;
            SUM <= l_add_res;
        when AND_OPE =>
            is_special <= CLASSIC_SEG;
            SUM <= l_and_res;
        when  OR_OPE =>
            is_special <= CLASSIC_SEG;
            SUM <= l_or_res;
        when XOR_OPE =>
            is_special <= CLASSIC_SEG;
            SUM <= l_xor_res;
        when CNT_OPE =>
            is_special <= CLASSIC_SEG;
            SUM <= l_cnt_res;
        when CMP_OPE =>
            is_special <= SPECIAL_SEG;
            SUM <= CMP_OFFSET & l_cmp_res;
        when PAR_OPE =>
            is_special <= SPECIAL_SEG;
            SUM <= PAR_OFFSET & l_par_res;
        when others =>
            is_special <= SPECIAL_SEG;
            SUM <= ERROR_CASE;
        end case;
    end process;
end Behavioral;
