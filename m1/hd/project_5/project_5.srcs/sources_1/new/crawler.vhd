----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/04/2023 10:59:57 AM
-- Design Name: 
-- Module Name: crawler - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity crawler is
    Port ( clk : in STD_LOGIC;
           output : out STD_LOGIC_VECTOR (7 downto 0));
end crawler;

architecture Behavioral of crawler is
    signal slow_clk : STD_LOGIC := '0';
begin
    PROCESS (clk)
        variable counter : integer := 0;
    begin
        if rising_edge(clk) then
            if counter = 45 then
                slow_clk <= '1';
                counter := 0;
            else 
                slow_clk <= '0';
                counter := counter + 1;
            end if;
        end if;
    end process;

    process (slow_clk)
        variable Q : std_logic_vector(7 downto 0) := b"10000000";
        variable smol : STD_LOGIC := '0';
    begin
        if rising_edge(slow_clk) then
            smol := Q(7);
            Q(7 downto 1) := Q(6 downto 0);
            Q(0) := smol;
        end if;

        output <= Q;
    end process;

end Behavioral;
