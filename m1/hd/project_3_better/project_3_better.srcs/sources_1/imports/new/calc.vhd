----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/07/2023 02:46:51 PM
-- Design Name: 
-- Module Name: calc - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity calc is
    Port ( sw  : in STD_LOGIC_VECTOR (15 downto 0)  := x"0000";
           led : out STD_LOGIC_VECTOR (15 downto 0) := x"0000";
           seg : out STD_LOGIC_VECTOR (6 downto 0)  := b"1000000";
           btnC, btnU, btnD, btnL, btnR : in std_logic := '0';
           an  : out STD_LOGIC_VECTOR (3 downto 0)  := b"1110"
         );
end calc;


architecture Behavioral of calc is
    signal out_data : std_logic_vector (4 downto 0) := "00000";
    
    component operator_dispatcher
        port (
            word_1, word_2 : in std_logic_vector (7 downto 0);
            operator : in std_logic_vector (4 downto 0);
            SUM : out std_logic_vector (4 downto 0);
            is_special : out std_logic
        );
    end component;
    component display_dispatcher
        port (
            in_data : in std_logic_vector (4 downto 0);
            is_special : in std_logic;
            o_seg : out STD_LOGIC_VECTOR (6 downto 0);
            an : out STD_LOGIC_VECTOR (3 downto 0) := "1110";
            leds : out std_logic_vector (15 downto 0)
        );
    end component;
    
    signal ope : std_logic_vector (4 downto 0) := b"00000"; 
    signal is_spe : std_logic := '0' ;
begin
    ope <= btnC & btnU & btnD & btnL & btnR;
    data_handler : operator_dispatcher port map (word_1=>sw(7 downto 0), word_2=>sw(15 downto 8), operator=>ope, SUM=>out_data, is_special=>is_spe);
    disp_handler :  display_dispatcher port map (in_data=>out_data, is_special=>is_spe, o_seg=>seg, an=>an, leds=>led ) ;
--    seg_display : x7seg port map (input_data=>out_data(3 downto 0), output_seg=>seg);
--    led_display : led_displayer port map (input_data=>out_data(4 downto 0), leds=>led);
end Behavioral;
