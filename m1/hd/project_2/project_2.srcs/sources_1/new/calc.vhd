----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/07/2023 02:46:51 PM
-- Design Name: 
-- Module Name: calc - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity calc is
    Port ( sw : in STD_LOGIC_VECTOR (15 downto 0) := x"0000";
           led : out STD_LOGIC_VECTOR (15 downto 0) := x"0000";
           seg : out STD_LOGIC_VECTOR (6 downto 0);
           btnL, btnR : in std_logic;
           an : out STD_LOGIC_VECTOR (3 downto 0) := "1110"
         );
end calc;


architecture Behavioral of calc is
    signal out_data : std_logic_vector(4 downto 0) := "00000";
    
    component operator_dispatcher
        port (
            word_1, word_2 : in std_logic_vector (3 downto 0);
            operator : in std_logic_vector (1 downto 0);
            SUM : out std_logic_vector (4 downto 0)
        );
    end component;
    
    component x7seg
        port ( 
                input_data : in std_logic_vector (3 downto 0);
                output_seg : out std_logic_vector (6 downto 0)
             );
    end component;
    
    component led_displayer
        port (
                input_data : in std_logic_vector (4 downto 0);
                leds : out std_logic_vector (15 downto 0)
             );
    end component;
    
    signal ope : std_logic_vector (1 downto 0); 
begin
    ope <= btnL & btnR;
    data_handler : operator_dispatcher port map (word_1=>sw(3 downto 0), word_2=>sw(15 downto 12), operator=>ope, SUM=>out_data);
    seg_display : x7seg port map (input_data=>out_data(3 downto 0), output_seg=>seg);
    led_display : led_displayer port map (input_data=>out_data, leds=>led);
end Behavioral;
