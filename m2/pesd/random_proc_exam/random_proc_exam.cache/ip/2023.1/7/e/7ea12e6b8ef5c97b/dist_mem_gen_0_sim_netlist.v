// Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
// Copyright 2022-2023 Advanced Micro Devices, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2023.1 (lin64) Build 3865809 Sun May  7 15:04:56 MDT 2023
// Date        : Sun Oct 15 17:36:02 2023
// Host        : enji running 64-bit EndeavourOS Linux
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ dist_mem_gen_0_sim_netlist.v
// Design      : dist_mem_gen_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35tcpg236-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "dist_mem_gen_0,dist_mem_gen_v8_0_13,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "dist_mem_gen_v8_0_13,Vivado 2023.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (a,
    spo);
  input [7:0]a;
  output [15:0]spo;

  wire [7:0]a;
  wire [15:0]spo;
  wire [15:0]NLW_U0_dpo_UNCONNECTED;
  wire [15:0]NLW_U0_qdpo_UNCONNECTED;
  wire [15:0]NLW_U0_qspo_UNCONNECTED;

  (* C_FAMILY = "artix7" *) 
  (* C_HAS_D = "0" *) 
  (* C_HAS_DPO = "0" *) 
  (* C_HAS_DPRA = "0" *) 
  (* C_HAS_I_CE = "0" *) 
  (* C_HAS_QDPO = "0" *) 
  (* C_HAS_QDPO_CE = "0" *) 
  (* C_HAS_QDPO_CLK = "0" *) 
  (* C_HAS_QDPO_RST = "0" *) 
  (* C_HAS_QDPO_SRST = "0" *) 
  (* C_HAS_WE = "0" *) 
  (* C_MEM_TYPE = "0" *) 
  (* C_PIPELINE_STAGES = "0" *) 
  (* C_QCE_JOINED = "0" *) 
  (* C_QUALIFY_WE = "0" *) 
  (* C_REG_DPRA_INPUT = "0" *) 
  (* c_addr_width = "8" *) 
  (* c_default_data = "0" *) 
  (* c_depth = "256" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_clk = "0" *) 
  (* c_has_qspo = "0" *) 
  (* c_has_qspo_ce = "0" *) 
  (* c_has_qspo_rst = "0" *) 
  (* c_has_qspo_srst = "0" *) 
  (* c_has_spo = "1" *) 
  (* c_mem_init_file = "dist_mem_gen_0.mif" *) 
  (* c_parser_type = "1" *) 
  (* c_read_mif = "1" *) 
  (* c_reg_a_d_inputs = "0" *) 
  (* c_sync_enable = "1" *) 
  (* c_width = "16" *) 
  (* is_du_within_envelope = "true" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_dist_mem_gen_v8_0_13 U0
       (.a(a),
        .clk(1'b0),
        .d({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .dpo(NLW_U0_dpo_UNCONNECTED[15:0]),
        .dpra({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .i_ce(1'b1),
        .qdpo(NLW_U0_qdpo_UNCONNECTED[15:0]),
        .qdpo_ce(1'b1),
        .qdpo_clk(1'b0),
        .qdpo_rst(1'b0),
        .qdpo_srst(1'b0),
        .qspo(NLW_U0_qspo_UNCONNECTED[15:0]),
        .qspo_ce(1'b1),
        .qspo_rst(1'b0),
        .qspo_srst(1'b0),
        .spo(spo),
        .we(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2023.1"
`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
bEktTo8XfP53J4LC9J1bzNOsr+DeYSQtsSeSeRwv1ROtu7MJT7BubpFM5B3JNITvmmXMIQ7cHCcM
BFy5Vu0fdwcQmgznzr1F4XAF5OH/PlBVKmCiA5IZpd+UQUMuy8l823afh4u8+Fg3bwZX7B36A3bn
Zez9yHjSKD7JGdQ9zA8=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
vAZQ8ZTe/MermX+omywGuwEzd7SLijiaDbuX0B9K4vjWUXvRoI6Em0qizreOX/qdo4JlybEpt70i
jJhVvWv69a9yKb8TMuvLagWbQydSwTJKTY6VSR/CtA2Uive8NvQyiQKFXLjR8k8OBlgOYmyzZEEM
vYgZLdnM3d2xSMMmeGF+dNh8tCJpM10LRaCrnj5w8L73RtOImlhI/zlR8cC5oo1TbyRV+JuHvvMZ
sYS3+4qn/f80Ugvao3cYMW0LtoTftK9oYpzhiyqg6hnJnbGsAENom2wqBpcRJf1vsI98WiJqDCuh
LIdMFI+M5KuqToM8D+FTQUOT2NniYpTmj5qTFg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
VpwnevLJi/mNDesLbbdRntRX/1KkSUuxvcBO6/opCSkxKA2w7s8Eyh+CvZJvHhBMtWZquJPlWZsE
d3toYaeyczcrzAzfKryx5nnTvscAyYnKl8QyY0fWsE1UqWjg6tazMCtzxlfF3HfKx/GSm3D/0NEz
xzyxLBgRosbKCX4YRV0=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MhGbYf5xy0E517prDNoCHbf/sVQ5JHlfzlh1Fz+rfDm8S3/Zt1g/AR2QuQPNwJUQO22hvTTB491a
xRG5ct3upD6ZdXgMesPA9KgwjRjoBp/uriYuT6Sb/yE2jugYl2qBGpqxN9n2OgAVfK3o9XZ/aIcR
St2PwrmKRzU/ZoYenWUMZ6ZRsVNlzFCEBcKop6f5TBy0bWAeebXRZ0Mot23DVX4pqVyFaQoXdmkm
56Vr2jGszkLic4M0JoKahUlQpnrZuHIWgFVd/RzXXP9HwYBRQTxaKnNX6eWTdksVvzAImMYoPa4G
PJJFf+gsNAKp5BIFXjwHfNC+Nerc6XzDmxe+pw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
jfnJJlFHpbB8S3PjID3rEIRi4fzY1WUZaITx6CJ38mSZfYSA13DJislb1OQ17w4Hnv5eGM/0GVgA
2jPR4wYaMzC8v3iDfETrH4kyrFglo3a/NDlACuR1U65YoHUnUu0UmMMovxQEnd9ByAfOtabZPL4j
FTvCoVMpwI8rdT4YJQ5pYXryESdM3NUe29p9OWbY1EalisEVViKuSwS4LzwtaOmrPecCE56FGEp+
2iyBMICOFF2PpT8Bqp39Z2rx4xyIiudZKo3LNimTm/UYBCnPAJ7XBIS+JiCIOkHsPER+wNivbtUb
J02F8ZLbEtS0qmUdYDXO4qqhc1njU9O6Uk9yNA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2022_10", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uOK6pXmc+RsarhB5GcgUPkseiDLhaN7KZ4C18Aqea9NqSbvIERAENTml4U58cVlx6j599K+L2aW5
rVMZLtj8UE4yfEDhtivrSdBYh446mqbnToHhH5r4BmzYnr6BUuXVZ4NIUU29WnaJUZxwrvZeCln4
GQCdP1kUA1Ozy9B47ndTYgOzCcZSr9w36W7ZA1gm34lqVpXYuGsaRTvk1DhS96aFGCeiCTbs5HM3
e0JPkZ7YUsMgWuRzE+jHE1TEMVjbPkpPjFGCYOEeDf2bc/2s2fPLA3bxMs61xUFH5LAd7Qrs9D2v
Mx+Vcfvo7kmp3J5LW99NXfA9OvG1JgjJ7ykhmw==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
OS52LCfxYaApFxxvQUqjJD8DSzwhbsM5irqCX6E4R0iBINlXI3QVmtLKp8vhPICYZWjEuTIVzohU
28vwAOP2ECPWOkJjN+ny9RQeAKmQhPbxHYOysXg4IgtMbK+ZODUoMyLIsJzz2yIFl5qvQeLBnc44
NvqDk7nFLhtrN9De4XV14FKtDvQG0BdWr2mXiS7WiEAQxiww87A0M8yP82JlG6ykYSwQh5G8K6pv
YHoqI8mKAC+KGuDltBnyBrKGip5pRq7Kf+0okVAOwt0lJwDvS0JMNEUg1HK/mEIR6TKUdd8B/fms
4qcaCBYsptjoZVCq4ygSG56x8uaQXMVsEALe2w==

`pragma protect key_keyowner="Atrenta", key_keyname="ATR-SG-RSA-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=384)
`pragma protect key_block
Aew/RSoMZUIh8oIZPhChM37w+R5unp+7pprfqezjGFUVX16UeT1spPFU1DaqTQvQkXhBe4/aNxvo
Y2eUJsQd8zSC9wBoevCnvwaHEv/IBc+OKmBzOPxO1hHXDVPtDZWdRCx+1y0ZYhQa+NA6jLP2zOJx
/emAZW55AWgZKKJS4QgantVgmUSyKVe/LlIVstraTkF4EzV092mOj1iPH/UqFFno9IwE1aOXuYuT
XrZU9D1dkPLBMg3CDwOi+bXRSgjvuueWT7ostJSFraLwDkurP1pYHHG4NDxYiDxMFWarWeII+T6v
hMJKd/8ZRrh5aHvGV5O/Hdc4rPitxa/cdQPAc0r2e2XWAJIdic09atzXXyU9o2vV/urpMsjSVva4
B5a/PwS16c18IMm6vAeFSLMo0T/jor1Q5SoxEC5QEkxvEfIUjjw7k0b1Crv5EfWz/sJ1LHwqlG7t
az+h03yAqvqGfOHC+7YoilYImR1NiLTCLgxnUfIvxo6woY4SgD+hLki4

`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="CDS_RSA_KEY_VER_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
iZJ+xdyrZbhNc8zYurF70yKiutV2IBjRXDiOZ/7w25UL6rCpY4Pd5gJN3+SNIoQ66bzRxlhaXMNu
tzoCM2kFY4N5ZbCy/S4rtBK0PUHKEVd7c5Btr5gn8BgQWiIafJ8Qa/8xqo95ocakFzN6/V+DNvyN
7FPkXDwuiaD0cmHW8XyOxnHM2b/XKHOibr7UKTRAomXyt7y80BVKpE50ddxXAxw9wlMn+gpW5Kpz
Dp8z4VH3uZrVv8Yl5RWELOQ3Uh0Xizb20mvc6Lu+BNoz0Ys9zZUaqKU71Kuv4s8vgPzrZXXNifo2
pU0aNj0oqAGlSTcTCBF8Tl6/jFvUXQEzYoIfiQ==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 7232)
`pragma protect data_block
zt5LIc7e/2+URbnpnt3gsA03WIMnByJvJ1nnXqDDaIyaO+zXj57QKPe8F76SWHJg1WBVvYKsFHo4
XwrHJf+0K4LD0B4Q7mHn4JvzF5ssln1l+nJZEAsYtb7hh4LdCufEIyB9MSUsHTD0caISjheQLnnG
Acy/R/NloRvP38iu0EujnM37GW/0BDxx3HEKWlArIn7ECL1NzQ+E1YnuUwqWM4ep5Jysg+FpQRq/
vS02Xo296s1x7sWuA23UcvTqSCgov527g8jvAw5wBrdG/wmG9xmMXA0HicZCJkQrG/gBbBsupZMN
LsugEM5MMrH1cK+c4s0Yqg8OfcZnvXazqVl9n7Em21xECyYuvTZk1EXJGr/1ONTVh3JG3BHC8nN1
u3x3HZxJ6rrGZ/mG7OEYs4LSQJmo9Vpk+6UcEBYU8iLpy8KOs5nsRBByO/WzmCfXAC03x1EzpNxU
jK78OavWf7dlX+J3PrIfdlu+mfBPRqoQVOF5lUzD/YCgvDccH06/LDh+wrjPdUqvW7VW7hlXpNhL
I4wemRRYjWI6AMsSXr8W3kGxW9ZxrJVqlsN8kYB1jNYJrrU0W0/fxp6uUU72t0DbH68zdM7LZ6QM
sjGYnRPZiYFWJSH77ZV0OYmpCT0tuHchgwszFveaSaVgoBi367SnU3MqrF5k92tIwUPlzd3TvZo7
ta+uvth1t6aYLF5j5hPb/HpxCl/h/PPDJwfnKoSqPt9VMgVXfTmQgMDrsLz4YsNw4/EzXhzdJWqP
sVNMS69RKDhds2OTakPNylKqmqZJqa0Igoni4LdJ8mxSNPVHT9svIHx55+8dzTfKme3XW4BRttOB
KvurQST1/TmK7Cjsc8l96tYGEx34+MLyaXIxu6iS8xOVh5rdus1R+lbc/gfy1JBcYTuDgybGAdJK
5wPXcQXNiDsE3g/DIl/YPbB5EHiot0mLeZiE0ESQrMV38CqdsP10PhHkAFCveE1bcan6WChb8v0v
YBIZ26lTtlyzs1fzLOPmaog7bdNiFUyxpVGI9PkaI7w+bC9Rcvw5ybecxxikVVrZghT5WRQhRwfH
EHEXj6VIwcttnWCbP7r9rXLA+gOgmeEWWoM0pK69SewdMWkWQ1CWAMuf8iBu92cnWkvJ4X8ksDB+
Vw8hX7FRpms1ZrNdoLxGqb+iUir8271ZFgrmOdm7U1ngNiOoy3yOZ7/1DJ2BsFGvqCm7pTItspIg
3/yZh26RPt9LRMObU8EEk4k5JetafztN2ficj19aewU93GyfW0Ca5OP3T4La6d3bQ5Sq332TLoMy
uvqOaR/QqkY9h4qmxMeCfPUf0oMd613ry3MvfdwLXLFRsrbjJDzJj5osR/oq5k53z+kR2eKyoza4
if3Y1TVfgo0p7bad5ICUDlF18RmSU8qAetu1V73t7XU+JUiRMo5beeRWGDFGLRSJwyhfPe+2n02r
xtgp8AQVRdAx1L4ygxIlADcicVrqpRQHV/l20mIisAfuPBZq+uji9j9Eiz6ZXo1ZJEKeUZXhuD7Q
2kPnrpIBEsBugAvvUI/dUDRhHa64VhP5wVNHQiRi7EnfNfI7kEcWASNXi6iNIZXOZQs8BMqJV2gW
rYIlBSsqMXc7oB5TSgqnlL6t+hA+VCvCMH6/sNrFJkQuefkD7De5OU/n8+9+9yjjyCSVvsoNgNlp
2F4KrJQi+OQr+tPlqbQQWksfNfcia4SDRZ1SNRvzC2XxUleyHcKOZOtKBPt8WWpO9M/PHM83uWvq
lw4hlWvFWpn1q7ARbH6Kmd0wB6Ewpg5zWAVCTu2fjqZ/jOA1leoUP0GwY1h988b7tx5p5ibl3I1T
rn7cJW0dh8cE8kATNGXwon5NVT0Fx5SlSFhncKs/Q/Zd9WltseqGpfCHp42Ru2JMiEzB6aP3xvMk
oMVg6gHCQNOSQeA1Vf53ek+NiK9CWUy1dMzZxk4lMQxqXeXPzvnV8zb7lex/wKTJt7IGmbJGJAM0
zDGhSxB+rpXlHNBElHTrplcjcofIlTV0lfzVW6ySuKMJlgpJz8Alp9bI222DFzO4dSc+JBXM9bPa
EJRmQ0soDEYRYgUcB9vQKzFmjLLkan7MGwu41u19dR19Fh+1yCyPxRY9JkiZkwFNp48gaLv9I+Tv
NwIkl0QmsSEtl24WfjHn4nVwZZ8wLtyvfZ7e7h60FUSMWZcJBrMDpz/NlxjzcaPp24ItxzpD0EwQ
1bnZhLXRGBd4KfJNBqwGSmnT3Y6C4kBRoTsQfuO3L7zlb3tryhZvcs3hIbaNjqDrQsP1Zif6u96Q
xRFfr5Texpt+pW3NbLBWXYMBtwG4MSZ2ihmR5eYkb9z2qW5p2r/XgJEMEgVgTyRiY7m9Au7H+dVs
iNV3XLx1GXaAKaMVH76Y3QHP4GJCwWMbtqbiSTANOrnRAdAZ+jyXSsfi1U5lt8YiC4FKrnqdDm5s
zFXu3+FXm+wOAEaO4szRDP0eLRL71OFx3+MfcjO3jCgNtpG0J27dUFUP/NkHDDO9ml4/quXGmhtG
ZNcVYOIBogJZwpyOEV1G0QRq4MaSkH2uc6ENVh3SVnv6JwGUGJWLktgwEwyNR4QKSe8cSmZEsPth
JrDCzXWPpbtwufTdOXk7G8w8C+DhSIABXqQLybdC0LRT4JW6P8XttltubXtqWBv0IkDA1vXKd4rQ
WqfB7yIxrZ1NbV4Fgytp6RNuOPs214vQrzpsIzhZeqgk3ZChQVcUdcX9d2vZod/N4QklOREH5seH
+TnanD43MIfmVQxmBNgwdk/kvvhfzt7SdK7R5trYIQplsDSkWAsPh8S1JWOtsodey64Ms7YSa/dh
GpD6JaOaOnx03g/cj8j5iTtzuUXqOAFPGX/OADq007c3ah4n+8Nc5754iYw4hZX0Hn61T5hPlJqO
4Ni+jF69IgFsnd8W7J45tBqdlQMQFrRCu9cXpJRXTwFn1vZK+pGanMs2WHKDNG9rrWfbW7tWzhX3
+O7L8JgDJrajs8cbufRnnRkyKQWLUkSXlpFcbygeLPIHXmmmTMH91/xe1SKNe++iAW2+UTjBOQtR
3KLKH8O5u9x12JPA0Q8Ri87CUpIuUIbaz1LTNTE2i88noawHNi8fI5Im6thgOjLmhtnoQrqvQEH9
s1mxubH+dHgq1Z9ospmMGvMVgAtGHlOtXQHC9eINiQVyI+0UhZiDmwFCMgFQ2PfscUUSlGeiwOny
qlQr3KszLHEsvRfBhFhQ64sUABJoZlc8NK7czLB2+AsRPTlA7g2fZlqPTHh54CBn8fo2xVED8Fpd
pEt6LoNZ8Ed0Ar8l2kigeu+fYZ94oLJB1Ye1GxaaDD6JkOy6uKJ78luHRMFCWyIGypNmokTkN2Yr
G55aBOOYVab7A6+HFcjPrgHSjuk3CxM013ixD0LxJgpfyfj3dq/vYlSPMQ1tViIihu+4Ouw/detC
EZ3xPNyQAX1jMtz+qz+vxo7hHoQfKokmi3et7cohBy57rkGxcENOeu0XOZXmy3Qe2O3n/zqrWYsI
CmQqr4vlLP3HcAYzlRqEANuhdWHjBKrjrWtFeBh67Ak/97PJhMWjqDZ6XAOJH0JsO+krjTqyHMlZ
aMSNb5Uu8dJh/Hq6nqrmm2ydwjDkLCypMbKsVTatJDNkS3R07QGtwVhgWu4x8YV21uiye3zHFRGD
2fUZ9TC6s0JZcMBWSAvzXeBGfX/kdu40FSATQ8XrsSe/9m8IhlSalSxxTg0rM8l17qolwF3fP4nY
QHcDIxPyCDDY/oCaJLoyoCqlnJlBJwzxFgkq0YsmmsR4s8QOKyyBxGWQAwqUmrY3FdsjFRye+TCJ
h0UfC1xVsLtn0seG/ZnfRnkL5qMlKGq9/lhPnzV3Tx816MDghlDLRHpCd/J+4ZJDPe+HY/4oZ67t
fUrkz1KUPr1uFzZncKEzr/B40uUIDEKVN0tplvLAusUAWghEG/6+Vj7h3F7zocg1QOve5mgmj7pN
Fb8DmlW9Ait1Ga5TW7AWDoe0fJT78cDGypVjdEQi70SZYrXxrXyx+kjzUiiYX/1dvpzvM/7JvEz1
mX8p9SKtELK2CPD/Ywy/p+nhd0/pU/XUvV6GKXDVNReufHk/P+zDrclId6kiKPHW1GF1AIIHneAS
9As1SO55gOXI+OlSryJrFKK+AHDJhAvek95BCQ3HoDF2iHGwkmVdEVvLCTdFfZwrGONXYN/tniCH
RChAoaWOmmn3FS9paZfTDzRBKaFE8pAGQIgXOUi5ZhutzqeeKwoLuMkCoQcphS2szLY74HCpnN6N
2xc9gn58sOB7msWEhyXJiH2IZEvbdUDXlXd+zuLm1YriEGzjPmScmJw5VOfCSF1TJ/a+Mlarx4bq
QsgrppBlOawEYSqvDtRnWfdO7xAVxhIfYhRvjcGOwx7qNY1A5RcMBzQLrW1Wgw2zBEceDxb74C2r
2hNoNS0uU+bM/Qf4byIqZdpCWU07Iukfl9aII6a+DqqsOZLTnhF5AUdbNihfxYtf1Cr+POFs3WBv
J3ROImwtvKuAQevDjn5mvzNS2bK5QPLU363VwizoPuPODeZfr+ZNcMCGOr32wi83dYVQrFc42KxB
Z94pZED566dtiB92gHxlIl3AKSHlKXAcKEs4gYL1Y6W6C8kPIzd8iWnpmHbR9gpdFQDYfBrWc/79
DmJb4Kp1o87gtubVpu4s2lNCmErPdXLU4pT7szdnNWs/36tyyJZfgesDyavqReU7Z1SQASTz/GK1
EAKgF7XdtkuHjqwGpyXRVOzhOlXCg/b4jaTcmPi4n1UkkgT2k/tqVn1kpUSZc4R8vgoJVC3SPPLE
zE64iSfPP/WSbu1SusIa3kLARXfeIJk9HHcNPxyQY8wdZN3kyAtolywpKmKnAY3cChvgSGn6P3m9
WIRfw4LWWCp/rU/klMphBTWXUhQxmWvtNytjpqwEHhpjV/SY5JflzmmQsaEAUZ/eBmpz25yGPPL3
+SZls0wnm3cqDrjibQUXwcMzXp4GDJamWpB/29WvnG0Xjhi5lDLLeCrwTu80eHX/5tYSs8SfP2Ho
PWFMdLWFzdfgYnd01hXyLwN5/SGCWzxuD5F/mfZVRK4bRpYi+KT1cVhbEAANOPXsUaNrKpb4w4zG
jFLTQni7V9qLFsusSfOi1vULwrkhPYrJS1TVRrWQJI/4S9E0vo6LtVuF4jibJfeZ0044zaL6ybsO
BnIDeL014uHfCo0wea+xrtAFCxxAOsHYOW227+UuzDN/1SAsHyYzGu6TKVwVk5l3gJlbHCl9Q6Zt
58/k2mw+CGopRKbT7b9WLj+5lm2r6hZWm44OtJPp/t8HjCu7XcFfJOSys+b2fB0GjoGU/IK6yNO3
SJ+hrgwwAUaxjhF047LcNjUNqism/Np4/2ei0cWZmwTIC1eLHa+R0kHOQGR+s7JDKPQUbL0bSq+E
cBZ5LOGn1gEwvmeP+86HmXEF4g4+zEGDgJlEDMOZPIHQQOX6XGlFHQMmTGU3w03QaG6Iso6+1BPY
/JmTBkpmbG6KUMOkKy314qK03jTkIDUccWw52EnYyycaS8r6SXHI/1sIgX6RGVw3Zhufy6RtvLAH
X2sH3DqbYm821HoqJyTvXfl/AXBJa9ozROPoLwYU1JwmQXa3W1bmGViCqRH+4UlULjLeS90F3W7y
oG4eL99eraNC41vJITnW+X6GTSWnxhfm+S/CE0dxPQKFixBFaoJduFMBPHBx16DlAYGrilt9lUY0
KruP4p2GYue6zlLMsUEzfCgi1qV5eoRQNCHJ3Ai/w3GWXWai1DMvIMc+UDrq5YwAstaOwgsB/Zqt
PHiSE0rY5dwnaziLOoxsPh6uTqL/pYnSSDEelEmk3zpuSubAkEDW/I/3XF6V5MR+Yh+EeRJ2Ea9R
enr9KWpvvJ0m5l553iG4/uTtdcdrxnYH2BqiZZgsSyrWNeP9TQi+f5sAkSvXu3TDeyk23E8tt1RT
mVWo89C5gEfACMgDkGBTbEfDBEenw6QWax70h7yuKplOk+xnGBowOSLVyRweZWHTSMIGGeZlq8br
tLXSn+oe9uWgoEIhti3ADg6q24JfX/LK9pQmVPg2aSprx/dYYv+ekChIHYYY+ffieThgCsFH6OJi
ivOlFcwiIzxdh9mtv4/yq45MhnKXvyCOgHQ0J+t13rHDEAQiDS0AXHRsweR+o+A8mg1qBbdeR8Wv
0n/m9u0c7WZgpxaMkLQznPMaAyvsm2nhZkEYHsFCpqgr0TvI+Pimlz+k1KYwSTWReOHrGJpaOnc+
gBPu7lCLPBTYtOL0aohX/s1NbzLfsc8uQ3CU7vXNdZp52A70P6Pyfbv8WxOYNni1ze7WsEnlk5LP
ZR7FiY8So9mSMOpXp5rPDwCo+zjKKllp9x/fwole2vxmY2tqSS63hztuRLm4Q2C56eTg6glfEMnX
utBDSAP+DrNt4zq0i2m3duIkDqYzXuTppMG3pEaHUGEA6vEB+GgKq+0y9AfZOlvls0AX7piaRy0k
nnrQptN7cE65/HWW1JLBW4OiHcCegEXZgZiR+KVxIFZFj8gEh+Wj1409M3xmQMDrfGT3DCE664Ob
ls7BV6+zBXzqbpVOIte6dSR0swTx9Vrr081NWhy59+0CcJqFuNFn7DW9J8DeGKo/ZrQm/C8HeTGz
0WB6W0PaTWTMFq0X1nHjSGEEPfRh6qjQC37nDnfXqKvOTMbLDO5yRKzuZGocS55UnLn6Rt6WGjtR
Cs/DSGBvh62uj9IEG0SFgAG1OMeF0R8iiUcmREU0Opzx89kx9KVz5/f0iPy53BbKM9eXNGwTd5U/
ubuTq52rfZoEWwZGI1DYfAHzheaeGOm/KYvizi1Gy21XhCdrb7Hm63Nx677IWzCo3GaQ/PjDUrlr
+lEjk8ElegjZEskAMRnCLiYg6kuZEHm6NPs5gODIN36Zm13nW2pT94GLrNRq8i2qNFbjpQ5mhsnH
B3VNQ9pccIM6FYVgYEwXEthTK7QU/4HEnrzDwybHkpAyf3dgMnk6hnFsnE+7H2SfVdY2x6uCwdY4
T2DIrmwexqT8DS2Yj6PFPv9vA/o7k2WIX4Zbe3qR+cHn1ltvxSSso7E2vJQ7xMvbGrP3DkUN5ULb
f2wyeNd07ugoPcYQ8v7zcHrjy8SJ0hEDyF0y6HreAhn5X1+jn9sHxzUXPoMUZaDVlmcYXURIV31h
P63HtXuRBElQgqhKNKW4FBs96mogCjXahsbqoIL7pZJHHpYEwLVhukVIQnVMJX++wXdvVWwqANsX
R3NF1EsK8s24ZAYUhl60Ap9UbfE1Dck5ZDL0zTA8doL3pyRqM7ZLrG4fAUTTk5CKl7PZKvHCScDz
ew8YQ22QbTFpsXZWXMpmUyTdMQcY0rmNMRiG5KKYsL0S6OtXnE3NbX+ZtYBAW7sVV82Rm7nFaMUD
Y3vy8ON9lqneHytsMq2MallMvgpvihYHXC5TSepkfeXYQ/Pz+WNuJtyeXUBpzo/RD6VUh/Y8Sn+U
iu8RqWYfTpn24njgKlGZcb/TfSOSmR/3ESI64Aiyg5nxL7W+63kKn3APdv8VCtPpU9WBaeoCx2Tj
0PfuC5OvnSjhRnPjs17sdoPMxqtBngTQ3iumHOnn50wBF2sfbnw9/9qP8MA6nG1hpjvsncOrQz/D
GBNX+Q/oqU8UL04/EV3i1xdA/Gyyjk+kmnt3NK7ipjLZ1P9NHc50GWLjqW7PGOJN9WzAWjnIZ3RL
4JcfGYOeGr/OoKDhiRTJnrjeFGP6a+c4JGsy9ryLroc5NeIQRlWpOjZONfwhHSl3Q/8zp3wl+4f2
S6IcLqf3fT4a3Sv6qsF+AYB/0gkkyZqNtQd9QDriTR04nfOgwg8z/kSF5JOGphGLv3SplIfav2Nm
bulIfHsMmg4ZCf3NVTkLBq7NB5w0pCMXv9tJswI8WfuV6k0cJ/Ng4lhymMGngID3PZoxkejkxixt
lIAOjjNSVC+qKaTX+/WadlC7soG9oe++bFBblHbHDqS579Ssk+ylvZZYWlzy0tqN2ZMxhqPT6C/N
eMLMzYx/fuqyECEOnrubYubahPFGnKwx5/sVSoc+9kwwmS5ymVT64w0ObfT/CsHmx1pRqfW2mTlk
QL7wghNryZhSkZVVsZ19Xp+TVJ9Wd86EGv2ovgCnP02AW4JKb9vXiDVUG0EFmX+Wxi70C2JWlJX2
wM1AVlQmQcgpGALWZzzv6jou2K8VVQ6ig1ULsCoaR3RESK0HVOB253+c0EzV7J7gXT6a5pVqUtpA
odFmHUG5KF47nTKdXy6ZI7w/bYNXvkS377RinKTF5OhMQmGlEkBIoUtuKsQd+OK4wyTxi10wOLiG
rh15UkWcHcSecm0so9LNQj/wFKZvsNVHDKTNlu7LAyUrZ1ilXCTBg3oabwRaXy8kj4ep8Bv+wzBT
rsRrS9qiLOQsn8w8cm5ykNPcbWz/ayoyhTrSDI44PcyoDs54K/6Ni79x3G9qdgauV2pJJCuLcyZQ
3kheDJkHL6soHtTxR/ToIRazKVpoXnv+p71Jb/6HksFFhkw/Z0/pTBIgiHVqYE1214OcRRcmfF8u
B5H0NvOSAVZTflVRa6YU5WtzcZ18g6W1thj5s6pFdf/hPnDkPk6bX5EGKxKLNYkSsAtigFhStPB/
4LiL1SLQ26S2MmJLXi1MNI7zWA4mI1KWAY2gt7YfYlmX6Nh1j9SfZGdPp3SKaV2U8uLTF7TvsGQI
CqB5YUDOZa6Grnwg+dUCuxl/6K+ba7JB/aBGL8wkC8c4TDWT42YBal7vkK5UoO7OIZavoz3Mz9DJ
ejaHYS8cqF/qaf/8RMNs7rxUZeShM6hxaLQnuAtKTz+nqXxMaAoRIqGNUOMCS1+A/PkRYDP/cOFu
iGqQPUzEp4fwXhsZVi/XYTljLrnVz42vyFZeWfgPjW+FhiARdr/aV8RSuB5N5UsL/Jo4RKPSSyni
wD4/LfeuvOF/Jo8cOLvDlASQPHCspDb2oAq7csSOnp7eDMdbzRrXd+27K5s076wTYVXrKtj9pVhV
DD/vmviRYTGgMWGLV2GT7yaLD0BsXsQTzqrCHwp+Ebyu+G0T2o7izagL0Wqm8q/df0pC5isqJ7mL
OEGcxl5/jraURDorznTyQoUr+TsNOo4X9R1n0i1Q0Jv3BEIG+v8TbSk277ysmKl/oAhzJkBJxy6h
tmSUb+EFPNcTGVYF9eVb6TAUd996INV5+5Qdh5P1mYNE1tSP4LIl+LNHipbRkaDzcw0DeraZysW5
DS2McN1c7MnCLU791vxMx8Ku+VCVjbvzy8HpySfLaliMQ3V6Imwy/hfUsn6VneU0mZDfGPT/bMjK
MQtSLcTpggGmRAf8YGtDJfCiZNL2Jh+47i/TxzZpWA3lLKxrn6/JEBVCiYx3rDq1cjq/2mnt0ihE
hwCFy9+VtsTNjCNCAEfbqZf6TDRtG0z2Oph4Sdjq1uzi5ONPSM39kX9SZH71CKauvJhd1K+B9QhO
QUVSd+xw9i59CUzLEjYZql1p1E7jIxmAd4Lm2+BECPVoEb5C2zd+woHTQrEcACykCZqPllRWcJJA
z4M7WJJ0m6Ze7FQhIu94BORk9I8ZmTv7a3mlidFb8oCDds0KMSpNdzP+GHJmZWWMs08jx0UmJ2Cc
V8tP9rDVqSscHIAqN+mExON5ZcI2waOxZgswBKJlbA2G+HJmpPURs8J10qs3N9Eq0nw=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
