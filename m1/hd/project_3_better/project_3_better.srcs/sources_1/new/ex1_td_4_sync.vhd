----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/21/2023 02:15:40 PM
-- Design Name: 
-- Module Name: ex1_td_4_sync - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ex1_td_4_sync is
    Port ( CLK : in STD_LOGIC;
           reset : in STD_LOGIC;
           D : in STD_LOGIC;
           Q : out STD_LOGIC);
end ex1_td_4_sync;

architecture Behavioral of ex1_td_4_sync is

begin

process (clk) begin
    if rising_edge(clk) then 
        if (reset = '1') then
            Q <= '0';
        else
            Q <= D;
        end if;
    end if;
end process;

end Behavioral;
